#!/bin/bash

set -e
run_cmd="dotnet StarData.Api.dll"

echo Waiting for $1 to listen on $2...
until nc -z $1 $2; do
>&2 echo "SQL Server is starting up"
sleep 1
done

>&2 echo "SQL Server is up - executing command"
exec $run_cmd
﻿//  InstantiateAdpter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:56:8 16/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using StarData.Utilities.Converters;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities
{
    public class AdapterFactory
    {
        public T InstanciateAdapter<T>() where T : IDataAdapter
        {
            IFileSystem fileSystem = new FileSystem();
            
            if(typeof(FSConverterAdapter) == typeof(T))
            {
                IConverterCache cache = new FSCache();
                ISQLProducer producer = new FSSQLProducer();
                FSLeagueConverter leagueConverter = new FSLeagueConverter(cache, producer);
                var converters = new List<IDataConverter<IMatchDto, string>>
                {
                    new FSClubConverter(cache, producer),
                    new FSPlayerConverter(cache, producer),
                    new FSMatchConverter(cache, producer),
                    new FSGoalConverter(cache, producer),
                    new FSSubstitutionConverter(cache, producer),
                    new FSFoulConverter(cache, producer),
                    new FSPlayerMatchConverter(cache, producer)
                };
                IDataAdapter adapter =
                    new FSConverterAdapter(fileSystem, leagueConverter, converters);
                return (T)adapter;
            }

            throw new NotImplementedException();
        }
    }
}

﻿//  FSSQLProducer.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:24:16 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Utils;

namespace StarData.Utilities.Converters
{
    public class FSSQLProducer : ISQLProducer
    {
        public string CreateInsert(string table, params (string Name, string Value)[] args)
        {
            if(string.IsNullOrEmpty(table) || args.Length == 0)
            {
                return string.Empty;
            }
            var builder = new StringBuilder($"INSERT INTO {table}(");

            var result = args?.Aggregate((a, b) =>
                (a.Name + "," + b.Name, a.Value + "','" + b.Value));
            builder.Append($"{result.Value.Name}) VALUES({FinalValue(result.Value.Value, false)});");
            return builder.ToString();
        }

        public string CreateInsertIfNotExist(string table, params (string Name, string Value)[] args)
        {
            if (string.IsNullOrEmpty(table) || args.Length == 0)
            {
                return string.Empty;
            }
            var builder = new StringBuilder($"INSERT INTO {table}(");

            var existsBuilder = new StringBuilder();
            //var result = args?.Aggregate((a, b) => {
            //    existsBuilder.Append($"{a.Name}='{a.Value}' AND {b.Name}='{b.Value}'");
            //    return (a.Name + "," + b.Name, a.Value + "','" + b.Value);
            //});
            /*
             * "INSERT INTO Nation(Name,Code) " +
                "SELECT 'China','CHN' " +
                "WHERE NOT EXISTS " +
                "(SELECT 1 FROM Nation WHERE Name='China' AND Code='CHN');"
             */
            var trans = args?.Select(a => $"{a.Name}='{a.Value}'");
            var names = string.Join(",", args?.Select(a => a.Name));
            var values = string.Join(",", args?.Select(a => $"'{a.Value}'"));
            var conds = string.Join(" AND ", trans);
            builder.Append($"{names}) ");
            builder.Append($"SELECT {values} ");
            builder.Append($"WHERE NOT EXISTS (SELECT 1 FROM {table} ");
            builder.Append($"WHERE {conds});");
            return builder.ToString();
        }

        public string CreateInsertQueryFK(string table,
            params (string Name, string Value, string FkName, string FkTable)[] args)
        {
            if (string.IsNullOrEmpty(table) || args.Length == 0)
            {
                return string.Empty;
            }
            var builder = new StringBuilder($"INSERT INTO {table}(");
            
            if(args?.Length > 1)
            {
                var result = args?.Aggregate((a, b) =>
                {
                    return (
                        a.Name + ", " + b.Name,
                        ReduceForInsert(a) + ", " + ReduceForInsert(b),
                        "",
                        "acc");
                });
                
                builder.Append($"{result.Value.Name}) VALUES(");
                builder.Append($"{result.Value.Value.Trim(' ').Trim(',')});");
            }
            else
            {
                var arg = args[0];
                if (string.IsNullOrEmpty(arg.FkName))
                {
                    return CreateInsert(table, (arg.Name, arg.Value));
                }
                builder.Append($"{args[0].Name}) VALUES((SELECT Id FROM {args[0].FkTable} ");
                builder.Append($"WHERE {args[0].FkName}='{args[0].Value}'));");
            }
            
            return builder.ToString();
        }

        public string CreateInsertRelationsForPlayerMatch(
            string table, string match,
            IEnumerable<(string Club, string Player, int IsBench)> args)
        {
            var builder = new StringBuilder();
            builder.Append("INSERT INTO PlayerMatch(Match, Player, Club, IsBench) VALUES");
            var relations = args?.Select(p => {
                return $"(@MatchId," +
                       $"(SELECT Id FROM Player WHERE Name='{ConvertUtil.SafeName(p.Player)}')," +
                       $"{p.Club},{p.IsBench})";
            });
            builder.Append(string.Join(",", relations) + ";");
            return builder.ToString();
        }

        public string CreateInsertQueryFKIfNotExist(string table, params (string Name, string Value, string FkName, string FkTable)[] args)
        {
            var builder = new StringBuilder();
            var trans = args?.Select(a =>
            {
                var variable = string.Empty;
                var value = $"'{a.Value}'";
                var cond = $"{a.Name}='{a.Value}'";
                if (!string.IsNullOrEmpty(a.FkName))
                {
                    var fkCondArray = a.FkName.Split(',')
                                     .Select(f => $"{f}='{a.Value}'");
                    var fkConds = string.Join(" OR ", fkCondArray);

                    variable = $"SELECT @{a.Name}Id=Id FROM {a.FkTable} WHERE {fkConds};";
                    value = $"@{a.Name}Id";
                    cond = $"{a.Name}={value}";
                }
                
                return (variable, name:a.Name, value, cond);
            });
            var variables = string.Join(";", trans.Where(t => !string.IsNullOrEmpty(t.variable)).Select(t => t.variable));
            var names = string.Join(",", trans.Select(t => t.name));
            var values = string.Join(",", trans.Select(t => t.value));
            var conds = string.Join(" AND ", trans.Select(t => t.cond));
            builder.Append(variables);
            builder.Append($"INSERT INTO {table}");
            builder.Append($"({names}) SELECT ");
            builder.Append($"{values} ");
            builder.Append($"WHERE NOT EXISTS (SELECT 1 FROM {table} WHERE ");
            builder.Append($"{conds}");
            builder.Append(");");

            return builder.ToString();
        }

        private string ReduceForInsert((string Name, string Value, string FkName, string FkTable) args)
        {
            if (string.IsNullOrEmpty(args.FkName))
            {
                return FinalValue(args.Value, args.FkTable.Equals("acc"));
            }
            else
            {
                var fkOrNames = args.FkName.Split(',')
                    .Select(f => $"{f}='{args.Value}'");
                var conditions = string.Join(" OR ", fkOrNames);
                return $"(SELECT Id FROM {args.FkTable} WHERE {conditions})";
            }
        }


        private string ReduceForSelect((string Name, string Value, string FkName, string FkTable) args)
        {

            return string.IsNullOrEmpty(args.FkName) ? $"{args.Name}='{args.Value}'" : string.Empty;
        }

        private string FinalValue(string value, bool fromAcc)
        {
            if (fromAcc || value.StartsWith("CONVERT"))
            {
                return value;
            }
            return $"'{value}'";
        }

        public string CreateInsertRelationIfNotExist(string table, params (string Name, string Value, string FkName, string FkTable)[] args)
        {
            
            var builder = new StringBuilder();
            var trans = args?.Select(a =>
            {
                var fkId = $"SELECT @{a.Name}Id=Id FROM {a.FkTable} WHERE {a.FkName}='{a.Value}'";
                var name = a.Name;
                var para = $"@{a.Name}Id";
                var cond = $"{a.Name}=@{a.Name}Id";
                return (fkId, name, para, cond);
            });
            var fkIds = string.Join(";", trans.Select(a => a.fkId));
            var names = string.Join(",", trans.Select(a => a.name));
            var paras = string.Join(",", trans.Select(a => a.para));
            var conds = string.Join(" AND ", trans.Select(a => a.cond));
            builder.Append(fkIds);
            builder.Append($";INSERT INTO {table}({names}) SELECT {paras} ");
            builder.Append($"WHERE NOT EXISTS(SELECT 1 FROM {table} WHERE {conds});");
            return builder.ToString();
        }
    }
}

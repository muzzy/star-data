﻿//  FSCache.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       20:22:47 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSCache : IConverterCache
    {
        public string CurrentNation { get; set; }
        public string CurrentSeason { get; set; }
        public string CurrentLeague { get; set; }
    }
}

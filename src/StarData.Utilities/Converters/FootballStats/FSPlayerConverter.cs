﻿//  FSPlayerConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:32:36 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Dtos;
using StarData.Utilities.Utils;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSPlayerConverter : FSConverterBase
    {
        private ISet<string> _playerCached = new HashSet<string>();
        public FSPlayerConverter(IConverterCache fSCache, ISQLProducer producer)
            : base(fSCache, producer)
        {
        }

        public override string Convert(IMatchDto match)
        {
            var builder = new StringBuilder();
            var homePlayers = ConvertUtil.MergeList<string>(match.TeamHomePlayers, match.BenchHomePlayers);
            var awayPlayers = ConvertUtil.MergeList<string>(match.TeamAwayPlayers, match.BenchAwayPlayers);
            GenerateSql(match.HomeTeam, homePlayers, builder);
            GenerateSql(match.AwayTeam, awayPlayers, builder);
            cache.Append(builder);
            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#Player");
            cache.Append("declare @ClubId int;declare @PlayerId int;");
        }

        private void GenerateSql(string team, IList<string> players, StringBuilder builder)
        {
            foreach (var player in players)
            {
                var validName = ConvertUtil.SafeName(player);
                if (!_playerCached.Contains(validName))
                {
                    builder.Append(_producer.CreateInsertIfNotExist(
                        "Player",
                        ("Name", validName)
                        ));
                    builder.Append(
                    _producer.CreateInsertRelationIfNotExist(
                        "Transfer",
                        ("Player", validName, "Name", "Player"),
                        ("Club", team, "Name", "Club")
                        ));
                    _playerCached.Add(validName);
                }
            }
        }
    }
}

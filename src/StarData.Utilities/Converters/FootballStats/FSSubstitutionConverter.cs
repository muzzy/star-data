﻿//  FSSubstitutionConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:3:26 2/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Utils;
using StarData.Utilities.Dtos;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSSubstitutionConverter : FSConverterBase
    {
        public FSSubstitutionConverter(IConverterCache fSCache, ISQLProducer producer)
            : base(fSCache, producer)
        {

        }

        public override string Convert(IMatchDto match)
        {
            //var match = source.ToObject<MatchForSubstitutionDto>();
            var builder = new StringBuilder();
            var subsHome = match.SubsHome.Where(s => match.BenchHomePlayers.Contains(s.On));
            var subsAway = match.SubsAway.Where(s => match.BenchAwayPlayers.Contains(s.On));
            var substitutions = subsHome.Union(subsAway);

            foreach (var sub in substitutions)
            {
                builder.Append(_producer.CreateInsertQueryFK(
                    "Substitution",
                    ("Match", match.MD5(), "MD5Code", "Match"),
                    ("OnPlayer", ConvertUtil.SafeName(sub.On), "Name", "Player"),
                    ("OffPlayer", ConvertUtil.SafeName(sub.Off), "Name", "Player"),
                    ("Time", sub.Time, string.Empty, string.Empty)
                    ));
            }
            cache.Append(builder);
            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#Substitution");
        }
    }
}

﻿//  FSFoulConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       13:49:23 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Dtos;
using StarData.Utilities.Utils;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSFoulConverter : FSConverterBase
    {
        public FSFoulConverter(IConverterCache fSCache, ISQLProducer producer)
            : base(fSCache, producer)
        {
        }

        public override string Convert(IMatchDto match)
        {
            var builder = new StringBuilder();
            var homePlayers = ConvertUtil.MergeList(match.TeamHomePlayers, match.BenchHomePlayers);
            GenerateSQL(match.MD5(), match.FoulHome, homePlayers, builder);
            var awayPlayers = ConvertUtil.MergeList(match.TeamAwayPlayers, match.BenchAwayPlayers);
            GenerateSQL(match.MD5(), match.FoulAway, awayPlayers, builder);

            cache.Append(builder);

            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#Foul");
        }

        private void GenerateSQL(string matchMd5,
            IList<(string Name, string card, string type, int Time)> fouls,
            IList<string> players,
            StringBuilder builder)
        {
            var sql = fouls?.Where(f =>
            players.Contains(f.Name)
            && f.card.Contains("Red")
            && f.Time > 0)
                .Select((f, i) =>
                _producer.CreateInsertQueryFK(
                    "Foul",
                    ("Match", matchMd5, "MD5Code", "Match"),
                    ("Player", ConvertUtil.SafeName(f.Name), "Name", "Player"),
                    ("Time", f.Time.ToString(), string.Empty, string.Empty)
                    )) ?? new string[] { };

            builder.Append(string.Join(string.Empty, sql));
        }
    }
}

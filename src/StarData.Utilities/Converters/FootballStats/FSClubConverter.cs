﻿//  FSMatchConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:49:17 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Text;
using StarData.Utilities.Dtos;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSClubConverter : FSConverterBase
    {
        private IDictionary<string, int> _nationIdCache = new Dictionary<string, int>();
        private ISet<string> _clubCached = new HashSet<string>();

        public FSClubConverter(IConverterCache fSCache, ISQLProducer producer)
            : base(fSCache, producer)
        {

        }
        
        public override string Convert(IMatchDto match)
        {
            StringBuilder builder = new();
            GenerateInsertSql(match.HomeTeam, builder);
            GenerateInsertSql(match.AwayTeam, builder);
            
            cache.Append(builder);
            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#Club");
            cache.Append("DECLARE @SeasonId int;DECLARE @ClubId int;DECLARE @LeagueId int;DECLARE @NationId int;");
        }

        private void GenerateInsertSql(string team, StringBuilder builder)
        {
            if (string.IsNullOrEmpty(_convertCache.CurrentNation))
            {
                throw new InvalidOperationException("Nation not found in FSCache, please convert Category first");
            }

            if (!_clubCached.Contains(team))
            {
                builder.Append(
                    _producer.CreateInsertQueryFKIfNotExist(
                        "Club",
                        ("Name", team, string.Empty, string.Empty),
                        ("Nation", _convertCache.CurrentNation, "Name,Alias", "Nation"),
                        ("League", _convertCache.CurrentLeague, "Name", "League")
                    ));
                builder.Append(
                    _producer.CreateInsertRelationIfNotExist(
                        "SeasonClub",
                        ("Season", _convertCache.CurrentSeason, "Name", "Season"),
                        ("Club", team, "Name", "Club")
                        ));
            }


        }
    }
}

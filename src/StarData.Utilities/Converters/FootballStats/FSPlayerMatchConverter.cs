﻿//  FSPlayerMatchConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:20:58 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Linq;
using System.Text;
using StarData.Utilities.Dtos;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSPlayerMatchConverter : FSConverterBase
    {
        public FSPlayerMatchConverter(IConverterCache convertCache, ISQLProducer producer) : base(convertCache, producer)
        {
        }

        public override string Convert(IMatchDto match)
        {
            cache.Append($"SELECT @MatchId=Id FROM Match WHERE MD5Code='{match.MD5()}';");
            cache.Append($"SELECT @HomeId=Id FROM Club WHERE Name='{match.HomeTeam}';");
            cache.Append($"SELECT @AwayId=Id FROM Club WHERE Name='{match.AwayTeam}';");

            var builder = new StringBuilder();
            var players = match.TeamHomePlayers.Select(p => (Club: "@HomeId", Player: p, IsBench: 0))
                            .Concat(match.BenchHomePlayers.Select(p => (Club: "@HomeId", Player: p, IsBench: 1)))
                            .Concat(match.TeamAwayPlayers.Select(p => (Club: "@AwayId", Player: p, IsBench: 0)))
                            .Concat(match.BenchAwayPlayers.Select(p => (Club: "@AwayId", Player: p, IsBench: 1)));
            builder.Append(_producer.CreateInsertRelationsForPlayerMatch(
                "PlayerMatch", match.MD5(), players));
            cache.Append(builder);
            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#PlayerMatch");
            cache.Append("DECLARE @MatchId int;DECLARE @HomeId int;DECLARE @AwayId int;");
            
        }
    }
}

﻿//  FSMatchCategoryConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       16:0:21 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Dtos;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSLeagueConverter : IDataConverter<string, string>
    {
        private ISQLProducer _producer;
        private IConverterCache _convertCache;
        private StringBuilder cache = new StringBuilder();
        public FSLeagueConverter(IConverterCache convertCache, ISQLProducer producer)
        {
            _producer = producer;
            _convertCache = convertCache;
            InitCache();
        }
        public string Convert(string source)
        {
            StringBuilder builder = new();
            if (source != null)
            {
                cache.AppendLine($" - {source}");
                var league = source;
                string[] names = league.Split(' ');
                string leagueName = string.Join(' ', names.Take(names.Length - 1));
                builder.Append(_producer.CreateInsertIfNotExist("League", ("Name",leagueName)));

                string seasonName = names[^1];
                builder.Append(_producer.CreateInsert(
                    "Season",
                    ("Name", seasonName)
                    ));
                cache.Append(builder);

                // Cache League, Nation & Season
                _convertCache.CurrentLeague = leagueName;
                _convertCache.CurrentNation = names.First();
                _convertCache.CurrentSeason = seasonName;
            }
            return builder.ToString();
        }

        public string Read()
        {
            var result = cache.ToString();
            cache.Clear();
            cache = null;
            cache = new StringBuilder();
            InitCache();
            return result;
        }

        private void InitCache()
        {
            cache.Append("#Category");
        }
    }
}

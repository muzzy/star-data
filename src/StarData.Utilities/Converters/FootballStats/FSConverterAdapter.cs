﻿//  Json2SqlConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       13:48:20 30/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System.Collections.Generic;
using System.IO.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Dtos;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSConverterAdapter : IDataAdapter
    {
        private readonly IFileSystem _fileSystem;
        private readonly FSLeagueConverter _categoryConverter;
        private readonly IEnumerable<IDataConverter<IMatchDto, string>> _converters;

        public FSConverterAdapter(
            IFileSystem fileSystem,
            FSLeagueConverter categoryConverter,
            IEnumerable<IDataConverter<IMatchDto, string>> converters
            )
        {
            _fileSystem = fileSystem;
            _categoryConverter = categoryConverter;
            _converters = converters;
        }


        public void ToSqlFile(string source, string target)
        {
            using(var fileReader = _fileSystem.File.OpenText(source))
            using(var fileWriter = _fileSystem.File.CreateText(target))
            {
                // Match data
                JsonReader reader = new JsonTextReader(fileReader);
                var jsonMatch = JObject.ReadFrom(reader);

                // Category
                if(_categoryConverter != null)
                {
                    string sql = _categoryConverter.Convert(jsonMatch["league"].Value<string>());
                    if (string.IsNullOrEmpty(sql))
                    {
                        return;
                    }
                }

                // Match
                if (jsonMatch["matches"] != null)
                {
                    var matches = jsonMatch["matches"].Values<JToken>().Values<JToken>();
                    foreach (var match in matches)
                    {
                        var matchDto = match.ToObject<FSMatchDto>();
                        if (_converters != null)
                        {
                            foreach (var converter in _converters)
                            {
                                converter.Convert(matchDto);
                            }
                        }
                    }
                }

                // Write to file
                fileWriter.WriteLine(_categoryConverter.Read());
                foreach (var converter in _converters)
                {
                    fileWriter.WriteLine(converter.Read());
                }
            }
        }
    }
}

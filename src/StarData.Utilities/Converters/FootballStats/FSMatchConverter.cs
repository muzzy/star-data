﻿//  FSMatchConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:32:41 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Text;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Dtos;
using StarData.Utilities.Utils;
using System.Linq;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSMatchConverter : FSConverterBase
    {
        public FSMatchConverter(IConverterCache fSCache, ISQLProducer producer)
            : base(fSCache, producer)
        {
        }

        public override string Convert(IMatchDto match)
        {
            var builder = new StringBuilder();
            builder.Append(_producer.CreateInsertQueryFK(
                "Match",
                ("Season", _convertCache.CurrentSeason, "Name", "Season"),
                ("League", _convertCache.CurrentLeague, "Name", "League"),
                ("Round", "1", string.Empty, string.Empty),
                ("HomeTeam", match.HomeTeam, "Name", "Club"),
                ("AwayTeam", match.AwayTeam, "Name", "Club"),
                ("HomeGoal", match.HomeScore.ToString(), string.Empty, string.Empty),
                ("AwayGoal", match.AwayScore.ToString(), string.Empty, string.Empty),
                ("Time", $"CONVERT(SMALLDATETIME, '{match.MatchTime()}')", string.Empty, string.Empty),
                ("MD5Code", match.MD5(), string.Empty, string.Empty)
                ));
            cache.Append(builder);
            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#Match");
        }
    }
}

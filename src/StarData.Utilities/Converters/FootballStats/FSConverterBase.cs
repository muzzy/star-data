﻿//  FSConverterBase.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:16:27 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Text;
using StarData.Utilities.Dtos;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Converters.FootballStats
{
    public abstract class FSConverterBase : IDataConverter<IMatchDto, string>
    {
        protected StringBuilder cache = new StringBuilder();
        protected ISQLProducer _producer;
        protected IConverterCache _convertCache;

        public FSConverterBase(IConverterCache convertCache, ISQLProducer producer)
        {
            _producer = producer;
            _convertCache = convertCache;
            InitCache();
        }

        public abstract string Convert(IMatchDto source);
        protected abstract void InitCache();

        public string Read()
        {
            var result = cache.ToString();
            cache.Clear();
            cache = null;
            cache = new StringBuilder();
            InitCache();
            return result;
        }
    }
}

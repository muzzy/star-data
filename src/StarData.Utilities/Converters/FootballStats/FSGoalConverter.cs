﻿//  FSGoalConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       8:41:14 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Dtos;
using StarData.Utilities.Utils;

namespace StarData.Utilities.Converters.FootballStats
{
    public class FSGoalConverter : FSConverterBase
    {
        public FSGoalConverter(IConverterCache fSCache, ISQLProducer producer)
            : base(fSCache, producer)
        {
        }

        public override string Convert(IMatchDto match)
        {
            var builder = new StringBuilder();
            var goalHome = match.GoalHome?.Take(Math.Min(match.HomeScore, match.GoalHome.Count()));
            if(goalHome != null)
            {
                GenerateSql(match.MD5(), match.HomeTeam, goalHome, builder);
            }
            var goalAway = match.GoalAway?.Take(Math.Min(match.AwayScore, match.GoalAway.Count()));
            if(goalAway != null)
            {
                GenerateSql(match.MD5(), match.AwayTeam, goalAway, builder);
            }

            cache.Append(builder);

            return builder.ToString();
        }

        protected override void InitCache()
        {
            cache.AppendLine("#Goal");
        }

        private void GenerateSql(string matchMD5, string club, IEnumerable<(string Name, int Time)> goals, StringBuilder builder)
        {
            foreach (var goal in goals)
            {
                
                builder.Append(_producer.CreateInsertQueryFK(
                    "Goal",
                    ("Match", matchMD5, "MD5Code", "Match"),
                    ("Player", ConvertUtil.SafeName(goal.Name), "Name", "Player"),
                    ("Club", club, "Name", "Club"),
                    ("Time", goal.Time.ToString(), string.Empty, string.Empty)
                ));
            }
        }
    }
}

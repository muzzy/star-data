﻿//  IConverterCache.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       21:40:55 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;

namespace StarData.Utilities.Interfaces
{
    public interface IConverterCache
    {
        public string CurrentSeason { get; set; }
        public string CurrentNation { get; set; }
        public string CurrentLeague { get; set; }
    }
}

﻿//  ISQLProducer.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:21:39 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Text;

namespace StarData.Utilities.Interfaces
{
    public interface ISQLProducer
    {
        string CreateInsert(string table, params (string Name, string Value)[] args);
        string CreateInsertIfNotExist(string table, params (string Name, string Value)[] args);
        string CreateInsertQueryFK(string table,
            params (string Name, string Value, string FkName, string FkTable)[] args);
        string CreateInsertQueryFKIfNotExist(string table,
            params (string Name, string Value, string FkName, string FkTable)[] args);
        string CreateInsertRelationIfNotExist(string table,
            params (string Name, string Value, string FkName, string FkTable)[] args);
        public string CreateInsertRelationsForPlayerMatch(
            string table, string match,
            IEnumerable<(string Club, string Player, int IsBench)> args);
    }
}

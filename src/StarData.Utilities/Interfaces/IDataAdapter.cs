﻿//  IDataAdapter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       21:40:11 2/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Utilities.Interfaces
{
    public interface IDataAdapter
    {
        void ToSqlFile(string source, string target);
    }
}

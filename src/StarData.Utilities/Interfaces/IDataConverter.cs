﻿//  IDataConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       13:28:32 30/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Threading.Tasks;

namespace StarData.Utilities.Interfaces
{
    public interface IDataConverter<S, T>
    {
        T Convert(S source);

        T Read();
    }
}

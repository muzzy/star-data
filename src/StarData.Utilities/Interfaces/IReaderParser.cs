﻿//  IReaderParser.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:36:29 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Data;

namespace StarData.Utilities.Interfaces
{
    public interface IReaderParser<T>
    {
        T Parse(IDataReader reader);
        IEnumerable ParseForCollection(IDataReader reader);
    }
}

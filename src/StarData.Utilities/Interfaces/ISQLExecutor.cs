﻿//  ISQLExecutor.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:38:40 23/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.CommandLine;
using System.Data;

namespace StarData.Utilities.Interfaces
{
    public interface ISQLExecutor
    {
        void Setup(string connectString);
        void AddLogging(IConsole console);
        int ExecuteSql(string sql);
        void ExecuteSqlForJson<T>(string sql, string outputFile, IReaderParser<T> parser);
        int ExecuteFile(string sqlFile, bool byLine = true, params string[] extras);
    }
}

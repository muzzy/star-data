﻿INSERT INTO Nation(Name,Code) VALUES ('Afghanistan','AF');
INSERT INTO Nation(Name,Code) VALUES ('Åland Islands','AX');
INSERT INTO Nation(Name,Code) VALUES ('Albania','AL');
INSERT INTO Nation(Name,Code) VALUES ('Algeria','DZ');
INSERT INTO Nation(Name,Code) VALUES ('American Samoa','AS');
INSERT INTO Nation(Name,Code) VALUES ('Andorra','AD');
INSERT INTO Nation(Name,Code) VALUES ('Angola','AO');
INSERT INTO Nation(Name,Code) VALUES ('Anguilla','AI');
INSERT INTO Nation(Name,Code) VALUES ('Antarctica','AQ');
INSERT INTO Nation(Name,Code) VALUES ('Antigua and Barbuda','AG');
INSERT INTO Nation(Name,Code) VALUES ('Argentina','AR');
INSERT INTO Nation(Name,Code) VALUES ('Armenia','AM');
INSERT INTO Nation(Name,Code) VALUES ('Aruba','AW');
INSERT INTO Nation(Name,Code) VALUES ('Australia','AU');
INSERT INTO Nation(Name,Code) VALUES ('Austria','AT');
INSERT INTO Nation(Name,Code) VALUES ('Azerbaijan','AZ');
INSERT INTO Nation(Name,Code) VALUES ('Bahamas','BS');
INSERT INTO Nation(Name,Code) VALUES ('Bahrain','BH');
INSERT INTO Nation(Name,Code) VALUES ('Bangladesh','BD');
INSERT INTO Nation(Name,Code) VALUES ('Barbados','BB');
INSERT INTO Nation(Name,Code) VALUES ('Belarus','BY');
INSERT INTO Nation(Name,Code) VALUES ('Belgium','BE');
INSERT INTO Nation(Name,Code) VALUES ('Belize','BZ');
INSERT INTO Nation(Name,Code) VALUES ('Benin','BJ');
INSERT INTO Nation(Name,Code) VALUES ('Bermuda','BM');
INSERT INTO Nation(Name,Code) VALUES ('Bhutan','BT');
INSERT INTO Nation(Name,Code) VALUES ('Bolivia, Plurinational State of','BO');
INSERT INTO Nation(Name,Code) VALUES ('Bonaire, Sint Eustatius and Saba','BQ');
INSERT INTO Nation(Name,Code) VALUES ('Bosnia and Herzegovina','BA');
INSERT INTO Nation(Name,Code) VALUES ('Botswana','BW');
INSERT INTO Nation(Name,Code) VALUES ('Bouvet Island','BV');
INSERT INTO Nation(Name,Code) VALUES ('Brazil','BR');
INSERT INTO Nation(Name,Code) VALUES ('British Indian Ocean Territory','IO');
INSERT INTO Nation(Name,Code) VALUES ('Brunei Darussalam','BN');
INSERT INTO Nation(Name,Code) VALUES ('Bulgaria','BG');
INSERT INTO Nation(Name,Code) VALUES ('Burkina Faso','BF');
INSERT INTO Nation(Name,Code) VALUES ('Burundi','BI');
INSERT INTO Nation(Name,Code) VALUES ('Cambodia','KH');
INSERT INTO Nation(Name,Code) VALUES ('Cameroon','CM');
INSERT INTO Nation(Name,Code) VALUES ('Canada','CA');
INSERT INTO Nation(Name,Code) VALUES ('Cape Verde','CV');
INSERT INTO Nation(Name,Code) VALUES ('Cayman Islands','KY');
INSERT INTO Nation(Name,Code) VALUES ('Central African Republic','CF');
INSERT INTO Nation(Name,Code) VALUES ('Chad','TD');
INSERT INTO Nation(Name,Code) VALUES ('Chile','CL');
INSERT INTO Nation(Name,Code) VALUES ('China','CN');
INSERT INTO Nation(Name,Code) VALUES ('Christmas Island','CX');
INSERT INTO Nation(Name,Code) VALUES ('Cocos (Keeling) Islands','CC');
INSERT INTO Nation(Name,Code) VALUES ('Colombia','CO');
INSERT INTO Nation(Name,Code) VALUES ('Comoros','KM');
INSERT INTO Nation(Name,Code) VALUES ('Congo','CG');
INSERT INTO Nation(Name,Code) VALUES ('Congo, the Democratic Republic of the','CD');
INSERT INTO Nation(Name,Code) VALUES ('Cook Islands','CK');
INSERT INTO Nation(Name,Code) VALUES ('Costa Rica','CR');
INSERT INTO Nation(Name,Code) VALUES ('Côte d''Ivoire','CI');
INSERT INTO Nation(Name,Code) VALUES ('Croatia','HR');
INSERT INTO Nation(Name,Code) VALUES ('Cuba','CU');
INSERT INTO Nation(Name,Code) VALUES ('Curaçao','CW');
INSERT INTO Nation(Name,Code) VALUES ('Cyprus','CY');
INSERT INTO Nation(Name,Code) VALUES ('Czech Republic','CZ');
INSERT INTO Nation(Name,Code) VALUES ('Denmark','DK');
INSERT INTO Nation(Name,Code) VALUES ('Djibouti','DJ');
INSERT INTO Nation(Name,Code) VALUES ('Dominica','DM');
INSERT INTO Nation(Name,Code) VALUES ('Dominican Republic','DO');
INSERT INTO Nation(Name,Code) VALUES ('Ecuador','EC');
INSERT INTO Nation(Name,Code) VALUES ('Egypt','EG');
INSERT INTO Nation(Name,Code) VALUES ('El Salvador','SV');
INSERT INTO Nation(Name,Code,Alias) VALUES ('England','UK','English');
INSERT INTO Nation(Name,Code) VALUES ('Equatorial Guinea','GQ');
INSERT INTO Nation(Name,Code) VALUES ('Eritrea','ER');
INSERT INTO Nation(Name,Code) VALUES ('Estonia','EE');
INSERT INTO Nation(Name,Code) VALUES ('Ethiopia','ET');
INSERT INTO Nation(Name,Code) VALUES ('Falkland Islands (Malvinas)','FK');
INSERT INTO Nation(Name,Code) VALUES ('Faroe Islands','FO');
INSERT INTO Nation(Name,Code) VALUES ('Fiji','FJ');
INSERT INTO Nation(Name,Code) VALUES ('Finland','FI');
INSERT INTO Nation(Name,Code) VALUES ('France','FR');
INSERT INTO Nation(Name,Code) VALUES ('French Guiana','GF');
INSERT INTO Nation(Name,Code) VALUES ('French Polynesia','PF');
INSERT INTO Nation(Name,Code) VALUES ('French Southern Territories','TF');
INSERT INTO Nation(Name,Code) VALUES ('Gabon','GA');
INSERT INTO Nation(Name,Code) VALUES ('Gambia','GM');
INSERT INTO Nation(Name,Code) VALUES ('Georgia','GE');
INSERT INTO Nation(Name,Code) VALUES ('Germany','DE');
INSERT INTO Nation(Name,Code) VALUES ('Ghana','GH');
INSERT INTO Nation(Name,Code) VALUES ('Gibraltar','GI');
INSERT INTO Nation(Name,Code) VALUES ('Greece','GR');
INSERT INTO Nation(Name,Code) VALUES ('Greenland','GL');
INSERT INTO Nation(Name,Code) VALUES ('Grenada','GD');
INSERT INTO Nation(Name,Code) VALUES ('Guadeloupe','GP');
INSERT INTO Nation(Name,Code) VALUES ('Guam','GU');
INSERT INTO Nation(Name,Code) VALUES ('Guatemala','GT');
INSERT INTO Nation(Name,Code) VALUES ('Guernsey','GG');
INSERT INTO Nation(Name,Code) VALUES ('Guinea','GN');
INSERT INTO Nation(Name,Code) VALUES ('Guinea-Bissau','GW');
INSERT INTO Nation(Name,Code) VALUES ('Guyana','GY');
INSERT INTO Nation(Name,Code) VALUES ('Haiti','HT');
INSERT INTO Nation(Name,Code) VALUES ('Heard Island and McDonald Islands','HM');
INSERT INTO Nation(Name,Code) VALUES ('Holy See (Vatican City State)','VA');
INSERT INTO Nation(Name,Code) VALUES ('Honduras','HN');
INSERT INTO Nation(Name,Code) VALUES ('Hong Kong','HK');
INSERT INTO Nation(Name,Code) VALUES ('Hungary','HU');
INSERT INTO Nation(Name,Code) VALUES ('Iceland','IS');
INSERT INTO Nation(Name,Code) VALUES ('India','IN');
INSERT INTO Nation(Name,Code) VALUES ('Indonesia','ID');
INSERT INTO Nation(Name,Code) VALUES ('Iran, Islamic Republic of','IR');
INSERT INTO Nation(Name,Code) VALUES ('Iraq','IQ');
INSERT INTO Nation(Name,Code) VALUES ('Ireland','IE');
INSERT INTO Nation(Name,Code) VALUES ('Isle of Man','IM');
INSERT INTO Nation(Name,Code) VALUES ('Israel','IL');
INSERT INTO Nation(Name,Code) VALUES ('Italy','IT');
INSERT INTO Nation(Name,Code) VALUES ('Jamaica','JM');
INSERT INTO Nation(Name,Code) VALUES ('Japan','JP');
INSERT INTO Nation(Name,Code) VALUES ('Jersey','JE');
INSERT INTO Nation(Name,Code) VALUES ('Jordan','JO');
INSERT INTO Nation(Name,Code) VALUES ('Kazakhstan','KZ');
INSERT INTO Nation(Name,Code) VALUES ('Kenya','KE');
INSERT INTO Nation(Name,Code) VALUES ('Kiribati','KI');
INSERT INTO Nation(Name,Code) VALUES ('Korea, Democratic People''s Republic of','KP');
INSERT INTO Nation(Name,Code) VALUES ('Korea, Republic of','KR');
INSERT INTO Nation(Name,Code) VALUES ('Kuwait','KW');
INSERT INTO Nation(Name,Code) VALUES ('Kyrgyzstan','KG');
INSERT INTO Nation(Name,Code) VALUES ('Lao People''s Democratic Republic','LA');
INSERT INTO Nation(Name,Code) VALUES ('Latvia','LV');
INSERT INTO Nation(Name,Code) VALUES ('Lebanon','LB');
INSERT INTO Nation(Name,Code) VALUES ('Lesotho','LS');
INSERT INTO Nation(Name,Code) VALUES ('Liberia','LR');
INSERT INTO Nation(Name,Code) VALUES ('Libya','LY');
INSERT INTO Nation(Name,Code) VALUES ('Liechtenstein','LI');
INSERT INTO Nation(Name,Code) VALUES ('Lithuania','LT');
INSERT INTO Nation(Name,Code) VALUES ('Luxembourg','LU');
INSERT INTO Nation(Name,Code) VALUES ('Macao','MO');
INSERT INTO Nation(Name,Code) VALUES ('Macedonia, the Former Yugoslav Republic of','MK');
INSERT INTO Nation(Name,Code) VALUES ('Madagascar','MG');
INSERT INTO Nation(Name,Code) VALUES ('Malawi','MW');
INSERT INTO Nation(Name,Code) VALUES ('Malaysia','MY');
INSERT INTO Nation(Name,Code) VALUES ('Maldives','MV');
INSERT INTO Nation(Name,Code) VALUES ('Mali','ML');
INSERT INTO Nation(Name,Code) VALUES ('Malta','MT');
INSERT INTO Nation(Name,Code) VALUES ('Marshall Islands','MH');
INSERT INTO Nation(Name,Code) VALUES ('Martinique','MQ');
INSERT INTO Nation(Name,Code) VALUES ('Mauritania','MR');
INSERT INTO Nation(Name,Code) VALUES ('Mauritius','MU');
INSERT INTO Nation(Name,Code) VALUES ('Mayotte','YT');
INSERT INTO Nation(Name,Code) VALUES ('Mexico','MX');
INSERT INTO Nation(Name,Code) VALUES ('Micronesia, Federated States of','FM');
INSERT INTO Nation(Name,Code) VALUES ('Moldova, Republic of','MD');
INSERT INTO Nation(Name,Code) VALUES ('Monaco','MC');
INSERT INTO Nation(Name,Code) VALUES ('Mongolia','MN');
INSERT INTO Nation(Name,Code) VALUES ('Montenegro','ME');
INSERT INTO Nation(Name,Code) VALUES ('Montserrat','MS');
INSERT INTO Nation(Name,Code) VALUES ('Morocco','MA');
INSERT INTO Nation(Name,Code) VALUES ('Mozambique','MZ');
INSERT INTO Nation(Name,Code) VALUES ('Myanmar','MM');
INSERT INTO Nation(Name,Code) VALUES ('Namibia','NA');
INSERT INTO Nation(Name,Code) VALUES ('Nauru','NR');
INSERT INTO Nation(Name,Code) VALUES ('Nepal','NP');
INSERT INTO Nation(Name,Code) VALUES ('Netherlands','NL');
INSERT INTO Nation(Name,Code) VALUES ('New Caledonia','NC');
INSERT INTO Nation(Name,Code) VALUES ('New Zealand','NZ');
INSERT INTO Nation(Name,Code) VALUES ('Nicaragua','NI');
INSERT INTO Nation(Name,Code) VALUES ('Niger','NE');
INSERT INTO Nation(Name,Code) VALUES ('Nigeria','NG');
INSERT INTO Nation(Name,Code) VALUES ('Niue','NU');
INSERT INTO Nation(Name,Code) VALUES ('Norfolk Island','NF');
INSERT INTO Nation(Name,Code) VALUES ('Northern Mariana Islands','MP');
INSERT INTO Nation(Name,Code) VALUES ('Norway','NO');
INSERT INTO Nation(Name,Code) VALUES ('Oman','OM');
INSERT INTO Nation(Name,Code) VALUES ('Pakistan','PK');
INSERT INTO Nation(Name,Code) VALUES ('Palau','PW');
INSERT INTO Nation(Name,Code) VALUES ('Palestine, State of','PS');
INSERT INTO Nation(Name,Code) VALUES ('Panama','PA');
INSERT INTO Nation(Name,Code) VALUES ('Papua New Guinea','PG');
INSERT INTO Nation(Name,Code) VALUES ('Paraguay','PY');
INSERT INTO Nation(Name,Code) VALUES ('Peru','PE');
INSERT INTO Nation(Name,Code) VALUES ('Philippines','PH');
INSERT INTO Nation(Name,Code) VALUES ('Pitcairn','PN');
INSERT INTO Nation(Name,Code) VALUES ('Poland','PL');
INSERT INTO Nation(Name,Code) VALUES ('Portugal','PT');
INSERT INTO Nation(Name,Code) VALUES ('Puerto Rico','PR');
INSERT INTO Nation(Name,Code) VALUES ('Qatar','QA');
INSERT INTO Nation(Name,Code) VALUES ('Réunion','RE');
INSERT INTO Nation(Name,Code) VALUES ('Romania','RO');
INSERT INTO Nation(Name,Code) VALUES ('Russian Federation','RU');
INSERT INTO Nation(Name,Code) VALUES ('Rwanda','RW');
INSERT INTO Nation(Name,Code) VALUES ('Saint Barthélemy','BL');
INSERT INTO Nation(Name,Code) VALUES ('Saint Helena, Ascension and Tristan da Cunha','SH');
INSERT INTO Nation(Name,Code) VALUES ('Saint Kitts and Nevis','KN');
INSERT INTO Nation(Name,Code) VALUES ('Saint Lucia','LC');
INSERT INTO Nation(Name,Code) VALUES ('Saint Martin (French part)','MF');
INSERT INTO Nation(Name,Code) VALUES ('Saint Pierre and Miquelon','PM');
INSERT INTO Nation(Name,Code) VALUES ('Saint Vincent and the Grenadines','VC');
INSERT INTO Nation(Name,Code) VALUES ('Samoa','WS');
INSERT INTO Nation(Name,Code) VALUES ('San Marino','SM');
INSERT INTO Nation(Name,Code) VALUES ('Sao Tome and Principe','ST');
INSERT INTO Nation(Name,Code) VALUES ('Saudi Arabia','SA');
INSERT INTO Nation(Name,Code) VALUES ('Senegal','SN');
INSERT INTO Nation(Name,Code) VALUES ('Serbia','RS');
INSERT INTO Nation(Name,Code) VALUES ('Seychelles','SC');
INSERT INTO Nation(Name,Code) VALUES ('Sierra Leone','SL');
INSERT INTO Nation(Name,Code) VALUES ('Singapore','SG');
INSERT INTO Nation(Name,Code) VALUES ('Sint Maarten (Dutch part)','SX');
INSERT INTO Nation(Name,Code) VALUES ('Slovakia','SK');
INSERT INTO Nation(Name,Code) VALUES ('Slovenia','SI');
INSERT INTO Nation(Name,Code) VALUES ('Solomon Islands','SB');
INSERT INTO Nation(Name,Code) VALUES ('Somalia','SO');
INSERT INTO Nation(Name,Code) VALUES ('South Africa','ZA');
INSERT INTO Nation(Name,Code) VALUES ('South Georgia and the South Sandwich Islands','GS');
INSERT INTO Nation(Name,Code) VALUES ('South Sudan','SS');
INSERT INTO Nation(Name,Code) VALUES ('Spain','ES');
INSERT INTO Nation(Name,Code) VALUES ('Sri Lanka','LK');
INSERT INTO Nation(Name,Code) VALUES ('Sudan','SD');
INSERT INTO Nation(Name,Code) VALUES ('Suriname','SR');
INSERT INTO Nation(Name,Code) VALUES ('Svalbard and Jan Mayen','SJ');
INSERT INTO Nation(Name,Code) VALUES ('Swaziland','SZ');
INSERT INTO Nation(Name,Code) VALUES ('Sweden','SE');
INSERT INTO Nation(Name,Code) VALUES ('Switzerland','CH');
INSERT INTO Nation(Name,Code) VALUES ('Syrian Arab Republic','SY');
INSERT INTO Nation(Name,Code) VALUES ('Taiwan, Province of China','TW');
INSERT INTO Nation(Name,Code) VALUES ('Tajikistan','TJ');
INSERT INTO Nation(Name,Code) VALUES ('Tanzania, United Republic of','TZ');
INSERT INTO Nation(Name,Code) VALUES ('Thailand','TH');
INSERT INTO Nation(Name,Code) VALUES ('Timor-Leste','TL');
INSERT INTO Nation(Name,Code) VALUES ('Togo','TG');
INSERT INTO Nation(Name,Code) VALUES ('Tokelau','TK');
INSERT INTO Nation(Name,Code) VALUES ('Tonga','TO');
INSERT INTO Nation(Name,Code) VALUES ('Trinidad and Tobago','TT');
INSERT INTO Nation(Name,Code) VALUES ('Tunisia','TN');
INSERT INTO Nation(Name,Code) VALUES ('Turkey','TR');
INSERT INTO Nation(Name,Code) VALUES ('Turkmenistan','TM');
INSERT INTO Nation(Name,Code) VALUES ('Turks and Caicos Islands','TC');
INSERT INTO Nation(Name,Code) VALUES ('Tuvalu','TV');
INSERT INTO Nation(Name,Code) VALUES ('Uganda','UG');
INSERT INTO Nation(Name,Code) VALUES ('Ukraine','UA');
INSERT INTO Nation(Name,Code) VALUES ('United Arab Emirates','AE');
INSERT INTO Nation(Name,Code) VALUES ('United Kingdom','GB');
INSERT INTO Nation(Name,Code) VALUES ('United States','US');
INSERT INTO Nation(Name,Code) VALUES ('United States Minor Outlying Islands','UM');
INSERT INTO Nation(Name,Code) VALUES ('Uruguay','UY');
INSERT INTO Nation(Name,Code) VALUES ('Uzbekistan','UZ');
INSERT INTO Nation(Name,Code) VALUES ('Vanuatu','VU');
INSERT INTO Nation(Name,Code) VALUES ('Venezuela, Bolivarian Republic of','VE');
INSERT INTO Nation(Name,Code) VALUES ('Viet Nam','VN');
INSERT INTO Nation(Name,Code) VALUES ('Virgin Islands, British','VG');
INSERT INTO Nation(Name,Code) VALUES ('Virgin Islands, U.S.','VI');
INSERT INTO Nation(Name,Code) VALUES ('Wallis and Futuna','WF');
INSERT INTO Nation(Name,Code) VALUES ('Western Sahara','EH');
INSERT INTO Nation(Name,Code) VALUES ('Yemen','YE');
INSERT INTO Nation(Name,Code) VALUES ('Zambia','ZM');
INSERT INTO Nation(Name,Code) VALUES ('Zimbabwe','ZW');
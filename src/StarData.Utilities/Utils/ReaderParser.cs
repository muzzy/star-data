﻿//  ReaderParser.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:40:4 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Utils
{
    public class ReaderParser<T> : IReaderParser<T>
    {
        private readonly PropertyInfo[] _properties;
        private readonly Type _type;
        private readonly Type _parseType;

        public ReaderParser()
        {
            _type = typeof(T);
            if(typeof(IEnumerable).IsAssignableFrom(_type))
            {
                _parseType = _type.GetGenericArguments()[0];
            }
            else
            {
                _parseType = _type;
            }
            _properties = _parseType.GetProperties();
        }

        public T Parse(IDataReader reader)
        {
            if (typeof(IEnumerable).IsAssignableFrom(_type))
            {
                throw new Exception("Please call ParseForCollection method instead.");
            }
            var obj = Activator.CreateInstance(_parseType);
            foreach (var prop in _properties)
            {
                var value = reader[prop.Name];
                if (!object.Equals(value, DBNull.Value))
                {
                    prop.SetValue(obj, value, null);
                }
            }

            return (T)obj;
        }

        public IEnumerable ParseForCollection(IDataReader reader)
        {
            if (!typeof(IEnumerable).IsAssignableFrom(_type))
            {
                throw new Exception("Please call Parse method instead.");
            }
            while (reader.Read())
            {
                var obj = Activator.CreateInstance(_parseType);
                foreach (var prop in _properties)
                {
                    var value = reader[prop.Name];
                    if (!object.Equals(value, DBNull.Value))
                    {
                        prop.SetValue(obj, value, null);
                    }
                }
                yield return obj;
            }
        }
       
    }
}

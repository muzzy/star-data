﻿//  SQLExecutor.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       1:22:4 30/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.CommandLine;
using System.CommandLine.IO;
using System.Data;
using System.IO.Abstractions;
using Newtonsoft.Json;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Utils
{
    public class SQLExecutor : ISQLExecutor, IDisposable
    {
        private IDbConnection _connection;
        private readonly IFileSystem _fileSystem;
        private IConsole _console;

        public SQLExecutor(IDbConnection connection, IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
            _connection = connection;
            if (!string.IsNullOrEmpty(_connection.ConnectionString))
            {
                _connection.Open();
            }
            
        }

        public void Setup(string connectString)
        {
            if(_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
            }
            _connection.ConnectionString = connectString;
            _connection.Open();
        }

        public void Dispose()
        {
            if(_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
        }

        public int ExecuteSql(string sql)
        {
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sql;
                return command.ExecuteNonQuery();
            }
        }

        public void ExecuteSqlForJson<T>(string sql, string outputFile, IReaderParser<T> parser)
        {
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sql;
                using(var reader = command.ExecuteReader())
                using(var writer = _fileSystem.File.CreateText(outputFile))
                {
                    JsonSerializer serializer = new();
                    if (typeof(IEnumerable).IsAssignableFrom(typeof(T)))
                    {
                        serializer.Serialize(writer, parser.ParseForCollection(reader));
                    }
                    else
                    {
                        serializer.Serialize(writer, parser.Parse(reader));
                    }
                        
                }
            }
        }

        public int ExecuteFile(string sqlFile, bool byLine = true, params string[] extras)
        {
            int rows = 0;
            if (!_fileSystem.File.Exists(sqlFile))
            {
                return rows;
            }
            using(var transaction = _connection.BeginTransaction())
            {
                try
                {
                    if (byLine)
                    {
                        using (var reader = _fileSystem.File.OpenText(sqlFile))
                        {
                            string line;
                            while (reader.Peek() >= 0)
                            {
                                line = reader.ReadLine();
                                if (line.StartsWith("#"))
                                {
                                    SafeWriteLine($"Loading {line} data");
                                }
                                else if (!string.IsNullOrEmpty(line))
                                {
                                    rows += DoExecution(transaction, line);
                                }
                            }
                        }
                    }
                    else
                    {
                        var sqlText = _fileSystem.File.ReadAllText(sqlFile);
                        if (!string.IsNullOrEmpty(sqlText))
                        {
                            rows += DoExecution(transaction, sqlText);
                        }
                    }

                    // Extras
                    if(extras != null)
                    {
                        foreach(var extra in extras)
                        {
                            rows += DoExecution(transaction, extra);
                        }
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
            return rows;
            
        }

        private int DoExecution(IDbTransaction transaction, string sql)
        {
            int rows = 0;
            using (var command = _connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = sql;
                rows += command.ExecuteNonQuery();
            }
            return rows;
        }

        public void AddLogging(IConsole console)
        {
            _console = console;
        }

        private void SafeWriteLine(string content)
        {
            if(_console != null)
            {
                _console.Out.WriteLine(content);
            }
        }
    }
}

﻿//  JsonPathConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       2:6:49 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace StarData.Utilities.Utils
{
    class JsonPathConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType,
                                        object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);
            object targetObj = Activator.CreateInstance(objectType);

            var contract = serializer.ContractResolver.ResolveContract(objectType) as JsonObjectContract;

            foreach (PropertyInfo prop in objectType.GetProperties()
                                                    .Where(p => p.CanRead && p.CanWrite))
            {
                // path
                JsonPropertyAttribute jpatt = prop.GetCustomAttributes(true)
                                                .OfType<JsonPropertyAttribute>()
                                                .FirstOrDefault();
                string jsonPath = (jpatt != null ? jpatt.PropertyName : prop.Name);
                JToken token = jo.SelectToken(jsonPath);

                if (token != null && token.Type != JTokenType.Null)
                {
                    object value;
                    if(prop.PropertyType.IsGenericType)
                    {
                        var itemType = prop.PropertyType.GenericTypeArguments[0];
                        var containers = token.Values<JProperty>().Values<JToken>();
                        var items = ConvertUtil.NewListOfType(itemType);

                        var property = FindProperty(contract, jpatt.PropertyName);
                        if(property.Converter != null)
                        {
                            serializer.Converters.Add(property.Converter);
                        }
                        var r = containers.Select((a, index) => {
                                var item = a.ToObject(itemType, serializer);
                                items.Add(item);
                                return item;
                            }).ToList();
                        
                        value = items;
                        if (property.Converter != null)
                        {
                            serializer.Converters.Remove(property.Converter);
                        }
                    }
                    else {
                        value = token.ToObject(prop.PropertyType, serializer);
                    }
                    

                    prop.SetValue(targetObj, value, null);
                }
            }

            return targetObj;
        }

        public override bool CanConvert(Type objectType)
        {
            // CanConvert is not called when [JsonConverter] attribute is used
            return false;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value,
                                       JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        protected JsonProperty FindProperty(JsonObjectContract contract, string propertyName)
        {
            propertyName = propertyName.Replace("_", "");
            return contract.Properties.GetClosestMatchProperty(propertyName);
        }
    }


}

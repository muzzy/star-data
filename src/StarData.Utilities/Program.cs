﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using StarData.Utilities.Utils;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Interfaces;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Text;
using System.CommandLine.IO;
using System.Data.SqlClient;
using System.IO.Abstractions;
using StarData.Utilities.Commands;
using Data = System.Data;
using System.Collections.Generic;

namespace StarData.Utilities
{
    class Program
    {
        static void Main(string[] args)
        {
            IFileSystem fileSystem = new FileSystem();
            Data.IDbConnection connection = new SqlConnection();
            var adapter = new AdapterFactory().InstanciateAdapter<FSConverterAdapter>();

            using(var executor = new SQLExecutor(connection, fileSystem))
            {
                var cmd = new RootCommand
                {
                   new ImportCommand(fileSystem, executor, adapter),
                   new ResetCommand(fileSystem, executor),
                   new Initcommand(fileSystem, executor),
                   new CleanCommand(fileSystem),
                   new ExportCommand(fileSystem,
                        executor,
                        new ReaderParser<IEnumerable<PlayerDto>>(),
                        new ReaderParser<IEnumerable<ClubDto>>())
                };
                cmd.Invoke(args);
            }
        }
    }
}

﻿//  Initcommand.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:12:4 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.IO.Abstractions;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Commands
{
    public class Initcommand : SDCommand
    {
        public Initcommand(
            IFileSystem fileSystem, ISQLExecutor executor)
            : base(fileSystem, executor, "init", "Initialize required data for starbase.")
        {
            #nullable enable
            Handler = CommandHandler.Create<string?, IConsole>(Init);
            AddOption(new Option<string?>(
                new[] { "--connection", "-c" }, "Database connection"));
        }

        private void Init(string? conn, IConsole console)
        {
            var basePath = _fileSystem.Directory.GetCurrentDirectory();
            var initFile = _fileSystem.Path.Combine(basePath, "__init__");
            if (_fileSystem.File.Exists(initFile))
            {
                console.Out.WriteLine("Star has been initialized.");
                return;
            }
            var file = _fileSystem.Path.Combine(basePath, "Seedings/Inits", "Nation.sql");
            var finalConn = FinalConnectString(conn);
            _executor.Setup(finalConn);
            _executor.AddLogging(console);
            int rows = _executor.ExecuteFile(file, false);
            if (rows != 0)
            {
                _fileSystem.File.CreateText(initFile);
            }
            console.Out.WriteLine($"Success! {rows} row(s) infected.");
        }
    }
}

﻿//  CleanCommand.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:18:56 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.IO.Abstractions;

namespace StarData.Utilities.Commands
{
    public class CleanCommand : SDCommand
    {
        public CleanCommand(
            IFileSystem fileSystem)
            : base(fileSystem, null, "clean", "Clean the generated files.")
        {
            #nullable enable
            Handler = CommandHandler.Create<string?, IConsole>(Clean);
            AddOption(new Option<string?>(new[] { "--directory", "-d" }, "Input the targe directory"));
        }

        private void Clean(string? directory, IConsole console)
        {
            var finalFolder = FinalPath(directory);
            if (string.IsNullOrEmpty(finalFolder))
            {
                finalFolder = _fileSystem.Path.Combine(
                _fileSystem.Directory.GetCurrentDirectory(), "Output");
            }
            else
            {
                finalFolder = _fileSystem.Path.Combine(finalFolder, "Output");
            }
            int count = 0;
            if (_fileSystem.Directory.Exists(finalFolder))
            {
                var files = _fileSystem.Directory.GetFiles(finalFolder, "*.sql");
                foreach (var file in files)
                {
                    _fileSystem.File.Delete(file);
                }
                count = files.Length;
            }

            console.Out.WriteLine($"{count} files were deleted.");
        }
    }
}

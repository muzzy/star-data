﻿//  SDCommand.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:5:10 23/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.CommandLine;
using System.IO.Abstractions;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Commands
{
    public class SDCommand : Command
    {
        protected readonly IFileSystem _fileSystem;
        protected readonly ISQLExecutor _executor;
        private readonly string DEFAULT_CONNECTION_STRING = "Server=localhost,1433;" +
                "User Id=sa;Password=34U8&4ie(b;Initial Catalog=starbase;";

        public SDCommand(IFileSystem fileSystem, ISQLExecutor executor, string name, string description = null) : base(name, description)
        {
            _fileSystem = fileSystem;
            _executor = executor;
        }

        protected string FinalConnectString(string conn)
        {
            return string.IsNullOrEmpty(conn) ? DEFAULT_CONNECTION_STRING : conn;
        }

        protected string FinalPath(string path)
        {
            var finalPath = path;
            if (!string.IsNullOrEmpty(path) && !path.StartsWith("\\"))
            {
                finalPath = _fileSystem.Path.Combine(_fileSystem.Directory.GetCurrentDirectory(), path);
            }
            return finalPath;
        }
    }
}

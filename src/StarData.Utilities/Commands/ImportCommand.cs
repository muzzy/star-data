﻿//  ExecCommand.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:25:38 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.IO.Abstractions;
using System.Linq;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Commands
{
    public class ImportCommand : SDCommand
    {
        private IDataAdapter _adapter;
        public ImportCommand(
            IFileSystem fileSystem, ISQLExecutor executor, IDataAdapter adapter)
            : base(fileSystem, executor, "import", "Convert data from file(s) to database.")
        {
            _adapter = adapter;
            #nullable enable
            Handler = CommandHandler.Create<string, string?, IConsole>(Exec);
            AddOption(new Option<string?>(
                new[] { "--connection", "-c" }, "Database connection"));
            AddArgument(new Argument<string>("src", "Input file or folder"));
        }

        private void Exec(string src, string? conn, IConsole console)
        {
            try
            {
                var finalSource = FinalPath(src);
                string finalConn = FinalConnectString(conn);
                if (_fileSystem.File.Exists(finalSource))
                {
                    var rows = DoExecOnFile(_adapter, finalSource, finalConn, console);
                    if (rows != 0)
                    {
                        console.Out.WriteLine($"Success! {rows} row(s) infected.");
                    }
                    return;
                }

                if (_fileSystem.Directory.Exists(finalSource))
                {
                    var rows = DoExecOnFolder(_adapter, finalSource, finalConn, console);
                    if (rows > 0)
                    {
                        console.Out.WriteLine($"Success! {rows} row(s) infected.");
                    }
                }
            }
            catch (Exception ex)
            {
                console.Error.WriteLine(ex.Message);
            }
        }

        private int DoExecOnFile(
            IDataAdapter adapter, string file, string conn, IConsole console)
        {
            int rows = 0;
            var basePath = _fileSystem.Path.GetDirectoryName(file) ?? "/";
            string targetFile = $"{_fileSystem.Path.GetFileNameWithoutExtension(file)}.sql";
            string outputFile = _fileSystem.Path.Combine(basePath, targetFile);
            adapter.ToSqlFile(file, outputFile);
            _executor.Setup(conn);
            _executor.AddLogging(console);
            string extraEntry = $"SET DATEFORMAT DMY;INSERT INTO __SeedingHistory(Name, CreateAt) " +
                    $"VALUES('{targetFile}', CONVERT(SMALLDATETIME, '{System.DateTime.Now.ToLocalTime()}'));";
            string extraClear = $"UPDATE Match SET MD5Code='';";
            var extras = new string[]
            {
                    extraClear,
                    extraEntry
            };
            rows = _executor.ExecuteFile(outputFile, true, extras);
            return rows;
        }

        private int DoExecOnFolder(
            IDataAdapter adapter, string folder, string conn, IConsole console)
        {
            int rows = 0;
            try
            {
                var rawFiles = _fileSystem.Directory.GetFiles(folder, "*.json").OrderBy(f => f);
                var outputFolder = _fileSystem.Path.Combine(folder, "Output");
                if (!_fileSystem.Directory.Exists(outputFolder))
                {
                    _fileSystem.Directory.CreateDirectory(outputFolder);
                }
                _executor.Setup(conn);
                _executor.AddLogging(console);
                foreach (var file in rawFiles)
                {
                    string targetFile = $"{_fileSystem.Path.GetFileNameWithoutExtension(file)}.sql";
                    var outputFile = _fileSystem.Path.Combine(outputFolder, targetFile);
                    adapter.ToSqlFile(file, outputFile);
                    rows += _executor.ExecuteFile(outputFile);
                }
            }
            catch
            {
                rows = -1;
            }
            return rows;
        }
    }
}

﻿//  ExportCommand.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:55:58 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.IO.Abstractions;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Utils;

namespace StarData.Utilities.Commands
{
    public class ExportCommand : SDCommand
    {
        private readonly IReaderParser<IEnumerable<PlayerDto>> _playerParser;
        private readonly IReaderParser<IEnumerable<ClubDto>> _clubParser;

        public ExportCommand(
            IFileSystem fileSystem,
            ISQLExecutor executor,
            IReaderParser<IEnumerable<PlayerDto>> playerParser,
            IReaderParser<IEnumerable<ClubDto>> clubParser)
            : base(fileSystem, executor, "export", "Export Json file from database.")
        {
            _playerParser = playerParser;
            _clubParser = clubParser;
            #nullable enable
            Handler = CommandHandler.Create<string, string?, string?, IConsole>(Export);
            AddOption(new Option<string?>(
                new[] { "--connection", "-c" }, "Database connection"));
            AddOption(new Option<string?>(
                new[] { "--type", "-t" }, "Specify export data type: player | club, default is player"));
            AddArgument(new Argument<string?>("file", "Output file path and name"));
        }

        private void Export(string file, string? conn, string? type, IConsole console)
        {
            try
            {
                _executor.Setup(FinalConnectString(conn));
                _executor.AddLogging(console);
                var finalFile = FinalPath(file);
                if (string.IsNullOrEmpty(type) || "player".Equals(type))
                {

                    _executor.ExecuteSqlForJson(
                        "DECLARE @PlayerClub TABLE(PlayerId BIGINT, ClubId INT);" +
                        "INSERT INTO @PlayerClub SELECT t1.Player, t1.Club " +
                        "FROM Transfer t1 INNER JOIN(SELECT t2.Player, MAX(t2.[Id]) as Recent " +
                        "FROM Transfer t2 GROUP BY t2.Player) t3 ON t3.Player = t1.Player " +
                        "AND t3.Recent = t1.Id;SELECT t1.Id, t1.Name, t1.Point, t3.Id ClubId, " +
                        "t3.Name ClubName FROM Player t1 LEFT JOIN @PlayerClub t2 " +
                        "ON t2.PlayerId = t1.Id LEFT JOIN Club t3 ON t3.Id = t2.ClubId " +
                        "ORDER BY t1.Point DESC;", file,
                        _playerParser);
                }else if ("club".Equals(type))
                {
                    _executor.ExecuteSqlForJson("SELECT Id, Name,Point FROM Club ORDER BY Point DESC;", file,
                        _clubParser);
                }
            }
            catch(Exception ex)
            {
                console.Error.WriteLine(ex.Message);
            }
        }
    }

    public class PlayerDto
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public long ClubId { get; set; }
        public string? ClubName { get; set; }
        public int Point { get; set; }
    }

    public class ClubDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int Point { get; set; }
    }
}

﻿//  SDCommand.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       21:54:12 23/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.IO.Abstractions;
using System.Text;
using StarData.Utilities.Interfaces;

namespace StarData.Utilities.Commands
{
    public class ResetCommand : SDCommand
    {

        public ResetCommand(
            IFileSystem fileSystem, ISQLExecutor executor)
            : base(fileSystem, executor, "reset", "Delete data in database")
        {
            #nullable enable
            Handler = CommandHandler.Create<string?, IConsole>(Reset);
            AddOption(new Option<string?>(
                new[] { "--connection", "-c" }, "Database connection"));
        }
        
        private void Reset(string? conn, IConsole console)
        {
            var finalConn = FinalConnectString(conn);
            _executor.Setup(finalConn);
            _executor.AddLogging(console);
            try
            {
                StringBuilder builder = new();
                builder.Append("truncate table starbase.dbo.__SeedingHistory;");
                builder.Append("truncate table starbase.dbo.Transfer;");
                builder.Append("truncate table starbase.dbo.Substitution;");
                builder.Append("truncate table starbase.dbo.SeasonClub;");
                builder.Append("truncate table starbase.dbo.PlayerMatch;");
                builder.Append("truncate table starbase.dbo.Foul;");
                builder.Append("truncate table starbase.dbo.Goal;");
                builder.Append("truncate table starbase.dbo.Match;");
                builder.Append("truncate table starbase.dbo.Rating;");
                builder.Append("truncate table starbase.dbo.Player;");
                builder.Append("truncate table starbase.dbo.Club;");
                builder.Append("truncate table starbase.dbo.Season;");
                builder.Append("truncate table starbase.dbo.League;");
                builder.Append("truncate table starbase.dbo.Nation;");
                int rows = _executor.ExecuteSql(builder.ToString());
                console.Out.WriteLine($"{rows} row(s) were infected.");
                if (rows != 0)
                {
                    var basePath = _fileSystem.Directory.GetCurrentDirectory();
                    var initFile = _fileSystem.Path.Combine(basePath, "__init__");
                    if (_fileSystem.File.Exists(initFile))
                    {
                        _fileSystem.File.Delete(initFile);
                    }
                }
            }
            catch (Exception ex)
            {
                console.Out.WriteLine(ex.Message);
            }

        }
    }
}

﻿//  IMatchDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:20:9 5/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;

namespace StarData.Utilities.Dtos
{
    public interface IMatchDto
    {
        public string MatchTime();
        public string MD5();
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }

        public IList<string> TeamHomePlayers { get; set; }
        public IList<string> TeamAwayPlayers { get; set; }
        public IList<string> BenchHomePlayers { get; set; }
        public IList<string> BenchAwayPlayers { get; set; }
        public IList<(string On, string Off, string Time)> SubsHome { get; set; }
        public IList<(string On, string Off, string Time)> SubsAway { get; set; }

        public IList<(string Name, int Time)> GoalHome { get; set; }
        public IList<(string Name, int Time)> GoalAway { get; set; }
        public IList<(string Name, string Card, string Type, int Time)> FoulHome { get; set; }
        public IList<(string Name, string Card, string Type, int Time)> FoulAway { get; set; }
    }
}

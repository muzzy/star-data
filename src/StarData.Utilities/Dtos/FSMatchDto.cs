﻿//  MatchDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       1:2:33 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using StarData.Utilities.Utils;

namespace StarData.Utilities.Dtos
{
    [JsonConverter(typeof(JsonPathConverter))]
    public class FSMatchDto : IMatchDto
    { 
        [JsonProperty("league")]
        public string League { get; set; }

        [JsonProperty("score.home")]
        public string HomeTeam { get; set; }

        [JsonProperty("score.away")]
        public string AwayTeam { get; set; }

        [JsonProperty("score.homeScore")]
        public int HomeScore { get; set; }

        [JsonProperty("score.awayScore")]
        public int AwayScore { get; set; }

        [JsonProperty("teamHome")]
        public IList<string> TeamHomePlayers { get; set; }

        [JsonProperty("teamAway")]
        public IList<string> TeamAwayPlayers { get; set; }

        [JsonProperty("benchHome")]
        public IList<string> BenchHomePlayers { get; set; }

        [JsonProperty("benchAway")]
        public IList<string> BenchAwayPlayers { get; set; }

        [JsonProperty("goalHome")]
        [JsonConverter(typeof(TupleConverter))]
        public IList<(string Name, int Time)> GoalHome { get; set; }

        [JsonProperty("goalAway")]
        [JsonConverter(typeof(TupleConverter))]
        public IList<(string Name, int Time)> GoalAway { get; set; }

        [JsonProperty("subsHome")]
        [JsonConverter(typeof(TupleConverter))]
        public IList<(string On, string Off, string Time)> SubsHome { get; set; }

        [JsonProperty("subsAway")]
        [JsonConverter(typeof(TupleConverter))]
        public IList<(string On, string Off, string Time)> SubsAway { get; set; }

        [JsonProperty("foulHome")]
        [JsonConverter(typeof(TupleConverter))]
        public IList<(string Name, string Card, string Type, int Time)> FoulHome { get; set; }

        [JsonProperty("foulAway")]
        [JsonConverter(typeof(TupleConverter))]
        public IList<(string Name, string Card, string Type, int Time)> FoulAway { get; set; }

        [JsonProperty("general.date")]
        public string Date { private get; set; }

        [JsonProperty("general.time")]
        public string Time { private get; set; }

        public string MatchTime()
        {
            return $"{Date} {Time}";
        }

        private string _md5;

        public string MD5()
        {
            if (string.IsNullOrEmpty(_md5))
            {
                _md5 = ConvertUtil.MD5($"{HomeTeam}{AwayTeam}{MatchTime()}");
            }
            return _md5;
        }
    }
}

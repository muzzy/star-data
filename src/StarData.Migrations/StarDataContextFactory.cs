﻿//  StarDataContextFactory.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:6:14 29/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using StarData.Infrastructure.Data;

namespace StarData.Migrations
{
    public class StarDataContextFactory : IDesignTimeDbContextFactory<StarDataContext>
    {
        public StarDataContextFactory()
        {
        }

        public StarDataContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json", true)
                 .AddEnvironmentVariables()
            .Build();

            var builder = new DbContextOptionsBuilder();

            var connectionString = configuration
                        .GetConnectionString("StarDataConnection");

            builder.UseSqlServer(connectionString,
                        x => x.MigrationsAssembly(typeof(StarDataContextFactory).Assembly.FullName));


            return new StarDataContext(builder.Options);
        }
    }
}

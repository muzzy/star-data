﻿//  IFactorDecider.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       21:56:10 26/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
namespace StarData.RatingEngine.Interface
{
    public interface IFactorDecider
    {
        int SelectKA(int rating, int numberOfGamePlayed, int age);
        decimal SelectSA(int dai);
    }

}

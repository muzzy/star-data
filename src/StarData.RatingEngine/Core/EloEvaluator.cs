﻿//  EloEvaluator.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       13:53:45 26/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using StarData.RatingEngine.Interface;

namespace StarData.RatingEngine.Core
{
    public class EloEvaluator
    {
        private IFactorDecider _decider;
        public EloEvaluator(IFactorDecider decider)
        {
            _decider = decider;
        }

        // ra: Player A rating;
        // rb: Player B rating
        // EA: (0, 1)
        public decimal CalculateEA(int ra, int rb)
        {
            return 1 / (1 + (decimal)Math.Pow(10, (rb - ra) / 400));
        }

        public int CalculateNewRA(int ra, int numberOfGamePlayed, int age, int rb, int dai)
        {
            var ea = CalculateEA(ra, rb);
            var sa = _decider.SelectSA(dai);
            var ka = _decider.SelectKA(ra, numberOfGamePlayed, age);
            var rc = ka * (sa - ea);
            return (int)(ra + rc);
        }

        public int CalculateTeamRA(IReadOnlyList<(int rating, int durationOfPlay)> players)
        {
            var sum = players.Aggregate((rmSum:0d, mSum:0d),
                (acc, p) => (acc.rmSum + p.rating * p.durationOfPlay, acc.mSum + p.durationOfPlay));
            return (int)Math.Round(sum.rmSum / sum.mSum);
        }

        /// <summary>
        /// Calculate the rating change of player A
        /// </summary>
        /// <returns>Decimal value: Resonable (-1, 1)</returns>
        public decimal CalculateCA(decimal w, int dA, int mA, int mMax, decimal eA)
        {
            if(mA > mMax)
            {
                throw new ArgumentOutOfRangeException();
            }
            var sA = _decider.SelectSA(dA);
            var wse = w * (sA - eA);
            if(dA == 0)
            {
                return wse * mA / mMax;
            }
            else
            {
                return (wse * (decimal)Math.Pow(Math.Abs(dA), 1 / 3));
            }
        }

        public decimal ReduceK(decimal k, bool isOn, bool isTransfer = false)
        {
            if (isTransfer)
            {
                return 40;
            }
            else
            {
                decimal value = isOn ? (k - 0.25M) : (k + 0.5M);
                value = Math.Min(40, value);
                value = Math.Max(24, value);
                return value;
            }
        }

        public decimal ReduceQ(decimal q, bool isOn, bool isTransfer = false)
        {
            if (isTransfer)
            {
                return 1;
            }
            else
            {
                decimal value = isOn ? (q - 0.025M) : (q + 0.025M);
                value = Math.Min(1, value);
                value = Math.Max(0.5M, value);
                return value;
            }
        }

        /// <summary>
        /// Reduce player rating to its real level
        /// </summary>
        /// <param name="rA"></param>
        /// <param name="kA">24 - 40</param>
        /// <param name="qA">
        /// Indicate the individual change in proportion to the team change: 0.5 - 1
        /// </param>
        /// <param name="mA">Player A duration of game</param>
        /// <param name="mMax">The max match time</param>
        /// <returns></returns>
        public int ReducePlayerRA(
            decimal rA,
            decimal kA,
            decimal qA,
            decimal cA,
            decimal teamCA,
            int mA,
            int mMax)
        {
            if(kA < 24 || kA > 40)
            {
                throw new ArgumentOutOfRangeException(
                            nameof(kA),
                            message: "Value shoulde be in range 24 - 40"
                        );
            }
            if(qA < 0.5M || qA > 1)
            {
                throw new ArgumentOutOfRangeException(
                            nameof(qA),
                            message: "Value shoulde be in range 0.5 - 1"
                        );
            }
            if(mA < 0 || (mA > mMax))
            {
                throw new ArgumentOutOfRangeException(
                            "mA or mMax",
                            message: "Value cannot be negative or mA > mMax"
                        );
            }
            
            return (int)(rA + kA * (qA * cA + (1 - qA) * teamCA * mA / mMax));
        }

        public int ReduceTeamRA(int rA, double cA) 
        {
            return (int)(rA + 20 * cA);
        }
    }
}

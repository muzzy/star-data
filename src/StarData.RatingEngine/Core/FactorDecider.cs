﻿//  FactorDecider.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       15:36:49 26/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using StarData.RatingEngine.Interface;

namespace StarData.RatingEngine.Core
{
    public class FactorDecider: IFactorDecider
    {

        public int SelectKA(int rating, int numberOfGamePlayed, int age)
        {
            int result = 20;
            if(numberOfGamePlayed >= 30 && rating >= 2400)
            {
                result = 10;
            }
            else if(numberOfGamePlayed < 30 || (age < 18 && rating < 2300))
            {
                result = 40;
            }

            return result;

        }

        /// <summary>
        /// Get player A's SA: indicate the actual result that Player A achieves
        /// </summary>
        /// <param name="dai">goal difference</param>
        /// <returns></returns>
        public decimal SelectSA(int dai)
        {
            return dai > 0 ? 1 : dai == 0 ? 0.5M : 0;
        }
    }
}

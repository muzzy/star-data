﻿//  IEntityContext.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       11:44:58 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using StarData.Core.Model.Base;

namespace StarData.Infrastructure.Interfaces
{
    public interface IEntityWork
    {
        DbSet<T> EntitySet<T>() where T : class, IEntity;

        T GetEntityById<T>(int id) where T : class, IEntity;
    }
}

﻿//  IUnitOfWork.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       16:59:43 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Threading.Tasks;

namespace StarData.Infrastructure.Interfaces
{
    public interface IUnitOfWork
    {
        Task Commit();
    }
}

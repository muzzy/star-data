﻿//  SDUtil.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:45:16 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace StarData.Infrastructure.Utils
{
    public static partial class SDUtil
    {
        private static readonly MD5CryptoServiceProvider _md5Provider = new MD5CryptoServiceProvider();
        public static string MD5(string input)
        {
            var builder = new StringBuilder();
            var bytes = _md5Provider.ComputeHash(new UTF8Encoding().GetBytes(input));
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            return builder.ToString();
        }

        public static IList NewListOfType(Type aType)
        {
            var openListType = typeof(List<>);
            var closedListType = openListType.MakeGenericType(aType);
            return (IList)Activator.CreateInstance(closedListType);
        }

        public static MethodInfo GetMethod(Type t)
        {
            MethodInfo method;
            if (t.IsGenericType)
            {
                if (t.ContainsGenericParameters)
                    method = t.GetMethod("Add", t.GetGenericArguments());
                // Get closed generic type arguments.
                else
                    method = t.GetMethod("Add", t.GenericTypeArguments);
            }
            else
            {
                method = t.GetMethod("Add", new Type[] { typeof(Object) });
            }

            return method;
        }

        public static string SafeName(string name)
        {
            return name?.Split("=>").Last().Trim().Replace("'","''");
        }

        public static string WrapParam(string param)
        {
            return $"'{param}'";
        }

        public static IList<T> MergeList<T>(IList<T> list1, IList<T> list2)
        {
            IList<T> result = new List<T>();
            var empty = new List<T>();
            return result.Concat(list1 ?? empty)
                .Concat(list2 ?? empty)
                .ToList();
        }

        public static IList<T> MergeList<T>(params IList<T>[] list)
        {
            IList<T> result = list?.Aggregate((a, b) => a.Concat(b).ToList());
            return result?? new List<T>();
        }

    }
}

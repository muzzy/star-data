﻿//  ClubConfiguration.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <>
//  Create at:
//       14:27:38 29/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Infrastructure.Data.Configurations
{
    public class ClubConfiguration : IEntityTypeConfiguration<Club>
    {

        public void Configure(EntityTypeBuilder<Club> builder)
        {
            builder.ToTable("Club");
            builder.Property(p => p.Point).HasDefaultValue(0);
        }
    }
}

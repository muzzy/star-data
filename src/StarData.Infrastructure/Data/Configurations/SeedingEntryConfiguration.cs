﻿//  SeedingEntryConfiguration.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <>
//  Create at:
//       14:58:13 29/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Core.Model.Entities;

namespace StarData.Infrastructure.Data.Configurations
{
    public class SeedingEntryConfiguration : IEntityTypeConfiguration<SeedingEntry>
    {
        public void Configure(EntityTypeBuilder<SeedingEntry> builder)
        {
            builder.ToTable("__SeedingHistory")
                .HasIndex(s => s.Name)
                .IsUnique();
        }
    }
}

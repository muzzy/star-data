﻿//  StarDataConfiguration.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <>
//  Create at:
//       14:2:18 29/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Model.Entities;

namespace StarData.Infrastructure.Data.Configurations
{
    public static class StarDataConfiguration 
    {
        public static ModelBuilder AddDomainBuilder(this ModelBuilder builder)
        {
            builder.ApplyConfiguration(new NationConfiguration());
            builder.ApplyConfiguration(new PlayerConfiguration());
            builder.Entity<League>().ToTable(nameof(League));
            builder.ApplyConfiguration(new ClubConfiguration());
            builder.Entity<Transfer>().ToTable(nameof(Transfer));
            builder.Entity<Season>().ToTable(nameof(Season));
            builder.Entity<SeasonClub>().ToTable(nameof(SeasonClub));
            builder.ApplyConfiguration(new MatchConfiguration());
            builder.Entity<Goal>().ToTable(nameof(Goal));
            builder.Entity<Foul>().ToTable(nameof(Foul));
            builder.Entity<Substitution>().ToTable(nameof(Substitution));
            builder.Entity<Rating>().ToTable(nameof(Rating));
            builder.Entity<PlayerMatch>().ToTable(nameof(PlayerMatch));
            return builder;
        }

        public static ModelBuilder AddSeedBuilder(this ModelBuilder builder)
        {
            builder.ApplyConfiguration(new SeedingEntryConfiguration());
            return builder;
        }
    }
}

﻿//  NationConfiguration.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       9:42:27 1/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Infrastructure.Data.Configurations
{
    public class NationConfiguration : IEntityTypeConfiguration<Nation>
    {
        public void Configure(EntityTypeBuilder<Nation> builder)
        {
            builder.ToTable("Nation");
                //.HasData(new Nation { Id = 1, Name = "England", Alias = "English" });
        }
    }
}

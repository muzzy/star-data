﻿//  PlayerConfiguration.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       11:55:30 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Infrastructure.Data.Configurations
{
    public class PlayerConfiguration : IEntityTypeConfiguration<Player>
    {
        public void Configure(EntityTypeBuilder<Player> builder)
        {
            builder.ToTable(nameof(Player));
            builder.Property(p => p.DateOfBirth)
                .HasComputedColumnSql("CONVERT(Date, '1970-01-01')");
            builder.Property(p => p.HFactor).HasDefaultValue(75m);
            builder.Property(p => p.KFactor).HasDefaultValue(32m);
            builder.Property(p => p.QFactor).HasDefaultValue(1m);
            builder.Property(p => p.NumberOfMatches).HasDefaultValue(0);
            builder.Property(p => p.Point).HasDefaultValue(0);
        }
    }
}

﻿//  MatchConfiguration.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <>
//  Create at:
//       14:31:16 29/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Infrastructure.Data.Configurations
{
    public class MatchConfiguration : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            builder.ToTable("Match");
            //builder.HasOne(m => m.HomeTeam)
            //    .WithMany(c => c.HostMatches)
            //    .OnDelete(DeleteBehavior.ClientSetNull);
            //builder.HasOne(m => m.AwayTeam)
            //    .WithMany(c => c.GuestMatches)
            //    .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}

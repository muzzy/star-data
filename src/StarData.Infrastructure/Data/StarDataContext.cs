﻿//  StarDataContext.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       11:15:28 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarData.Infrastructure.Data.Configurations;
using StarData.Infrastructure.Interfaces;

namespace StarData.Infrastructure.Data
{
    public class StarDataContext : DbContext
    {
        public StarDataContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.AddDomainBuilder();
            builder.AddSeedBuilder();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}

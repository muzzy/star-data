﻿//  UnitOfWork.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       2:6:39 10/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Threading.Tasks;
using StarData.Infrastructure.Interfaces;

namespace StarData.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StarDataContext _context;

        public UnitOfWork(StarDataContext context)
        {
            _context = context;
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }
    }
}

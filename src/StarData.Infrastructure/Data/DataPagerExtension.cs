﻿//  DataPagerExtension.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       10:30:58 14/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StarData.Core.Model.Base;
using StarData.Core.Repositories;

namespace StarData.Infrastructure.Data
{
    public static class DataPagerExtension
    {
        public static PagedResult<T> Paginate<T>(
            this IQueryable<T> query,
            int page,
            int limit,
            CancellationToken cancellation
            )
            where T : class, IEntityBase
        {
            page = (page < 0) ? 1 : page;
            var start = (page - 1) * limit;
            var data = query
                              .Skip(start)
                              .Take(limit)
                              .ToList();

            //var result = PagedResult<IList<T>>.FromData(data);
            var result = new PagedResult<T>
            {
                PageData = data,
                PageNumber = page,
                PageSize = limit,
                TotalItems =  query.Count()
            };
            result.TotalPages = (int)Math.Ceiling(result.TotalItems / (double)limit);

            return result;
        }
    }
}

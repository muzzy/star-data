﻿//  StarDataCollectionExtension.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       20:27:6 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using StarData.Infrastructure.Interfaces;
using StarData.Core.Repositories;
using StarData.Infrastructure.Repositories;
using StarData.Infrastructure.Utils;
using StarData.Core.Model.Entities;
using StarData.Core.Services;
using StarData.Infrastructure.Services;
using System.Globalization;

namespace StarData.Infrastructure.Data
{
    public static class StarDataExtension
    {
        public static void AddStarDataCore(this IServiceCollection services)
        {
            services.AddScoped<IFileSystem, FileSystem>();
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.TryAddScoped(typeof(IDao<>), typeof(DaoAgent<>));
            services.TryAddScoped(typeof(IEntityDao<>), typeof(EntityDao<>));
            services.TryAddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.TryAddScoped<IUnitOfWork, UnitOfWork>();
            services.TryAddScoped<IMatchRepository, MatchRepository>();
            services.TryAddScoped<IClubRepository, ClubRepository>();
            services.TryAddScoped<IPlayerRepository, PlayerRepository>();
            services.TryAddScoped<IRatingRepository, RatingRepository>();
            services.TryAddScoped<IDomainService, DomainService>();
        }

        public static void SeedStarData(this StarDataContext context)
        {
            try
            {
                //context.Database.EnsureDeleted();
                context.Database.Migrate();
                DoSeeding(context);
            }
            catch
            {
                throw;
            }
        }
        private static void DoSeeding(StarDataContext context)
        {
            //var executor = new SQLExecutor(context);
            //string path = AppDomain.CurrentDomain.BaseDirectory;

            //using (var transaction = context.Database.BeginTransaction())
            //{
            //    try
            //    {
            //        DoExtraSeedings(executor,
            //            Path.Combine(path, "Seedings/Nations.csv"), InsertSQL);
            //        DoExtraSeedings(executor,
            //            Path.Combine(path, "Seedings/Players.csv"), UpdateSQL);
            //        transaction.Commit();
            //        context.SaveChanges();
            //    }
            //    catch (Exception e)
            //    {
            //        transaction.Rollback();
            //        Console.WriteLine(e.Message);
            //        throw;
            //    }
            //}
        }

        private static Func<string, string> UpdateSQL = s =>
        {
            var args = s.Split(",");
            return $"update Player SET Player.DateOfBirth=CONVERT(date,'{args[1]}')," +
                   $"Player.NationId = (select Id from Nation where Name = '{args[2]}' )" +
                   $"from Player where Player.Name = '{args[0]}';";
        };
        private static Func<string, string> InsertSQL = s =>
        {
            return $"insert Nation(Name, Alias) select '{s}','{s}' " +
                   $"where not exists(select 1 from Nation where Name='{s}' or Alias='{s}');";
        };

        //private static void DoExtraSeedings(
        //    SQLExecutor executor,
        //    string file,
        //    Func<string, string> sql)
        //{
        //    var sqlBuilder = new StringBuilder();
        //    using (var reader = File.OpenText(file))
        //    {
        //        while (reader.Peek() >= 0)
        //        {
        //            sqlBuilder.Append(sql(reader.ReadLine()));
        //        }
        //    }
        //    if(sqlBuilder.Length > 0)
        //    {
        //        executor.ExecuteRaw(sqlBuilder.ToString());
        //    }
        //}
    }
}

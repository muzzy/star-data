﻿//  InfrastructureMappings.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       13:33:36 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using AutoMapper;
using StarData.Core.Model.Entities;
using StarData.Core.Model.VOs;

namespace StarData.Infrastructure.MappingProfiles
{
    public class InfrastructureMappings : Profile
    {
        public InfrastructureMappings()
        {
            CreateMap<Goal, GoalVO>().ReverseMap();
        }
    }
}

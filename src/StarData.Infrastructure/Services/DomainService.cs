﻿//  DomainService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       11:1:18 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Model.Entities;
using StarData.Core.Repositories;
using StarData.Core.Services;
using StarData.Infrastructure.Data;
using StarData.Infrastructure.Interfaces;

namespace StarData.Infrastructure.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainService : IDomainService
    {
        #region Private Fields
        private readonly IMatchRepository _matchRepository;
        private readonly IPlayerRepository _playerRepository;
        #endregion
        public DomainService(
            IMatchRepository matchRepository,
            IPlayerRepository playerRepository)
        {
            _matchRepository = matchRepository;
            _playerRepository = playerRepository;
        }

        public Match GetMatch(int id)
        {
            //var match = _matchRepository.GetById(id);
            //var players = _entityWork.EntitySet<PlayerMatch>().Where(pm => pm.MatchId == id).ToList();
            //var substitutions = _entityWork.EntitySet<Substitution>().Where(s => s.MatchId == id).ToList();
            //var homePlayers = new List<(int Id, int On, int Off)>();
            //var awayPlayers = new List<(int Id, int On, int Off)>();
            //players.ForEach(p =>
            //{
            //    var player = (p.Id, On: 0, Off: 90);
            //    var subOff = substitutions.Where(s => s.OffPlayerId == p.Id).FirstOrDefault();
            //    var subOn = substitutions.Where(s => s.OnPlayerId == p.Id).FirstOrDefault();
            //    player.On = subOn != null ? subOn.Time : player.On;
            //    player.Off = subOff != null ? subOff.Time : player.Off;
            //    if(p.ClubId == match.HomeTeamId)
            //    {
            //        homePlayers.Add(player);
            //    }
            //    else
            //    {
            //        awayPlayers.Add(player);
            //    }

            //});
            //match.HomePlayers = homePlayers;
            //match.AwayPlayers = awayPlayers;
            //return match;
            return null;
        }
    }
}

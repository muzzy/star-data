﻿//  DaoAgent.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:27:57 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Model.Base;
using StarData.Core.Repositories;
using StarData.Core.Specifications;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Repositories
{
    public class DaoAgent<T> : IDao<T> where T : class, IEntityBase
    {
        protected readonly StarDataContext _context;
        protected readonly ILogger _logger;

        public DaoAgent(
            StarDataContext context,
            ILogger<DaoAgent<T>> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void Create(T t)
        {
            _context.Set<T>().Add(t);
        }

        public bool Exists(ISpecification<T> specification)
        {
            var count = _context.Set<T>().Count(specification.IsSatisfiedBy);
            return count != 0;
        }

        public bool Exists(Func<T, bool> predicate)
        {
            var count = _context.Set<T>().Count(predicate);
            return count != 0;
        }

        public IEnumerable<T> GetAllAsync(ISpecification<T> spec)
        {
            return GetAllAsync(spec?.Expression);
        }

        public IEnumerable<T> GetAllAsync(Expression<Func<T, bool>> cond)
        {
            var query = _context.Set<T>().AsNoTracking().AsQueryable();
            if (cond != null)
            {
                query = _context.Set<T>().Where(cond);
            }
            
            return query.AsEnumerable();
        }

        public async Task<IEnumerable<T>> GetAllAsync(
            ISpecification<T> spec,
            Expression<Func<T, dynamic>> sortPredicate,
            SortOrder sortOrder = SortOrder.Ascending)
        {
            var query = _context.Set<T>().Where(spec.Expression);
            if (sortPredicate != null)
            {
                switch (sortOrder)
                {
                    case SortOrder.Ascending:
                        query =  query.OrderBy(sortPredicate);
                        break;
                    case SortOrder.Descending:
                        query =  query.OrderByDescending(sortPredicate);
                        break;
                    default:
                        break;
                }
            }

            return await query.ToListAsync();
        }

        public PagedResult<T> GetAllAsync(
            Func<T, bool> conds,
            Func<T, dynamic> sorts,
            SortOrder sortOrder, int pageNumber, int pageSize)
        {
            var query = _context.Set<T>().Where(conds);
            var skip = (pageNumber - 1) * pageSize;
            var take = pageSize;
            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    query = query.OrderBy(sorts);
                    break;
                case SortOrder.Descending:
                    query = query.OrderByDescending(sorts);
                    break;
                default:
                    break;
            }
            var cancellation = default(CancellationToken);
            return query.AsQueryable<T>().AsNoTracking().Paginate<T>(pageNumber, pageSize, cancellation);
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(a => a.Id == id);
        }

        public T GetLastAsync<F>(
            Func<T, F> order, ISpecification<T> condSpec)
        {
            return _context.Set<T>().Where(condSpec.Expression).OrderBy<T, F>(order).LastOrDefault();
        }
        
        public void UpdateAsync(T t)
        {
            //_context.Attach(t).State = EntityState.Modified;
            _context.Entry<T>(t).State = EntityState.Modified;
            //_context.SaveChanges();
        }
    }
}

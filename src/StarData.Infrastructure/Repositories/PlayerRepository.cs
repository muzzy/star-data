﻿//  PlayerRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:8:16 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Repositories
{
    public class PlayerRepository : Repository<Player>, IPlayerRepository
    {
        public PlayerRepository(
            IDao<Player> dao,
            IEnumerable<IDataValidator<Player>> validators,
            ILogger<Repository<Player>> logger) : base(dao, validators, logger)
        {
        }

    }
}

﻿//  ClubRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:51:18 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;

namespace StarData.Infrastructure.Repositories
{
    public class ClubRepository : Repository<Club>, IClubRepository
    {
        public ClubRepository(
            IDao<Club> dao,
            IEnumerable<IDataValidator<Club>> validators,
            ILogger<Repository<Club>> logger) : base(dao, validators, logger)
        {
        }
    }
}

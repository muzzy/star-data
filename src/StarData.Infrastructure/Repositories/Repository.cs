﻿//  Repository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       15:49:6 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StarData.Core.Error;
using StarData.Core.Model.Base;
using StarData.Core.Repositories;
using StarData.Core.Specifications;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Repositories
{
    public class Repository<E> : IRepository<E> where E : class, IAggregateRoot
    {
        private readonly IDao<E> _dao;
        private readonly IEnumerable<IDataValidator<E>> _validators;
        private DataErrorDescriber _describer;

        private bool _disposed;
        public Repository(
            IDao<E> dao,
            IEnumerable<IDataValidator<E>> validators,
            ILogger<Repository<E>> logger)
        {
            _dao = dao;
            _validators = validators;
        }

        public void Create(E e)
        {
            _dao.Create(e);
        }

        //public async Task<DataResult<E>> CreateAsync(E entity, CancellationToken cancellationToken = default)
        //{
        //    // validate data for creation
        //    if (_validators != null)
        //    {
        //        foreach (var v in _validators)
        //        {
        //            var result = await v.ValidateAsync(this, entity);
        //            // return if errors exist
        //            if (!result.Succeeded)
        //            {
        //                return result;
        //            }
        //        }
        //    }
        //    // create if validate successful
        //    var resultEntity = _context.Add(entity);

        //    await _context.SaveChangesAsync(cancellationToken);

        //    return DataResult<E>.FromData(resultEntity.Entity);
        //}

        public void Dispose()
        {
            _disposed = true;
        }

        public bool Exists(ISpecification<E> specification)
        {
            return _dao.Exists(specification);
        }

        public bool Exists(Func<E, bool> predicate)
        {
            return _dao.Exists(predicate);
        }

        //public async Task<PagedResult<IList<E>>> GetAllAsync(
        //    int pageNumber = -1,
        //    int pageSize = 10,
        //    CancellationToken cancellationToken = default)
        //{
        //    cancellationToken.ThrowIfCancellationRequested();
        //    ThrowIfDisposed();
        //    try
        //    {
        //        IQueryable<E> query = _context.Set<E>().AsQueryable<E>();

        //        var result = await query.AsNoTracking()
        //                              .PaginateAsync<E>(pageNumber, pageSize, cancellationToken);

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.Message);
        //        _describer = _describer == null ? new DataErrorDescriber() : _describer;
        //        return PagedResult<IList<E>>.Failed(_describer.FromException(ex));
        //    }
        //}

        public IEnumerable<E> GetAllAsync(ISpecification<E> spec = null)
        {
            return _dao.GetAllAsync(spec);
        }

        public async Task<IEnumerable<E>> GetAllAsync(
            ISpecification<E> spec,
            Expression<Func<E, dynamic>> sortPredicate,
            SortOrder sortOrder = SortOrder.Ascending)
        {
            return await _dao.GetAllAsync(spec, sortPredicate, sortOrder);
        }

        public PagedResult<E> GetAllAsync(Func<E, bool> conds, Func<E, dynamic> sorts, SortOrder sortOrder, int pageNumber, int pageSize)
        {
            return _dao.GetAllAsync(conds, sorts, sortOrder, pageNumber, pageSize);
        }

        public IEnumerable<E> GetAllAsync(Expression<Func<E, bool>> cond = null)
        {
            return _dao.GetAllAsync(cond);
        }

        public virtual Task<E> GetByIdAsync(int id)
        {
            return _dao.GetByIdAsync(id);
        }

        public E GetLastAsync<F>(Func<E, F> order, ISpecification<E> condSpec)
        {
            return _dao.GetLastAsync(order, condSpec);
        }

        public void UpdateAsync(E t)
        {
            _dao.UpdateAsync(t);
        }

        protected void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }
    }
}

﻿//  MatchRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:12:51 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Model.Entities;
using StarData.Core.Model.VOs;
using StarData.Core.Repositories;
using StarData.Core.Specifications;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Repositories
{
    public class MatchRepository : Repository<Match>, IMatchRepository
    {
        private readonly IEntityDao<PlayerMatch> _playerMatchDao;
        private readonly IEntityDao<Substitution> _substitutionDao;
        private readonly IEntityDao<Goal> _goalDao;
        private readonly IEntityDao<Foul> _foulDao;
        private readonly IMapper _mapper;

        public MatchRepository(
            IDao<Match> dao,
            IEntityDao<PlayerMatch> playerMatchDao,
            IEntityDao<Substitution> substitutionDao,
            IEntityDao<Goal> goalDao,
            IEntityDao<Foul> foulDao,
            IEnumerable<IDataValidator<Match>> validators,
            IMapper mapper,
            ILogger<Repository<Match>> logger) : base(dao, validators, logger)
        {
            _playerMatchDao = playerMatchDao;
            _substitutionDao = substitutionDao;
            _goalDao = goalDao;
            _foulDao = foulDao;
            _mapper = mapper;
        }

        public override async Task<Match> GetByIdAsync(int id)
        {
            var match =  await base.GetByIdAsync(id);
            if(match != null)
            {
                InnerLoad(match);
            }
            
            return match;
        }

        public void LoadById(Match match)
        {
            if(match != null)
            {
                InnerLoad(match);
            }
            
        }

        private void InnerLoad(Match match)
        {
            #region Load goals
            var players = _playerMatchDao.GetAllAsync(
                Specification<PlayerMatch>.Eval(pm => pm.MatchId == match.Id));
            var substitutions = _substitutionDao.GetAllAsync(
                Specification<Substitution>.Eval(s => s.MatchId == match.Id))
                .ToList();
            var homeGoals = _goalDao.GetAllAsync(
                Specification<Goal>.Eval(s => s.MatchId == match.Id && s.ClubId == match.HomeTeamId));
            var awayGoals = _goalDao.GetAllAsync(
                Specification<Goal>.Eval(s => s.MatchId == match.Id && s.ClubId == match.AwayTeamId));
            match.HomeGoals = _mapper.Map<IEnumerable<Goal>, IEnumerable<GoalVO>>(homeGoals);
            match.AwayGoals = _mapper.Map<IEnumerable<Goal>, IEnumerable<GoalVO>>(awayGoals);
            match.Goals = _goalDao.GetAllAsync(
                Specification<Goal>.Eval(s => s.MatchId == match.Id));
            var fouls = _foulDao.GetAllAsync(f => f.MatchId == match.Id).ToList();
            #endregion

            #region Load players
            var homePlayers = new List<PlayerVO>();
            var awayPlayers = new List<PlayerVO>();
            foreach (var p in players)
            {
                var player = new PlayerVO();
                player.Id = p.PlayerId;
                player.On = p.IsBench ? 91 : 0;
                player.Off = 90;

                // Substitution
                var subOff = substitutions.SingleOrDefault(s => s.OffPlayerId == p.PlayerId);
                var subOn = substitutions.SingleOrDefault(s => s.OnPlayerId == p.PlayerId);
                player.On = subOn != null ? subOn.Time : player.On;
                player.Off = subOff != null ? subOff.Time : player.Off;

                // Foul
                var redCardFoul = fouls.SingleOrDefault(f => f.PlayerId == p.PlayerId);
                player.Off = redCardFoul != null ? redCardFoul.Time : player.Off;

                if(player.On == 91)
                {
                    continue;
                }

                if (p.ClubId == match.HomeTeamId)
                {
                    homePlayers.Add(player);
                }
                else
                {
                    awayPlayers.Add(player);
                }
            }

            match.HomePlayers = homePlayers;
            match.AwayPlayers = awayPlayers;
            #endregion
        }
    }
}

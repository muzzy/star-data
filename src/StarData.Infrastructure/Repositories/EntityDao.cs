﻿//  EntityDao.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:19:52 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using StarData.Core.Model.Base;
using StarData.Core.Repositories;
using StarData.Core.Specifications;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Repositories
{
    public class EntityDao<T> : IEntityDao<T> where T : class, IEntity
    {
        private readonly ILogger<EntityDao<T>> _logger;
        private readonly IDao<T> _dao;
        public EntityDao(
            IDao<T> dao,
            ILogger<EntityDao<T>> logger)
        {
            _dao = dao;
            _logger = logger;
        }

        public void Create(T t)
        {
            _dao.Create(t);
        }

        public bool Exists(ISpecification<T> specification)
        {
            return _dao.Exists(specification);
        }

        public bool Exists(Func<T, bool> predicate)
        {
            return _dao.Exists(predicate);
        }

        public IEnumerable<T> GetAllAsync(ISpecification<T> spec)
        {
            return _dao.GetAllAsync(spec);
        }

        public async Task<IEnumerable<T>> GetAllAsync(
            ISpecification<T> spec,
            Expression<Func<T, dynamic>> sortPredicate,
            SortOrder sortOrder = SortOrder.Ascending)
        {
            return await _dao.GetAllAsync(spec, sortPredicate, sortOrder);
        }

        public PagedResult<T> GetAllAsync(Func<T, bool> conds, Func<T, dynamic> sorts, SortOrder sortOrder, int pageNumber, int pageSize)
        {
            return _dao.GetAllAsync(conds, sorts, sortOrder, pageNumber, pageSize);
        }

        public IEnumerable<T> GetAllAsync(Expression<Func<T, bool>> cond = null)
        {
            return _dao.GetAllAsync(cond);
        }

        public Task<T> GetByIdAsync(int id)
        {
            return _dao.GetByIdAsync(id);
        }

        public T GetLastAsync<F>(Func<T, F> order, ISpecification<T> condSpec)
        {
            return _dao.GetLastAsync(order, condSpec);
        }

        public void UpdateAsync(T t)
        {
            _dao.UpdateAsync(t);
        }
    }
}

﻿//  RatingRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       8:53:6 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;

namespace StarData.Infrastructure.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(
            IDao<Rating> dao,
            IEnumerable<IDataValidator<Rating>> validators,
            ILogger<Repository<Rating>> logger) : base(dao, validators, logger)
        {
        }
    }
}

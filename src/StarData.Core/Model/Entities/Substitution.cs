﻿//  Substitution.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       2:59:9 31/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    public class Substitution : Entity, IMatchRelation
    {
        [Column("Match")]
        public int MatchId { get; set; }
        [Column("OnPlayer")]
        public int OnPlayerId { get; set; }
        [Column("OffPlayer")]
        public int OffPlayerId { get; set; }
        public int Time { get; set; }
    }
}

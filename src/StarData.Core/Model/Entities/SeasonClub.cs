﻿//  SeasonClub.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:49:52 6/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    public class SeasonClub : Entity
    {
        [Column("Season")]
        public int SeasonId { get; set; }
        [Column("Club")]
        public int ClubId { get; set; }
    }
}

﻿//  Foul.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       3:13:31 31/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    /// <summary>
    /// Record Red Card
    /// </summary>
    public class Foul : Entity
    {
        [Column("Match")]
        public int MatchId { get; set; }
        [Column("Player")]
        public int PlayerId { get; set; }
        public int Time { get; set; }
    }
}

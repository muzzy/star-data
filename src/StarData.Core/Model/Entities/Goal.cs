﻿//  Goal.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       3:6:48 31/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    public class Goal : Entity, IMatchRelation
    {
        [Column("Match")]
        public int MatchId { get; set; }
        [Column("Player")]
        public int PlayerId { get; set; }
        [Column("Club")]
        public int ClubId { get; set; }
        public int Time { get; set; }
    }
}

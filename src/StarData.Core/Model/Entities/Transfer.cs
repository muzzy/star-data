﻿//  Transfer.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       1:53:41 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    public class Transfer : Entity
    {
        [Column("Player")]
        public int PlayerId { get; set; }

        [Column("Club")]
        public int ClubId { get; set; }

        public DateTime? Time { get; set; }
    }
}

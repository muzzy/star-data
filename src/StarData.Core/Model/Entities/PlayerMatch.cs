﻿//  PlayerMatch.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       17:37:27 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    public class PlayerMatch : Entity, IMatchRelation
    {
        [Column("Player")]
        public int PlayerId { get; set; }
        [Column("Match")]
        public int MatchId { get; set; }
        [Column("Club")]
        public int ClubId { get; set; }
        public bool IsBench { get; set; }
    }
}

﻿//  SeedingEntry.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <>
//  Create at:
//       14:52:6 29/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.Entities
{
    public class SeedingEntry : Entity
    {
        
        public string Name { get; set; }
        [Column(TypeName = "SmallDateTime")]
        public DateTime CreateAt { get; set; }
    }
}

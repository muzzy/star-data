﻿//  IEntityBase.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       23:6:35 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Model.Base
{
    public interface IEntityBase
    {
        int Id { get; }
    }
}

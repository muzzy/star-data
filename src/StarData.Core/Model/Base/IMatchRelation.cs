﻿//  IMatchRelation.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       12:40:9 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Model.Base
{
    public interface IMatchRelation
    {
        int MatchId { get; }
    }
}

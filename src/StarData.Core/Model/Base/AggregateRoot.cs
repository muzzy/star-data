﻿//  AggregateRoot.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:14:57 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Model.Base
{
    public class AggregateRoot : IAggregateRoot
    {
        public int Id { get; set; }
    }
}

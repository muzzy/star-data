﻿//  IAggregateRoot.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:13:26 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Model.Base
{
    public interface IAggregateRoot : IEntityBase
    {
        
    }
}

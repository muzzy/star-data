﻿//  Entity.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:31:54 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;

namespace StarData.Core.Model.Base
{
    public class Entity : IEntity
    {
        public int Id { get; set; }
    }
}

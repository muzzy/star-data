﻿//  GoalVO.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       11:36:37 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Model.VOs
{
    public class GoalVO
    {
        public int Player { get; set; }
        public int Time { get; set; }
    }
}

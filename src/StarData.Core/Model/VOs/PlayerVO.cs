﻿//  PlayerVO.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       8:16:16 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Model.VOs
{
    public class PlayerVO
    {
        public int Id { get; set; }
        public int On { get; set; }
        public int Off { get; set; }
    }
}

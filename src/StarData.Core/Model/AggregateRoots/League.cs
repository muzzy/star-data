﻿//  MatchCategory.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       12:58:3 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.AggregateRoots
{
    public class League : AggregateRoot
    {

        public string Name { get; set; }
    }
}

﻿//  Rating.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:59:51 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.AggregateRoots
{
    public class Rating : AggregateRoot
    {
        public int MatchId { get; set; }
        public DateTime Time { get; set; }
        [NotMapped]
        public Match MatchData { get; set; }
    }
}

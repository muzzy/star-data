﻿//  Nation.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       12:27:55 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.ComponentModel.DataAnnotations;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.AggregateRoots
{
    public class Nation : AggregateRoot
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public string Alias { get; set; }
    }
}

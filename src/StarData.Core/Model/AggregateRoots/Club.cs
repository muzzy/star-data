﻿//  Club.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       12:33:35 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.AggregateRoots
{
    public class Club : AggregateRoot
    {

        [Required]
        public string Name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EstablishedTime { get; set; }

        [Column("Nation")]
        public int NationId { get; set; }
        [Column("League")]
        public int LeagueId { get; set; }
        public int Point { get; set; }
        
    }
}

﻿//  Player.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       11:27:34 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;

namespace StarData.Core.Model.AggregateRoots
{
    public class Player : AggregateRoot
    {
        [Required]
        public string Name { get; set; }
        [Column(TypeName = "SmallDateTime")]
        public DateTime DateOfBirth { get; set; }
        public int? NationId { get; set; }
        public int Point { get; set; }
        public int NumberOfMatches { get; set; }
        public decimal HFactor { get; set; }
        public decimal KFactor { get; set; }
        public decimal QFactor { get; set; }
    }
}

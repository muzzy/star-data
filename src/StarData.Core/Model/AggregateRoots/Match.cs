﻿//  Match.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       13:5:28 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using StarData.Core.Model.Base;
using StarData.Core.Model.Entities;
using StarData.Core.Model.VOs;

namespace StarData.Core.Model.AggregateRoots
{
    public class Match : AggregateRoot
    {
        [Column("Season")]
        public int SeasonId { get; set; }
        [Column("League")]
        public int LeagueId { get; set; }

        public int Round { get; set; }
        [Column("HomeTeam")]
        public int HomeTeamId { get; set; }

        [Column("AwayTeam")]
        public int AwayTeamId { get; set; }

        public int HomeGoal { get; set; }
        public int AwayGoal { get; set; }

        [NotMapped]
        public IEnumerable<GoalVO> HomeGoals { get; set; }
        [NotMapped]
        public IEnumerable<GoalVO> AwayGoals { get; set; }

        [NotMapped]
        public IEnumerable<Goal> Goals { get; set; }

        [NotMapped]
        public IEnumerable<PlayerVO> HomePlayers;
        [NotMapped]
        public IEnumerable<PlayerVO> AwayPlayers;

        [Column(TypeName ="SmallDateTime")]
        public DateTime Time { get; set; }

        // HomeTeam & AwayTeam & Time
        public string MD5Code { get; set; }
    }
   
}

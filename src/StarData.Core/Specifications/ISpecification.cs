﻿//  ISpecification.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       8:0:41 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Linq.Expressions;

namespace StarData.Core.Specifications
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T candidate);
        Expression<Func<T, bool>> Expression { get; }
    }
}

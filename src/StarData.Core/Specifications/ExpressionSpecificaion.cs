﻿//  ExpressionSpecificaion.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       9:53:18 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Linq.Expressions;

namespace StarData.Core.Specifications
{
    public sealed class ExpressionSpecification<T> : Specification<T>
    {
        private readonly Expression<Func<T, bool>> _expression;
        public ExpressionSpecification(Expression<Func<T,bool>> expression)
        {
            this._expression = expression;
        }

        public override Expression<Func<T, bool>> Expression
        {
            get { return _expression; }
        }
    }
}

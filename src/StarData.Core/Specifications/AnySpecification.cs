﻿//  AnySpecification.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:30:55 11/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Linq.Expressions;

namespace StarData.Core.Specifications
{
    public class AnySpecification<T> : Specification<T>
    {
        public override Expression<Func<T, bool>> Expression
        {
            get
            {
                return a => true;
            }
        }
    }
}

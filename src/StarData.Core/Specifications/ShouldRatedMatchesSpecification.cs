﻿//  NextMatchSpecification.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       1:19:0 11/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Core.Specifications
{
    public class ShouldRatedMatchesSpecification : Specification<Match>
    {
        public Match TargetMatch { get; set; }

        public override Expression<Func<Match, bool>> Expression
        {
            get
            {
                if(TargetMatch == null)
                {
                    throw new InvalidOperationException("Target match is required.");
                }
                Expression<Func<Match, bool>> homeClubMatches =
                 m => m.HomeTeamId == TargetMatch.HomeTeamId || m.AwayTeamId == TargetMatch.HomeTeamId;
                Expression<Func<Match, bool>> awayClubMatches =
                    m => m.HomeTeamId == TargetMatch.AwayTeamId || m.AwayTeamId == TargetMatch.AwayTeamId;
                Expression<Func<Match, bool>> shouldRatedMatches =
                    m => m.Time < TargetMatch.Time;
                return homeClubMatches.Or(awayClubMatches).And(shouldRatedMatches);

            }
        }
    }
}

﻿//  Specification.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       9:56:31 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Linq.Expressions;

namespace StarData.Core.Specifications
{
    public abstract class Specification<T> : ISpecification<T>
    {
        public static Specification<T> Eval(Expression<Func<T, bool>> expression)
        {
            return new ExpressionSpecification<T>(expression);
        }

        public abstract Expression<Func<T, bool>> Expression { get; }

        public bool IsSatisfiedBy(T candidate)
        {
            return this.Expression.Compile()(candidate);
        }
    }
}

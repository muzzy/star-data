﻿//  PageResult.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:6:2 11/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Collections.Generic;

namespace StarData.Core.Repositories
{
    public class PagedResult<T> : IEnumerable<T>
    {
        #region Public properties
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public List<T> PageData { get; set; }
        #endregion

        public PagedResult()
        {
            PageData = new List<T>();
        }

        public PagedResult(int totalItems, int totalPages, int pageSize, int pageNumber, List<T> data)
        {
            this.TotalPages = totalPages;
            this.TotalItems = totalItems;
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            this.PageData = data;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return PageData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return PageData.GetEnumerator();
        }
    }
}

﻿//  EntityDao.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:14:27 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using StarData.Core.Model.Base;
using StarData.Core.Specifications;

namespace StarData.Core.Repositories
{
    public interface IEntityDao<T> : IDao<T> where T : class, IEntity
    {

    }
}

﻿//  IClubRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:50:23 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Core.Repositories
{
    public interface IClubRepository : IRepository<Club>
    {
    }
}

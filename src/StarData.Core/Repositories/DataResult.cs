﻿//  DataResult.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       15:53:27 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using StarData.Core.Error;
using StarData.Core.Model.Base;

namespace StarData.Core.Repositories
{
    public class DataResult<E>
    {
        protected List<DataError> _errors = new List<DataError>();
        public IEnumerable<DataError> Errors => _errors;
        public bool Succeeded { get; protected set; }

        private static readonly DataResult<E> _success = new DataResult<E> { Succeeded = true };
        public static DataResult<E> Success => _success;

        public E Data { get; protected set; }

        public static DataResult<E> Failed(params DataError[] errors)
        {
            var result = new DataResult<E> { Succeeded = false };
            if (errors != null)
            {
                result._errors.AddRange(errors);

            }
            return result;
        }

        public static DataResult<E> FromData(E data)
        {
            var result = new DataResult<E> { Succeeded = true };
            result.Data = data;
            return result;
        }

    }
}

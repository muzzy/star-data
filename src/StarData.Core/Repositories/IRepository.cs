﻿//  IRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       13:35:58 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StarData.Core.Model.Base;
using StarData.Core.Specifications;

namespace StarData.Core.Repositories
{
    public interface IRepository<T> : IDao<T>, IDisposable where T : IAggregateRoot
    {

        
    }
}

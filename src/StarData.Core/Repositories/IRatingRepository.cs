﻿//  IRatingRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:57:52 6/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Core.Repositories
{
    public interface IRatingRepository : IRepository<Rating>
    {
        
    }
}

﻿//  IDao.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:42:32 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using StarData.Core.Specifications;

namespace StarData.Core.Repositories
{
    public interface IDao<T>
    {
        IEnumerable<T> GetAllAsync(ISpecification<T> spec);
        Task<IEnumerable<T>> GetAllAsync(ISpecification<T> spec,
            Expression<Func<T, dynamic>> sortPredicate, SortOrder sortOrder = SortOrder.Ascending);
        IEnumerable<T> GetAllAsync(Expression<Func<T, bool>> cond = null);
        Task<T> GetByIdAsync(int id);

        void Create(T t);

        void UpdateAsync(T t);

        bool Exists(ISpecification<T> spec);
        bool Exists(Func<T, bool> predicate);

        public T GetLastAsync<F>(Func<T, F> order, ISpecification<T> condSpec);

        #region Paged support

        PagedResult<T> GetAllAsync(
            Func<T, bool> conds,
            Func<T, dynamic> sorts,
            SortOrder sortOrder, int pageNumber, int pageSize);

        #endregion

    }
}

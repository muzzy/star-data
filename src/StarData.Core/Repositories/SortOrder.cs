﻿//  SortOrder.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:9:22 11/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Core.Repositories
{
    public enum SortOrder
    {
        UnSpecified = -1,
        Ascending = 0,
        Descending = 1
    }
}

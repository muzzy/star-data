﻿//  IDataValidator.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       16:20:35 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Threading.Tasks;
using StarData.Core.Model.Base;

namespace StarData.Core.Repositories
{
    public interface IDataValidator<T> where T : IAggregateRoot
    {
        Task<DataResult<T>> ValidateAsync(IRepository<T> repository, T root);
    }
}

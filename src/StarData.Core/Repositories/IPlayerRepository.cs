﻿//  IPlayerRepository.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:6:11 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Core.Repositories
{
    public interface IPlayerRepository : IRepository<Player>
    {
        
    }
}

﻿//  IDomainService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       10:58:20 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Core.Services
{
    public interface IDomainService
    {
        Match GetMatch(int Id);
    }
}

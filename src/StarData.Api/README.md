# StarData Application

## DevOps

## Useful Docker Commands

Build image: docker build --no-cache -t stardata .
Build image from specific path: docker build --no-cache -t stardata  -f ./src/StarData.Api/Dockerfile .

Run image as container: docker run -it stardata:latest /bin/bash 
Show container list: docker container ls -a
Swith to container context: docker exec -it containerID /bin/bash
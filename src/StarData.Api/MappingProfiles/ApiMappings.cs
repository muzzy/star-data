﻿//  StarMappings.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       11:34:58 14/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using AutoMapper;
using StarData.Api.Dtos;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Api.MappingProfiles
{
    public class ApiMappings : Profile
    {
        public ApiMappings()
        {
            //    CreateMap<Player, PlayerCreateDto>().ReverseMap();
            CreateMap<Player, PlayerDto>().ReverseMap();

            //    CreateMap<Nation, NationDto>().ReverseMap();

            //    CreateMap<League, LeagueCreateDto>().ReverseMap();
            //    CreateMap<League, LeagueDto>().ReverseMap();

        }
    }
}

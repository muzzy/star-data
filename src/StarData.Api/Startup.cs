﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StarData.Api.MappingProfiles;
using StarData.Application.Extentions;
using StarData.Application.MappingProfiles;
using StarData.Infrastructure.Data;
using StarData.Infrastructure.MappingProfiles;

namespace StarData.Api
{
    public class Startup
    {
        private ILogger<Startup> _logger = LoggerFactory.
            Create(builder => builder.AddConsole())
            .CreateLogger<Startup>();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Config CORS
            services.AddCors(options =>
            {
                var corsConfig = Configuration.GetSection("CorsUrls");
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins(corsConfig?.Get<string[]>());
                    });
            });

            services.AddControllers().AddNewtonsoftJson();

            // Database
            services.AddDbContext<StarDataContext>(c => c.UseSqlServer(Configuration.GetConnectionString("StarDataConnection"),
                x => x.MigrationsAssembly("StarData.Migrations")));
            services.AddStarDataCore();
            services.AddRepositories();
            services.AddServices();
            services.AddAutoMapper(typeof(ApiMappings), typeof(ApplicationMappings), typeof(InfrastructureMappings));
            _logger.LogInformation(Configuration.GetConnectionString("StarDataConnection"));

            // Swagger
            services.AddSwaggerGen();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable file server
            app.UseFileServer();

            app.UseHttpsRedirection();

            // Enable Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Star Data API V1");
            });

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            Console.WriteLine("Startup Configure");
        }
    }
}

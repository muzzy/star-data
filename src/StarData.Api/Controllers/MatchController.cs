﻿//  MatchController.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:40:22 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StarData.Application.Interfaces;

namespace StarData.Api.Controllers
{
    [Route("api/matches")]
    public class MatchController : ControllerBase
    {
        private IMatchService _matchService;
        private ILogger<RatingController> _logger;

        public MatchController(
            IMatchService matchService,
            ILogger<RatingController> logger)
        {
            _matchService = matchService;
            _logger = logger;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetMatchData(int id)
        {
            var match = await _matchService.GetById(id);
            return Ok(match);

        }
    }
}

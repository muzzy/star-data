﻿//  PlayerController.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       20:38:6 21/6/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarData.Api.Dtos;
using StarData.Api.RestfulHelpers;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;
using StarData.Application.Interfaces;

namespace StarData.Api.Controllers
{
    [Route("api/players")]
    public class PlayerController : ControllerBase
    {
        private readonly ILogger<PlayerController> _logger;
        private readonly IPlayerService _playerService;
        private readonly IMapper _mapper;

        public PlayerController(
            IPlayerService playerService,
            ILogger<PlayerController> logger,
            IMapper mapper)
        {
            _playerService = playerService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetRankedPlayers(int pageNumber = 0, int pageSize = 10)
        {
            var players = _playerService.Rank(pageNumber, pageSize);
            return Ok(_mapper.Map<IList<Player>, IList<PlayerDto>>(players.ToList()));
        }

        //[HttpPost]
        //public async Task<IActionResult> CreateAsync([FromBody] PlayerCreateDto player)
        //{

        //    var toAdd = _mapper.Map<Player>(player);
        //    var result = await _repository.CreateAsync(toAdd);

        //    //

        //    if (!result.Succeeded)
        //    {
        //        return Accepted(result.Errors.First());
        //    }
        //    var tt = _mapper.Map<PlayerDto>(result.Data) ;
        //    _logger.LogInformation(tt.ToString());
        //    return Ok(new List<PlayerDto> { _mapper.Map<PlayerDto>(result.Data) });

        //}

        //[HttpGet]
        //public async Task<IActionResult> GetAllAsync([FromQuery] QueryParameters parameters)
        //{
        //    PagedResult<IList<Player>> result = await _repository.GetAllAsync();
        //    // Return the first error if failed.
        //    if (!result.Succeeded)
        //    {
        //        return Accepted(result.Errors.First());
        //    }

        //    return Ok(_mapper.Map<IList<Player>, IList<PlayerDto>>(result.Data));

        //}
    }
}

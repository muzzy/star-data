﻿//  ConfigurationController.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       12:44:56 25/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;

namespace StarData.Api.Controllers
{
    [Route("api/configuration")]
    public class ConfigurationController : ControllerBase
    {
        private IConfiguration _config;

        public ConfigurationController(IConfiguration configuration)
        {
            _config = configuration;
        }

        [HttpGet]
        public IActionResult GetAllAsync()
        {
            var dbConf = _config.GetConnectionString("StarDataConnection").Split(';');
            var corsConf = _config.GetSection("CorsUrls").Get<string[]>();

            return Ok(new
            {
                database = new {
                    server = dbConf[0].Split('=')[1],
                    name = dbConf[3].Split('=')[1],
                    user = dbConf[1].Split('=')[1]
                },
                cors = corsConf
            });
        }
    }
}

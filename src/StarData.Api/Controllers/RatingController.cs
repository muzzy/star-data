﻿//  RatingController.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:7:21 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarData.Application.Interfaces;

namespace StarData.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/rating")]
    public class RatingController : ControllerBase
    {
        private IRatingService _ratingService;
        private IMatchService _matchService;
        private ILogger<RatingController> _logger;

        public RatingController(
            IRatingService ratingService,
            IMatchService matchService,
            ILogger<RatingController> logger)
        {
            _ratingService = ratingService;
            _matchService = matchService;
            _logger = logger;
        }

        [HttpPut]
        [Route("match")]
        public async Task<IActionResult> CreateAsync(int matchId)
        {
            try
            {
                await _ratingService.Rate(matchId);
                return Ok();
            }
            catch(Exception ex)
            {
                return Conflict(ex.Message);
            }
            
            
        }
        [HttpPut]
        [Route("season")]
        public async Task<IActionResult> CreateAsync(int leagueId, int seasonId)
        {
            await _ratingService.Rate(leagueId, seasonId);
            return Ok();
        }
    }
}
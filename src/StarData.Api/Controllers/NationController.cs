﻿//  NationController.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       10:21:34 19/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarData.Api.Dtos;
using StarData.Api.RestfulHelpers;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;

namespace StarData.Api.Controllers
{
    [Route("api/nations")]
    public class NationController : ControllerBase
    {
        private readonly ILogger<NationController> _logger;
        private readonly IRepository<Nation> _repository;
        private readonly IMapper _mapper;

        public NationController(IRepository<Nation> repository,
            ILogger<NationController> logger,
            IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        //[HttpGet]
        //public async Task<IActionResult> GetAllAsync([FromQuery] QueryParameters parameters)
        //{
        //    PagedResult<IList<Nation>> result = await _repository.GetAllAsync();
        //    // Return the first error if failed.
        //    if (!result.Succeeded)
        //    {
        //        return Accepted(result.Errors.First());
        //    }

        //    return Ok(_mapper.Map<IList<Nation>, IList<NationDto>>(result.Data));

        //}
    }
}

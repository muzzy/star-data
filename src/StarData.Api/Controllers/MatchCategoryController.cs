﻿//  MatchCategoryController.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       21:54:10 19/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarData.Api.Dtos;
using StarData.Api.RestfulHelpers;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;

namespace StarData.Api.Controllers
{
    [Route("api/match-categories")]
    public class MatchCategoryController : ControllerBase
    {
        private readonly ILogger<MatchCategoryController> _logger;
        private readonly IRepository<League> _repository;
        private readonly IMapper _mapper;

        public MatchCategoryController(IRepository<League> repository,
            ILogger<MatchCategoryController> logger,
            IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        

        //[HttpPost]
        //public async Task<IActionResult> CreateAsync([FromBody] LeagueCreateDto catgory)
        //{

        //    var toAdd = _mapper.Map<League>(catgory);
        //    var result = await _repository.CreateAsync(toAdd);

        //    //

        //    if (!result.Succeeded)
        //    {
        //        return Accepted(result.Errors.First());
        //    }
        //    var tt = _mapper.Map<LeagueDto>(result.Data);
        //    _logger.LogInformation(tt.ToString());
        //    return Ok(new List<LeagueDto> { _mapper.Map<LeagueDto>(result.Data) });

        //}

        //[HttpGet]
        //public async Task<IActionResult> GetAllAsync([FromQuery] QueryParameters parameters)
        //{
        //    PagedResult<IList<League>> result = await _repository.GetAllAsync();
        //    // Return the first error if failed.
        //    if (!result.Succeeded)
        //    {
        //        return Accepted(result.Errors.First());
        //    }

        //    return Ok(_mapper.Map<IList<League>, IList<LeagueDto>>(result.Data));

        //}
    }
}

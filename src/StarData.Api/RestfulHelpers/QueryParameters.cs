﻿//  QueryParameters.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       21:59:53 17/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
namespace StarData.Api.RestfulHelpers
{
    public class QueryParameters
    {
        private const int MAX_PATE_SIZE = 50;
        private int _pageSize = MAX_PATE_SIZE;

        public int Page { get; set; } = 1;
        public int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = (value > MAX_PATE_SIZE) ? MAX_PATE_SIZE : value; }
        }

        public string Query { get; set; }

        public string OrderBy { get; set; } = "Name";
    }
}

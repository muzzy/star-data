﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StarData.Infrastructure.Interfaces;
using StarData.Infrastructure.Data;
using StarData.Core.Services;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            //using (var serviceScope = host.Services.CreateScope())
            //{
            //    var provider = serviceScope.ServiceProvider;
            //    using (var context = provider.GetService<StarDataContext>())
            //    {
            //        try
            //        {
            //            context.SeedStarData();
            //        }
            //        catch(Exception ex)
            //        {
            //            ILogger<StarDataContext> logger = provider.GetService<ILogger<StarDataContext>>();
            //            logger.LogError(ex, "An error occurred creating the DB.");
            //        }
                    
            //    }
            //}

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

﻿//  MatchCategoryDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       22:2:31 19/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
namespace StarData.Api.Dtos
{
    public class LeagueDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿//  MatchCategoryCreateDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       21:59:40 19/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.ComponentModel.DataAnnotations;

namespace StarData.Api.Dtos
{
    public class LeagueCreateDto
    {
        [Required]
        public string Name { get; set; }
    }
}

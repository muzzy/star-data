﻿//  NationDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       10:26:7 19/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
namespace StarData.Api.Dtos
{
    public class NationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

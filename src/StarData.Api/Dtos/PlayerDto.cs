﻿//  PlayerDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       11:33:58 14/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
namespace StarData.Api.Dtos
{
    public class PlayerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Point { get; set; }
    }
}

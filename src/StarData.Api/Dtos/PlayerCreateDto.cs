﻿//  PlayerCreateDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       11:43:23 14/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.ComponentModel.DataAnnotations;

namespace StarData.Api.Dtos
{
    public class PlayerCreateDto
    {
        [Required]
        public string Name { get; set; }
        public int? Height { get; set; } = 0;
        public int? Weight { get; set; } = 0;
        public string DateOfBirth { get; set; }
        public int? NationId { get; set; }

    }
}

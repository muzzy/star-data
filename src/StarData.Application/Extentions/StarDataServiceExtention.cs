﻿//  StarDataServiceExtention.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       1:1:43 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using StarData.Application.Helpers;
using StarData.Application.Interfaces;
using StarData.Application.Services;
using StarData.RatingEngine.Core;
using StarData.RatingEngine.Interface;

namespace StarData.Application.Extentions
{
    public static class StarDataServiceExtention
    {

        public static void AddServices(this IServiceCollection services)
        {
            services.TryAddScoped<IMatchService, MatchService>();
            services.TryAddScoped<IRatingService, RatingService>();
            services.TryAddScoped<IPlayerService, PlayerService>();

            #region Rating engine
            services.TryAddScoped<IRatingHelper, RatingHelper>();
            services.TryAddScoped<IFactorDecider, FactorDecider>();
            services.TryAddScoped<EloEvaluator>();
            #endregion
        }
    }
}

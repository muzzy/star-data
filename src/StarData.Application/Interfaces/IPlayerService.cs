﻿//  IPlayerService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       13:35:35 11/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Application.Interfaces
{
    public interface IPlayerService
    {
        IEnumerable<Player> Rank(int pageNumber, int pageSize);
    }
}

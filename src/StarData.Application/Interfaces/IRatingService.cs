﻿//  IRatingService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:4:1 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StarData.Application.Interfaces
{
    public interface IRatingService
    {
        Task Rate(int matchId);
        Task Rate(int leagueId, int seasonid);
    }
}

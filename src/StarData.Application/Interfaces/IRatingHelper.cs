﻿//  IRatingHelper.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       17:25:56 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using StarData.Core.Model.Entities;

namespace StarData.Application.Interfaces
{
    public interface IRatingHelper
    {
        public IList<(int Start, int End, int Time)> ToGoalPrinciples(
            IEnumerable<Goal> goals, Func<int, bool> goalFlag);

        public int ToPlayerD(
            IList<(int Start, int End, int Difference)> goals, int On, int Off, int initValue);
    }
}

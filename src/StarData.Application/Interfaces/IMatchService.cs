﻿//  IMatchService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       20:4:6 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Threading.Tasks;
using StarData.Application.Dtos;

namespace StarData.Application.Interfaces
{
    public interface IMatchService
    {
        bool IsRated(int id);

        Task<MatchUIDto> GetById(int id);
    }
}

﻿//  RatingHelper.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:55:26 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using StarData.Application.Interfaces;
using StarData.Core.Model.Entities;

namespace StarData.Application.Helpers
{
    public class RatingHelper : IRatingHelper
    {

        public IList<(int Start, int End, int Time)> ToGoalPrinciples(
            IEnumerable<Goal> goals, Func<int, bool> goalFlag)
        {
            int start = 0;
            int difference = 0;
            List<(int Start, int End, int Difference)> goalList = new();
            goals.OrderBy(g => g.Time).ToList().ForEach(g =>
            {
                goalList.Add((Start: start, End: g.Time, Difference: difference));
                difference += goalFlag(g.ClubId) ? 1 : -1;
                start = g.Time;
            });
            if (start < 90)
            {
                goalList.Add((Start: start, End: 90, Difference: difference));
            }
            return goalList;
        }

        public int ToPlayerD(IList<(int Start, int End, int Difference)> goals, int On, int Off, int initValue)
        {
            int D = initValue;
            if ((On > 0 && On <= 90 )|| Off != 90)
            {
                var diffs = goals.Where(
                    g => On <= g.End && On >= g.Start
                    || (Off <= g.End && Off >= g.Start));
                var first = diffs.FirstOrDefault();
                var last = diffs.LastOrDefault();
                D = last.Difference - first.Difference;
            }
            return D;
        }
    }
}

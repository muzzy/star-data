﻿//  MatchDto.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       20:22:35 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using StarData.Core.Model.VOs;

namespace StarData.Application.Dtos
{
    public class MatchUIDto
    {
        public int Id { get; set; }
        public IEnumerable<PlayerVO> HomePlayers;
        public IEnumerable<PlayerVO> AwayPlayers;
    }
}

﻿//  StarDataMappings.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:30:9 9/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using AutoMapper;
using StarData.Application.Dtos;
using StarData.Core.Model.AggregateRoots;

namespace StarData.Application.MappingProfiles
{
    public class ApplicationMappings : Profile
    {
        public ApplicationMappings()
        {
            CreateMap<Match, MatchUIDto>().ReverseMap();
        }
    }
}

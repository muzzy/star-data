﻿//  PlayerService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       13:45:2 11/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using StarData.Application.Interfaces;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;

namespace StarData.Application.Services
{
    public class PlayerService : IPlayerService
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly ILogger<RatingService> _logger;

        public PlayerService(
            IPlayerRepository playerRepository,
            IRepository<Nation> nationRepository,
            IRepository<League> leagueRepository,
            ILogger<RatingService> logger)
        {
            _playerRepository = playerRepository;
            _logger = logger;
        }

        public IEnumerable<Player> Rank(int pageNumber = 0, int pageSize = 10)
        {
            return _playerRepository.GetAllAsync(
                p => p.Point > 0, p => p.Point, SortOrder.Descending, pageNumber, pageSize);
        }
    }
}

﻿//  RatingService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:6:30 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using StarData.Application.Interfaces;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Model.VOs;
using StarData.Core.Repositories;
using StarData.Core.Services;
using StarData.Core.Specifications;
using StarData.Infrastructure.Interfaces;
using StarData.RatingEngine.Core;

namespace StarData.Application.Services
{
    public class RatingService : IRatingService
    {
        #region Private fields
        private readonly IRatingRepository _ratingRepository;
        private readonly IMatchRepository _matchRepository;
        private readonly IClubRepository _clubRepository;
        private readonly IPlayerRepository _playerRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRatingHelper _ratingHelper;
        private readonly EloEvaluator _evaluator;
        private readonly ILogger<RatingService> _logger;
        private readonly ISet<int> _addedMatch = new HashSet<int>();
        #endregion

        public RatingService(
            IRatingRepository ratingRepository,
            IMatchRepository matchRepository,
            IClubRepository clubRepository,
            IPlayerRepository playerRepository,
            IUnitOfWork unitOfWork,
            IRatingHelper ratingHelper,
            EloEvaluator evaluator,
            ILogger<RatingService> logger)
        {
            _ratingRepository = ratingRepository;
            _matchRepository = matchRepository;
            _clubRepository = clubRepository;
            _playerRepository = playerRepository;
            _unitOfWork = unitOfWork;
            _ratingHelper = ratingHelper;
            _evaluator = evaluator;
            _logger = logger;
        }


        public async Task Rate(int leagueId, int seasonId)
        {
            _addedMatch.Clear();
            var matches = await _matchRepository.GetAllAsync(
                ExpressionSpecification<Match>.Eval(m => m.LeagueId == leagueId && m.SeasonId == seasonId),
                m => m.Time);
            var shouldSpec = new ShouldRatedMatchesSpecification();
            foreach (var match in matches)
            {
                _logger.LogInformation($"Match(#{match.Id}) is being rated.");
                if (_ratingRepository.Exists(r => r.MatchId == match.Id))
                {
                    continue;
                }
                try
                {
                    shouldSpec.TargetMatch = match;
                    var shouldRatedMatchId = await UnRatedMatchId(shouldSpec);
                    if (shouldRatedMatchId != 0 && !_addedMatch.Contains(shouldRatedMatchId))
                    {
                        throw new InvalidOperationException(
                                    $"Match(#{shouldRatedMatchId}) should be rated before {match.Id}");
                    }
                    _matchRepository.LoadById(match);
                    await InnerRatingMatch(match);
                    _addedMatch.Add(match.Id);
                }
                catch
                {
                    throw;
                }
                
            }
            _addedMatch.Clear();
            await _unitOfWork.Commit();
        }

        public async Task Rate(int matchId)
        {
            try
            {
                if (_ratingRepository.Exists(
                ExpressionSpecification<Rating>.Eval(r => r.MatchId == matchId)))
                {
                    throw new InvalidOperationException($"Match(#{matchId}) has been rated");
                }
                var match = await _matchRepository.GetByIdAsync(matchId);

                var shouldSpec = new ShouldRatedMatchesSpecification();
                shouldSpec.TargetMatch = match;
                var shouldRatedMatchId = await UnRatedMatchId(shouldSpec);
                if (shouldRatedMatchId != 0)
                {
                    throw new InvalidOperationException(
                                $"Match(#{shouldRatedMatchId}) should be rated before {match.Id}");
                }

                await InnerRatingMatch(match);
                await _unitOfWork.Commit();
            }
            catch
            {
                throw;
            }
            

        }

        private async Task InnerRatingMatch(Match match)
        {
            #region Match data
            var homePlayerIds = match.HomePlayers.Select(p => p.Id);
            var awayPlayerIds = match.AwayPlayers.Select(p => p.Id);
            var homeClub = await _clubRepository.GetByIdAsync(match.HomeTeamId);
            var awayClub = await _clubRepository.GetByIdAsync(match.AwayTeamId);
            var homePlayers = (_playerRepository.GetAllAsync(
                Specification<Player>.Eval(p => homePlayerIds.Contains(p.Id))))
                .OrderBy(p => p.Id);
            var awayPlayers = (_playerRepository.GetAllAsync(
                Specification<Player>.Eval(p => awayPlayerIds.Contains(p.Id))))
                .OrderBy(p => p.Id);
            #endregion

            #region Rating
            IList<(int Start, int End, int Difference)> goalPrinciples =
                _ratingHelper.ToGoalPrinciples(match.Goals, f => f == match.HomeTeamId);
            int difference = match.HomeGoal - match.AwayGoal;
            var homeE = _evaluator.CalculateEA(homeClub.Point, awayClub.Point);
            var homeC = _evaluator.CalculateCA(1, difference, 90, 90, homeE);
            var awayE = _evaluator.CalculateEA(awayClub.Point, homeClub.Point);
            var awayC = _evaluator.CalculateCA(1, -difference, 90, 90, awayE);

            var homePlayerVOs = match.HomePlayers.OrderBy(p => p.Id);
            var homeRatings = DoPlayersRating(
                goalPrinciples, homePlayerVOs, homePlayers, difference,
                awayClub.Point, homeC, true, match.Time.Year);

            var awayPlayerVOs = match.AwayPlayers.OrderBy(p => p.Id);
            var awayRatings = DoPlayersRating(
                goalPrinciples, awayPlayerVOs, awayPlayers, -difference,
                homeClub.Point, awayC, false, match.Time.Year);

            homeClub.Point = _evaluator.CalculateTeamRA(
               homeRatings.Select(a => (a.Point, a.durationOfPlay)).ToList().AsReadOnly());
            awayClub.Point = _evaluator.CalculateTeamRA(
                awayRatings.Select(a => (a.Point, a.durationOfPlay)).ToList().AsReadOnly());
            #endregion

            #region Setup for DB
            for (int i = 0; i < homeRatings.Count; i++)
            {
                var player = homePlayers.ElementAt(i);
                player.Point = homeRatings[i].Point;
                player.NumberOfMatches++;
                _playerRepository.UpdateAsync(player);
            }

            for (int i = 0; i < awayRatings.Count; i++)
            {
                var player = awayPlayers.ElementAt(i);
                player.Point = awayRatings[i].Point;
                player.NumberOfMatches++;
                _playerRepository.UpdateAsync(player);
            }

            _clubRepository.UpdateAsync(homeClub);
            _clubRepository.UpdateAsync(awayClub);
            _ratingRepository.Create(new Rating { MatchId = match.Id, Time = DateTime.Now });
            _logger.LogInformation($"Match(#{match.Id}) has been rated");
            #endregion
        }

        private List<(int Id, int Point, int durationOfPlay)> DoPlayersRating(
            IList<(int Start, int End, int Difference)> goals,
            IEnumerable<PlayerVO> playerVOs,
            IEnumerable<Player> players,
            int initD,
            int opponentPoint,
            decimal teamC,
            bool isHome,
            int matchYear)
        {

            return playerVOs.Select((p, i) =>
            {
                var player = players.ElementAt(i);
                var age = matchYear - player.DateOfBirth.Year;
                
                int D = _ratingHelper.ToPlayerD(
                    goals, p.On, p.Off, initD);
                player.KFactor = _evaluator.ReduceK(player.KFactor, p.On < 90);
                player.QFactor = _evaluator.ReduceQ(player.QFactor, p.On < 90);
                var E = _evaluator.CalculateEA(player.Point, opponentPoint);
                var C = _evaluator.CalculateCA(1, D, Math.Max(0, p.Off - p.On), 90, E);
                player.HFactor = isHome ? player.HFactor + C : player.HFactor;
                var Point = _evaluator.ReducePlayerRA(
                    isHome ? player.Point + player.HFactor : player.Point,
                    player.KFactor, player.QFactor,
                    C, teamC, Math.Max(0, p.Off - p.On), 90);

                return (player.Id, Point, p.Off - p.On);
            }).ToList();
        }

        private async Task<int> UnRatedMatchId(ShouldRatedMatchesSpecification spec)
        {
            var shouldMatches = await _matchRepository.GetAllAsync(spec, m => m.Time);
            if (shouldMatches.Count() > 0)
            {
                int shouldRatedMatchId = shouldMatches.Last().Id;
                var mm = _ratingRepository.GetAllAsync();
                if (!_ratingRepository.Exists(r => r.MatchId == shouldRatedMatchId))
                {
                    return shouldRatedMatchId;
                }
            }
            return 0;
        }
       
    }
}

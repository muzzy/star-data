﻿//  MatchService.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       20:4:28 8/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Threading.Tasks;
using AutoMapper;
using StarData.Application.Dtos;
using StarData.Application.Interfaces;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;
using StarData.Core.Services;

namespace StarData.Application.Services
{
    public class MatchService : IMatchService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IDomainService _domainService;
        private readonly IMapper _mapper;

        public MatchService(
            IMatchRepository matchRepository,
            IDomainService domainService,
            IMapper mapper)
        {
            _matchRepository = matchRepository;
            _domainService = domainService;
            _mapper = mapper;
        }

        public bool IsRated(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<MatchUIDto> GetById(int id)
        {
            var match = await _matchRepository.GetByIdAsync(id);
            var matchUIDto = _mapper.Map<Match, MatchUIDto>(match);
            return matchUIDto;
        }
    }
}

﻿//  FSSubstitutionConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       23:25:46 2/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using AutoFixture;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Tests.Base;
using StarData.Utilities.Utils;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSSubstitutionConverterTests
    {
        [Fact]
        public void Convert_InputMatchData_ReturnInsertSubstitionSql()
        {
            // Arrange
            var fixture = new Fixture();
            var players = new List<string>
            {
                fixture.Create<string>(),
                fixture.Create<string>(),
                fixture.Create<string>(),
                fixture.Create<string>()
            };
            var time = fixture.Create<string>();
            var match = new Mock<IMatchDto>();
            match.SetupGet(m => m.SubsHome).Returns(new List<(string On, string Off, string Time)>
            {
                (players[0], players[1], time)
            });
            match.SetupGet(m => m.SubsAway).Returns(new List<(string On, string Off, string Time)>
            {
                (fixture.Create<string>(), fixture.Create<string>(),fixture.Create<string>())
            });
            match.SetupGet(m => m.BenchHomePlayers).Returns(new List<string>
            {
                players[0],
                players[1]
            });
            match.SetupGet(m => m.BenchAwayPlayers).Returns(new List<string>
            {
                players[2],
                players[3]
            });
            match.Setup(m => m.MD5()).Returns(fixture.Create<string>());
            var matchDto = match.Object;
            var convertCache = new Mock<FSCache>();
            var producer = new Mock<ISQLProducer>();
            var args = new (string Name, string Value, string FkName, string FkTable)[]
            {
                ("Match", matchDto.MD5(), "MD5Code", "Match"),
                ("OnPlayer", ConvertUtil.SafeName(players[0]), "Name", "Player"),
                ("OffPlayer", ConvertUtil.SafeName(players[1]), "Name", "Player"),
                ("Time", time, string.Empty, string.Empty),
            };
            const string OUTPUT_SUB = "Substitution SQL;";
            producer.Setup(p => p.CreateInsertQueryFK("Substitution", args)).Returns(OUTPUT_SUB);
            var converter = new FSSubstitutionConverter(convertCache.Object, producer.Object);
            

            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(OUTPUT_SUB, actual);
        }

    }
}

﻿//  FSPlayerMatchConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:59:48 7/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using Moq;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Tests.Base;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSPlayerMatchConverterTests : FixtureBaseTests
    {
        [Fact]
        public void Convert_InputMatchData_ReturnInsertSubstitionSql()
        {
            // Arrange
            var players = new List<string>{S(),S(),S(),S()};
            var home = S();
            var away = S();
            var match = new Mock<IMatchDto>();
            match.SetupGet(m => m.HomeTeam).Returns(home);
            match.SetupGet(m => m.AwayTeam).Returns(away);
            match.SetupGet(m => m.TeamHomePlayers).Returns(new List<string>{players[0]});
            match.SetupGet(m => m.BenchHomePlayers).Returns(new List<string> { players[1] });
            match.SetupGet(m => m.TeamAwayPlayers).Returns(new List<string>{players[2]});
            match.SetupGet(m => m.BenchAwayPlayers).Returns(new List<string>{players[3]});
            match.Setup(m => m.MD5()).Returns(S());
            var matchDto = match.Object;
            var convertCache = new Mock<FSCache>();
            var producer = new Mock<ISQLProducer>();
            
            const string OUTPUT_SUB = "PlayerMatch SQL;";
            
            var args = new List<(string, string, int)>{
                ("@HomeId", players[0], 0),
                ("@HomeId", players[1], 1),
                ("@AwayId", players[2], 0),
                ("@AwayId", players[3], 1),
            };
            producer.Setup(p =>
                p.CreateInsertRelationsForPlayerMatch("PlayerMatch", matchDto.MD5(), args))
                .Returns(OUTPUT_SUB);
            var converter = new FSPlayerMatchConverter(convertCache.Object, producer.Object);


            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(OUTPUT_SUB, actual);
        }
    }
}

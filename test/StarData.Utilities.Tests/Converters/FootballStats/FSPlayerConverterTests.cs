﻿//  FSPlayerConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       22:47:2 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using Moq;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Tests.Base;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSPlayerConverterTests : FixtureBaseTests
    {
        [Fact]
        public void Convert_PlayersData_ReturnSql()
        {
            // Arrange
            var fixture = new Fixture();
            var match = new Mock<IMatchDto>();
            var players = new string[]
            {
                S(),
                S(),
                S(),
                S()
            };
            match.SetupGet(m => m.TeamHomePlayers).Returns(new List<string>(){players[0]});
            match.SetupGet(m => m.BenchHomePlayers).Returns(new List<string>() { players[1] });
            match.SetupGet(m => m.TeamAwayPlayers).Returns(new List<string>(){players[2]});
            match.SetupGet(m => m.BenchAwayPlayers).Returns(new List<string>(){players[3]});
            match.SetupGet(m => m.HomeTeam).Returns(fixture.Create<string>());
            match.SetupGet(m => m.AwayTeam).Returns(fixture.Create<string>());
            var matchDto = match.Object;

            var convertCache = new Mock<IConverterCache>();
            var producer = new Mock<ISQLProducer>();

            var outArray = players.Select((p, i) =>
            {
                var club = i <= 1 ? matchDto.HomeTeam : matchDto.AwayTeam;
                var playerArgs = new (string, string)[]
                {
                    ("Name", p),
                };
                var resultP = $"Player {p} SQL;";
                producer.Setup(l => l.CreateInsertIfNotExist("Player", playerArgs)).Returns(resultP);

                var relationArgs = new (string, string, string, string)[]
                {
                    ("Player", p, "Name", "Player"),
                    ("Club", club, "Name", "Club")
                };
                var resultR = $"Relation {p} and {club} SQL;";
                producer.Setup(l => l.CreateInsertRelationIfNotExist("Transfer", relationArgs))
                .Returns(resultR);
                return resultP + resultR.TrimEnd(';');
            });

            var output = string.Join(';', outArray) + ";";
            var converter = new FSPlayerConverter(convertCache.Object, producer.Object);
            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(output, actual);
        }

        [Fact]
        public void Convert_DuplicatedPlayersData_ReturnUnduplicatedSql()
        {
            // Arrange
            var fixture = new Fixture();
            var match = new Mock<IMatchDto>();
            var player = fixture.Create<string>();
            match.SetupGet(m => m.TeamHomePlayers).Returns(new List<string>()
            {
                player
            });
            match.SetupGet(m => m.HomeTeam).Returns(fixture.Create<string>());
            var matchDto = match.Object;

            var convertCache = new Mock<IConverterCache>();
            var producer = new Mock<ISQLProducer>();
            var args = new (string Name, string Value, string FkName, string FkTable)[]
              {
                    (Name:"Name", Value:player, FkName: string.Empty, FkTable: string.Empty),
                    (Name:"Club", Value:matchDto.HomeTeam, FkName:"Name", FkTable: "Club")
              };
            var result = $"Player {player} SQL;";
            producer.Setup(l => l.CreateInsertQueryFKIfNotExist("Player", args)).Returns(result);
            var converter = new FSPlayerConverter(convertCache.Object, producer.Object);
            converter.Convert(matchDto);

            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Empty(actual);
        }
    }
}

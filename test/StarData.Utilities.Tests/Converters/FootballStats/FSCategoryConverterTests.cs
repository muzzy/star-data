﻿//  FSCategoryConverter.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       16:13:28 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using AutoFixture;
using Moq;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSCategoryConverterTests
    {
        [Fact]
        public void Convert_InputCategoryData_ReturnInsertCategorySql()
        {
            // Arrange
            var fixture = new Fixture();
            string nation = fixture.Create<string>();
            string league = fixture.Create<string>();
            string season = fixture.Create<string>();
            var producer = new Mock<ISQLProducer>();
            const string OUTPUT_CATEGORY = "League SQL;";
            const string OUTPUT_SEASON = "Season SQL";

            string leagueData = $"{nation} {league} {season}";
            string category = $"{nation} {league}";

            var categoryArgs = new (string Name, string Value)[]
            {
                (Name:"Name", Value:category)
            };
            producer.Setup(p => p.CreateInsertIfNotExist("League", categoryArgs))
                .Returns(OUTPUT_CATEGORY);

            var seasonArgs = new (string Name, string Value)[]
            {
                (Name:"Name", Value:season)
            };
            producer.Setup(p => p.CreateInsert("Season", seasonArgs))
                .Returns(OUTPUT_SEASON);

            var fSCache = new Mock<FSCache>();
            var converter = new FSLeagueConverter(fSCache.Object, producer.Object);

            // Action
            string actual = converter.Convert(leagueData);

            // Assert
            Assert.Equal(OUTPUT_CATEGORY + OUTPUT_SEASON, actual);
        }

        [Fact]
        public void Convert_InputNull_ReturnEmpty()
        {
            // Arrange
            string nullData = string.Empty;
            var fSCache = new Mock<FSCache>();
            var producer = new Mock<ISQLProducer>();
            var converter = new FSLeagueConverter(fSCache.Object, producer.Object);
            string OUTPUT = string.Empty;

            // Action
            string actual = converter.Convert(nullData);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }
    }
}

﻿//  FSClubConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:51:50 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using AutoFixture;
using Moq;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Tests.Base;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSClubConverterTests : FixtureBaseTests
    {
        [Fact]
        public void Convert_ClubsDataWithNationCache_ReturnSql()
        {
            // Arrange

            var match = new Mock<IMatchDto>();
            match.SetupGet(m => m.HomeTeam).Returns(S());
            match.SetupGet(m => m.AwayTeam).Returns(S());
            var matchDto = match.Object;

            const string OUTPUT_HOME = "Club h SQL;";
            const string OUTPUT_SEASON_HOME = "SeasonClub h SQL;";
            const string OUTPUT_AWAY = "Club a SQL;";
            const string OUTPUT_SEASON_AWAY = "SeasonClub a SQL;";

            var convertCache = new Mock<IConverterCache>();
            convertCache.SetupGet(c => c.CurrentNation).Returns(S());
            convertCache.SetupGet(c => c.CurrentSeason).Returns(S());
            convertCache.SetupGet(c => c.CurrentLeague).Returns(S());
            var cacheStub = convertCache.Object;

            var producer = new Mock<ISQLProducer>();
            var argsHome = new (string Name, string Value, string FkName, string FkTable)[]
            {
                (Name:"Name", Value:matchDto.HomeTeam, FkName: string.Empty, FkTable: string.Empty),
                (Name:"Nation", Value:cacheStub.CurrentNation, FkName:"Name,Alias", FkTable: "Nation"),
                ("League", cacheStub.CurrentLeague, "Name", "League")
            };
            producer.Setup(c => c.CreateInsertQueryFKIfNotExist("Club", argsHome))
                .Returns(OUTPUT_HOME);
            var argsSeasonHome = new (string Name, string Value, string FkName, string FkTable)[]
                {
                    ("Season", cacheStub.CurrentSeason, "Name", "Season"),
                    ("Club", matchDto.HomeTeam, "Name", "Club")
                };
            producer.Setup(c => c.CreateInsertRelationIfNotExist("SeasonClub", argsSeasonHome))
                .Returns(OUTPUT_SEASON_HOME);

            var argsAway = new (string Name, string Value, string FkName, string FkTable)[]
            {
                (Name:"Name", Value:matchDto.AwayTeam, FkName: string.Empty, FkTable: string.Empty),
                (Name:"Nation", Value:cacheStub.CurrentNation, FkName:"Name,Alias", FkTable: "Nation"),
                ("League", cacheStub.CurrentLeague, "Name", "League")
            };
            producer.Setup(c => c.CreateInsertQueryFKIfNotExist("Club", argsAway))
                .Returns(OUTPUT_AWAY);
            var argsSeasonAway = new (string Name, string Value, string FkName, string FkTable)[]
                {
                    ("Season", cacheStub.CurrentSeason, "Name", "Season"),
                    ("Club", matchDto.AwayTeam, "Name", "Club")
                };
            producer.Setup(c => c.CreateInsertRelationIfNotExist("SeasonClub", argsSeasonAway))
                .Returns(OUTPUT_SEASON_AWAY);



            var converter = new FSClubConverter(cacheStub, producer.Object);

            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(
                OUTPUT_HOME + OUTPUT_SEASON_HOME  + OUTPUT_AWAY + OUTPUT_SEASON_AWAY,
                actual);
        }

        [Fact]
        public void Convert_DuplicatedClubsDataWithNationCache_ReturnUnduplicatedSql()
        {
            // Arrange
            var fixture = new Fixture();

            var match = new Mock<IMatchDto>();
            match.SetupGet(m => m.HomeTeam).Returns(fixture.Create<string>());
            match.SetupGet(m => m.AwayTeam).Returns(fixture.Create<string>());
            var matchDto = match.Object;

            var convertCache = new Mock<IConverterCache>();
            convertCache.SetupGet(c => c.CurrentNation).Returns(fixture.Create<string>());
            var cacheStub = convertCache.Object;

            var producer = new Mock<ISQLProducer>();
            var converter = new FSClubConverter(cacheStub, producer.Object);
            converter.Convert(matchDto);

            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Empty(actual);
        }

        [Fact]
        public void Convert_ClubsDataWithoutNationCache_ThrowsInvalidOperationException()
        {
            // Arrange
            var match = new Mock<IMatchDto>();
            var convertCache = new Mock<IConverterCache>();
            var producer = new Mock<ISQLProducer>();
            var converter = new FSClubConverter(convertCache.Object, producer.Object);
            string OUTPUT = "Nation not found in FSCache, please convert Category first";

            // Action
            var actual = Assert.Throws<InvalidOperationException>(
                () => converter.Convert(match.Object));

            // Assert
            Assert.Equal(OUTPUT, actual.Message);
        }

        
    }
}

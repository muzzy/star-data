﻿//  FSMatchConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       0:35:16 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using AutoFixture;
using Moq;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Tests.Base;
using StarData.Utilities.Utils;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSMatchConverterTests : FixtureBaseTests
    {
        [Fact]
        public void Convert_MatchData_ReturnSql()
        {
            // Arrange
            var season = S();
            var league = S();
            var match = new Mock<IMatchDto>();
            match.SetupGet(m => m.HomeTeam).Returns(S());
            match.SetupGet(m => m.AwayTeam).Returns(S());
            match.SetupGet(m => m.HomeScore).Returns(I());
            match.SetupGet(m => m.AwayScore).Returns(I());
            match.Setup(m => m.MatchTime()).Returns(S());
            match.Setup(m => m.MD5()).Returns(S());
     
            var matchDto = match.Object;
            string OUTPUT = "Match SQL;";
    
            var convertCache = new Mock<IConverterCache>();
            convertCache.SetupGet(c => c.CurrentSeason).Returns(season);
            convertCache.SetupGet(c => c.CurrentLeague).Returns(league);
            var producer = new Mock<ISQLProducer>();
            var args = new(string Name, string Value, string FkName, string FkTable)[]
            {
                ("Season", season, "Name", "Season"),
                ("League", league, "Name", "League"),
                ("Round", "1", string.Empty, string.Empty),
                ("HomeTeam", matchDto.HomeTeam, "Name", "Club"),
                ("AwayTeam", matchDto.AwayTeam, "Name", "Club"),
                ("HomeGoal", matchDto.HomeScore.ToString(), string.Empty, string.Empty),
                ("AwayGoal", matchDto.AwayScore.ToString(), string.Empty, string.Empty),
                ("Time", $"CONVERT(SMALLDATETIME, '{matchDto.MatchTime()}')", string.Empty, string.Empty),
                ("MD5Code", matchDto.MD5(), string.Empty, string.Empty)
            };
            producer.Setup(m => m.CreateInsertQueryFK("Match", args)).Returns(OUTPUT);
            var converter = new FSMatchConverter(convertCache.Object, producer.Object);
           
            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }
    }
}

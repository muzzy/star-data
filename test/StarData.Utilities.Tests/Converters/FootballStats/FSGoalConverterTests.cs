﻿//  FSGoalConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       8:44:20 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using AutoFixture;
using Moq;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Tests.Base;
using StarData.Utilities.Utils;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSGoalConverterTests : FixtureBaseTests
    {
        [Fact]
        public void Convert_GoalData_ReturnSql()
        {
            // Arrange
            var fixute = new Fixture();
            var match = new Mock<IMatchDto>();
            const int GOAL_COUNT_HOME = 1;
            const int GOAL_COUNT_AWAY = 2;
            match.SetupGet(m => m.GoalHome).Returns(new List<(string Name, int time)>()
            {
                (S(), I()),
                (S(), I())
            });
            match.SetupGet(m => m.GoalAway).Returns(new List<(string Name, int time)>()
            {
                (S(), I()),
                (S(), I())
            });
            match.SetupGet(m => m.HomeScore).Returns(GOAL_COUNT_HOME);
            match.SetupGet(m => m.AwayScore).Returns(GOAL_COUNT_AWAY);
            match.SetupGet(m => m.HomeTeam).Returns(S());
            match.Setup(m => m.MD5()).Returns(fixute.Create<string>());
            var matchDto = match.Object;

            string OUTPUT_GOAL = "Goal SQL;";
            var convertCache = new Mock<IConverterCache>();
            var producer = new Mock<ISQLProducer>();
            foreach(var goal in matchDto.GoalHome)
            {
                var args = new (string Name, string Value, string FkName, string FkTalbe)[]
                {
                    ("Match", matchDto.MD5(), "MD5Code", "Match"),
                    ("Player", goal.Name, "Name", "Player"),
                    ("Club", matchDto.HomeTeam, "Name", "Club"),
                    ("Time", goal.Time.ToString(), string.Empty, string.Empty)
                };
                producer.Setup(g => g.CreateInsertQueryFK("Goal", args)).Returns(OUTPUT_GOAL);
            }
            foreach (var goal in matchDto.GoalAway)
            {
                var args = new (string Name, string Value, string FkName, string FkTalbe)[]
                {
                    ("Match", matchDto.MD5(), "MD5Code", "Match"),
                    ("Player", goal.Name, "Name", "Player"),
                    ("Club", matchDto.AwayTeam, "Name", "Club"),
                    ("Time", goal.Time.ToString(), string.Empty, string.Empty)
                };
                producer.Setup(g => g.CreateInsertQueryFK("Goal", args)).Returns(OUTPUT_GOAL);
            }

            var converter = new FSGoalConverter(convertCache.Object, producer.Object);
            

            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(OUTPUT_GOAL + OUTPUT_GOAL + OUTPUT_GOAL, actual);
        }

        [Fact]
        public void Convert_NoGoalData_ReturnEmpty()
        {
            // Arrange
            var match = new Mock<IMatchDto>();
            var convertCache = new Mock<IConverterCache>();
            var producer = new Mock<ISQLProducer>();
            var converter = new FSGoalConverter(convertCache.Object, producer.Object);
            string OUTPUT = string.Empty;

            // Action
            string actual = converter.Convert(match.Object);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

    }
}

﻿//  FSFoulConverterTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:3:48 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using AutoFixture;
using Moq;
using Newtonsoft.Json.Linq;
using StarData.Utilities.Interfaces;
using StarData.Utilities.Converters.FootballStats;
using StarData.Utilities.Dtos;
using StarData.Utilities.Tests.Base;
using StarData.Utilities.Utils;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters.FootballStats
{
    public class FSFoulConverterTests
    {
        [Fact]
        public void Convert_FoulData_ReturnSql()
        {
            // Arrange
            var fixture = new Fixture();
            const string OUTPUT_FOUL = "Foul SQL;";
            const string CARD_RED = "Red";
            var match = new Mock<IMatchDto>();
            var players = new string[]
            {
                fixture.Create<string>(),
                fixture.Create<string>(),
                fixture.Create<string>(),
                fixture.Create<string>()
            };
            match.SetupGet(m => m.TeamHomePlayers).Returns(new List<string>()
            {
                players[0], players[1]
            });
            match.SetupGet(m => m.TeamAwayPlayers).Returns(new List<string>()
            {
                players[2], players[3]
            });
            match.SetupGet(m => m.FoulHome).Returns(
                new List<(string Name, string Card, string Type, int Time)>()
            {
                (players[1], fixture.Create<string>(), fixture.Create<string>(),fixture.Create<int>())
            });
            match.SetupGet(m => m.FoulAway).Returns(
                new List<(string Name, string Card, string Type, int Time)>()
            {
                (players[2], CARD_RED, fixture.Create<string>(),fixture.Create<int>())
            });
            match.Setup(m => m.MD5()).Returns(fixture.Create<string>());
            var matchDto = match.Object;
            var convertCache = new Mock<IConverterCache>();
            var producer = new Mock<ISQLProducer>();
            var fouls = ConvertUtil.MergeList(matchDto.FoulHome, matchDto.FoulAway);
            foreach(var foul in fouls)
            {
                var args = new (string Name, string Value, string FkName, string FkTable)[]
                {
                    ("Match", matchDto.MD5(), "MD5Code", "Match"),
                    ("Player", foul.Name, "Name", "Player"),
                    ("Time", foul.Time.ToString(), string.Empty, string.Empty)
                };
                producer.Setup(f => f.CreateInsertQueryFK("Foul", args)).Returns(OUTPUT_FOUL);
            }
            var converter = new FSFoulConverter(convertCache.Object, producer.Object);
            

            // Action
            string actual = converter.Convert(matchDto);

            // Assert
            Assert.Equal(OUTPUT_FOUL, actual);
        }
    }
}

﻿//  FSSQLProducerTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:43:4 4/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections.Generic;
using AutoFixture;
using StarData.Utilities.Converters;
using StarData.Utilities.Tests.Base;
using Xunit;

namespace StarData.Infrastructure.Tests.Converters
{
    public class FSSQLProducerTests : FixtureBaseTests
    {
        [Fact]
        public void CreateInsert_Input_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "Nation";
            var ARGS = new (string Name, string Value)[]
            {
                (Name:"Name", Value:"China"),
                (Name:"Code", Value:"CHN")
            };
            const string OUTPUT = "INSERT INTO Nation(Name,Code) VALUES('China','CHN');";

            // Action
            var actual = producer.CreateInsert(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsert_InputEmptyTable_ReturnsEmpty()
        {
            // Arrange
            var fixture = new Fixture();
            var producer = new FSSQLProducer();
            var TABLE_NAME_EMPTY = string.Empty;
            var args = fixture.Create<(string, string)>();
            var OUTPUT_EMPTY = string.Empty;

            // Action
            var actual = producer.CreateInsert(TABLE_NAME_EMPTY, args);

            // Assert
            Assert.Equal(OUTPUT_EMPTY, actual);
        }

        [Fact]
        public void CreateInsert_InputEmptyArgs_ReturnsEmpty()
        {
            // Arrange
            var fixture = new Fixture();
            var producer = new FSSQLProducer();
            var TABLE_NAME_EMPTY = fixture.Create<string>();
            var OUTPUT_EMPTY = string.Empty;

            // Action
            var actual = producer.CreateInsert(TABLE_NAME_EMPTY);

            // Assert
            Assert.Equal(OUTPUT_EMPTY, actual);
        }

        [Fact]
        public void CreateInsertIfNotExist_InputSingleParam_ReturnsInserSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "Nation";
            var ARGS = new (string Name, string Value)[]
            {
                (Name:"Name", Value:"China")
            };
            const string OUTPUT =
                "INSERT INTO Nation(Name) " +
                "SELECT 'China' " +
                "WHERE NOT EXISTS " +
                "(SELECT 1 FROM Nation WHERE Name='China');";

            // Action
            var actual = producer.CreateInsertIfNotExist(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertIfNotExist_InputMultipleParams_ReturnsInserSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "Nation";
            var ARGS = new (string Name, string Value)[]
            {
                (Name:"Name", Value:"China"),
                (Name:"Code", Value:"CHN")
            };
            const string OUTPUT =
                "INSERT INTO Nation(Name,Code) " +
                "SELECT 'China','CHN' " +
                "WHERE NOT EXISTS " +
                "(SELECT 1 FROM Nation WHERE Name='China' AND Code='CHN');";

            // Action
            var actual = producer.CreateInsertIfNotExist(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFK_InputSingleFK_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string fkName, string fkTable)[]
            {
                (Name:"a1", Value:"b1", fkName:"c1", fkTable:"t1")
            };
            const string OUTPUT =
                "INSERT INTO t(a1) VALUES(" +
                "(SELECT Id FROM t1 WHERE c1='b1'));";

            // Action
            var actual = producer.CreateInsertQueryFK(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFK_InputSpecialValue_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string fkName, string fkTable)[]
            {
                (Name:"a1", Value: "CONVERT()", string.Empty, string.Empty)
            };
            const string OUTPUT =
                "INSERT INTO t(a1) VALUES(CONVERT());";

            // Action
            var actual = producer.CreateInsertQueryFK(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFK_InputMultipleFK_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string fkName, string fkTable)[]
            {
                (Name:"a1", Value:"b1", fkName:"c1", fkTable:"t1"),
                (Name:"a2", Value:"b2", fkName:"c2", fkTable:"t2"),
                (Name:"a3", Value:"b3", fkName:"c3", fkTable:"t3")
            };
            const string OUTPUT =
                "INSERT INTO t(a1, a2, a3) VALUES(" +
                "(SELECT Id FROM t1 WHERE c1='b1'), " +
                "(SELECT Id FROM t2 WHERE c2='b2'), " +
                "(SELECT Id FROM t3 WHERE c3='b3')" +
                ");";

            // Action
            var actual = producer.CreateInsertQueryFK(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFK_InputMixFKAndValue_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string fkName, string fkTable)[]
            {
                (Name:"a1", Value:"b1", fkName:"c1", fkTable:"t1"),
                (Name:"a2", Value:"b2", fkName: string.Empty, fkTable:string.Empty)
            };
            const string OUTPUT =
                "INSERT INTO t(a1, a2) VALUES(" +
                "(SELECT Id FROM t1 WHERE c1='b1'), " +
                "'b2'" +
                ");";

            // Action
            var actual = producer.CreateInsertQueryFK(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFK_InputMixValueAndFKAndValue_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string fkName, string fkTable)[]
            {
                (Name:"a1", Value:"b1", fkName: string.Empty, fkTable:string.Empty),
                (Name:"a2", Value:"b2", fkName:"c2", fkTable:"t2"),
                (Name:"a3", Value:"b3", fkName: string.Empty, fkTable:string.Empty)
            };
            const string OUTPUT =
                "INSERT INTO t(a1, a2, a3) VALUES(" +
                "'b1', " +
                "(SELECT Id FROM t2 WHERE c2='b2'), " +
                "'b3');";

            // Action
            var actual = producer.CreateInsertQueryFK(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFKIfNotExist_InputOneFk_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string FkName, string FkTable)[]
            {
                (Name:"a1", Value:"b1", FkName: string.Empty, FkTable: string.Empty),
                (Name:"a2", Value:"b2", FkName: "c2", FkTable: "t2")
            };
            const string OUTPUT =
                "SELECT @a2Id=Id FROM t2 WHERE c2='b2';" +
                "INSERT INTO t(a1,a2) " +
                "SELECT 'b1',@a2Id " +
                "WHERE NOT EXISTS (SELECT 1 FROM t WHERE a1='b1' AND a2=@a2Id);";

            // Action
            var actual = producer.CreateInsertQueryFKIfNotExist(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFKIfNotExist_InputOneFkWithOrFkName_ReturnsInsertSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            const string TABLE_NAME = "t";
            var ARGS = new (string Name, string Value, string FkName, string FkTable)[]
            {
                (Name:"a1", Value:"b1", FkName: string.Empty, FkTable: string.Empty),
                (Name:"a2", Value:"b2", FkName: "c2,d2", FkTable: "t2")
            };
            const string OUTPUT =
                "SELECT @a2Id=Id FROM t2 WHERE c2='b2' OR d2='b2';" +
                "INSERT INTO t(a1,a2) " +
                "SELECT 'b1',@a2Id " +
                "WHERE NOT EXISTS (SELECT 1 FROM t WHERE a1='b1' AND a2=@a2Id);";

            // Action
            var actual = producer.CreateInsertQueryFKIfNotExist(TABLE_NAME, ARGS);

            // Assert
            Assert.Equal(OUTPUT, actual);
        }

        [Fact]
        public void CreateInsertQueryFKIfNotExist_InputMixFxAndValue_ReturnsSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            string table = "t1";
            var ARGS = new (string Name, string Value, string FkName, string FkTable)[]
            {
                (Name:"a1", Value:"b1", FkName: string.Empty, FkTable: string.Empty),
                (Name:"a2", Value:"b2", FkName: "c2", FkTable: "t2"),
                (Name:"a3", Value:"b3", string.Empty, string.Empty)
            };
            string output = 
                "SELECT @a2Id=Id FROM t2 WHERE c2='b2';" + 
                "INSERT INTO t1(a1,a2,a3) " +
                "SELECT 'b1',@a2Id,'b3' " +
                "WHERE NOT EXISTS (SELECT 1 FROM t1 WHERE a1='b1' AND a2=@a2Id AND a3='b3');";

            // Action
            var actual = producer.CreateInsertQueryFKIfNotExist(table, ARGS);

            // Assert
            Assert.Equal(output, actual);
        }

        [Fact]
        public void CreateInsertRelationIfNotExist_ReturnsSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            var table = S();
            var fkA = (Name: S(), Value: S(), FkName: S(), FkTable: S());
            var fkB = (Name: S(), Value: S(), FkName: S(), FkTable: S());
            var args = new (string Name, string Value, string FkName, string FkTable)[] {fkA, fkB};
            var output =
                $"SELECT @{fkA.Name}Id=Id FROM {fkA.FkTable} WHERE {fkA.FkName}='{fkA.Value}';" +
                $"SELECT @{fkB.Name}Id=Id FROM {fkB.FkTable} WHERE {fkB.FkName}='{fkB.Value}';" +
                $"INSERT INTO {table}({fkA.Name},{fkB.Name}) SELECT @{fkA.Name}Id,@{fkB.Name}Id " +
                $"WHERE NOT EXISTS(SELECT 1 FROM {table} " +
                $"WHERE {fkA.Name}=@{fkA.Name}Id AND {fkB.Name}=@{fkB.Name}Id);";

            // Action
            var actual = producer.CreateInsertRelationIfNotExist(table, args);

            // Assert
            Assert.Equal(output, actual);

        }

        [Fact]
        public void CreateInsertRelationsForPlayerMatch_ReturnsSQL()
        {
            // Arrange
            var producer = new FSSQLProducer();
            var table = S();
            var md5 = S();
            var homeId = S();
            var awayId = S();
            const int BENCH_FLAG = 1;
            const int NOT_BENCH_FLAG = 0;
            var playerA = (Club:homeId, Player:S(), IsBench:NOT_BENCH_FLAG);
            var playerB = (Club: awayId, Player: S(), IsBench: BENCH_FLAG);
            var players = new List<(string Club, string Player, int IsBench)>()
            {
                playerA, playerB
            };
            var output = "INSERT INTO PlayerMatch(Match, Player, Club, IsBench) VALUES(" +
                $"@MatchId," +
                $"(SELECT Id FROM Player WHERE Name='{playerA.Player}')," +
                $"{homeId},{playerA.IsBench})," +
                $"(@MatchId," +
                $"(SELECT Id FROM Player WHERE Name='{playerB.Player}')," +
                $"{awayId},{playerB.IsBench});";

            // Action
            var actual = producer.CreateInsertRelationsForPlayerMatch(table, md5, players);

            // Assert
            Assert.Equal(output, actual);
        }
    }
}

﻿//  SQLExecutorTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:5:26 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using StarData.Utilities.Utils;
using StarData.Utilities.Tests.Base;
using Xunit;
using Microsoft.Data.Sqlite;
using Moq;
using System.Data.Common;
using System.Data;
using System.IO.Abstractions;
using System.IO;
using System.Text;
using System.CommandLine;
using System.CommandLine.IO;

namespace StarData.Utilities.Tests.Utils
{
    public class SQLExecutorTests : FixtureBaseTests
    {
        

        [Fact]
        public void ExecuteFile_CommentLines_ReturnsZero()
        {
            // Arrange
            var builder = new StringBuilder();
            builder.AppendLine("#CommentLine1");
            builder.AppendLine("#CommentLine2");
            const int OUTPUT = 0;
            var file = S();
            var fileSystem = MockFileSystem(file, builder.ToString());
            var conn = MockConnection();

            using (var executor = new SQLExecutor(conn, fileSystem))
            {
                // Action
                var actual = executor.ExecuteFile(file);

                // Assert
                Assert.Equal(OUTPUT, actual);
            }

        }

        [Fact]
        public void ExecuteFile_SQLLines_ReturnsRowsCount()
        {
            // Arrange
            var builder = new StringBuilder();
            builder.AppendLine("SQLLine1");
            builder.AppendLine("SQLLine2");
            const int OUTPUT = 2;
            var file = S();
            var fileSystem = MockFileSystem(file, builder.ToString());
            var conn = MockConnection();

            using (var executor = new SQLExecutor(conn, fileSystem))
            {
                // Action
                var actual = executor.ExecuteFile(file);

                // Assert
                Assert.Equal(OUTPUT, actual);
            }
        }

        [Fact]
        public void ExecuteFile_SQLLinesAndIsLineEqualFalse_ReturnsRowsCount()
        {
            // Arrange
            var builder = new StringBuilder();
            builder.AppendLine("SQLLine1");
            builder.AppendLine("SQLLine2");
            const int OUTPUT = 1;
            var file = S();
            var fileSystem = MockFileSystem(file, builder.ToString());
            var conn = MockConnection();

            using (var executor = new SQLExecutor(conn, fileSystem))
            {
                // Action
                var actual = executor.ExecuteFile(file, false);

                // Assert
                Assert.Equal(OUTPUT, actual);
            }
        }

        [Fact]
        public void ExecuteFile_Extras_ReturnsRowsCount()
        {
            // Arrange
            const int OUTPUT = 2;
            var file = S();
            const string content = "";
            var fileSystem = MockFileSystem(file, content);
            var conn = MockConnection();
            var extras = new string[]
            {
                S(), S()
            };

            using (var executor = new SQLExecutor(conn, fileSystem))
            {
                // Action
                var actual = executor.ExecuteFile(file, T(), extras);

                // Assert
                Assert.Equal(OUTPUT, actual);
            }
        }

        [Fact]
        public void ExecuteFile_NotExistedFile_ReturnsZeroRowsCount()
        {
            // Arrange
            const int OUTPUT = 0;
            var file = S();
            const string content = "SQLLine";
            var fileSystem = MockFileSystem(file, content);
            var conn = MockConnection();
            

            using (var executor = new SQLExecutor(conn, fileSystem))
            {
                // Action
                var actual = executor.ExecuteFile(S());

                // Assert
                Assert.Equal(OUTPUT, actual);
            }
        }

        [Fact]
        public void ExecuteFile_DbError_ThrowsException()
        {
            // Arrange
            var file = S();
            const string content = "SQLLine";
            var fileSystem = MockFileSystem(file, content);
            var conn = MockConnectionWithException();

            using (var executor = new SQLExecutor(conn, fileSystem))
            {
                // Action

                // Assert
                Assert.Throws<Exception>(() => executor.ExecuteFile(file));
            }
        }

        private IDbConnection MockConnection()
        {
            var conn = new Mock<IDbConnection>();
            var transaction = new Mock<IDbTransaction>();
            conn.Setup(c => c.BeginTransaction()).Returns(transaction.Object);

            var command = new Mock<IDbCommand>();
            command.Setup(c => c.ExecuteNonQuery()).Returns(1);
            conn.Setup(c => c.CreateCommand()).Returns(command.Object);
            return conn.Object;
        }

        private IDbConnection MockConnectionWithException()
        {
            var conn = new Mock<IDbConnection>();
            var transaction = new Mock<IDbTransaction>();
            conn.Setup(c => c.BeginTransaction()).Returns(transaction.Object);
            var command = new Mock<IDbCommand>();
            command.Setup(c => c.ExecuteNonQuery()).Throws<Exception>();
            conn.Setup(c => c.CreateCommand()).Returns(command.Object);
            return conn.Object;
        }

        private IFileSystem MockFileSystem(string file, string content)
        {
            var fileSystem = new Mock<IFileSystem>();
            var fileProp = new Mock<IFile>();
            MemoryStream ms = new (Encoding.UTF8.GetBytes(content));
            fileProp.Setup(f => f.OpenText(file)).Returns(new StreamReader(ms));
            fileProp.Setup(f => f.ReadAllText(file)).Returns(content);
            fileProp.Setup(f => f.Exists(file)).Returns(true);
            fileSystem.SetupGet(f => f.File).Returns(fileProp.Object);
            return fileSystem.Object;
        }

        private IConsole MockConsole()
        {         
            var console = new Mock<IConsole>();
            var writer = new Mock<IStandardStreamWriter>();
            console.SetupGet(c => c.Out).Returns(writer.Object);
            return console.Object;
        }
    }
}

﻿//  ReaderParserTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       11:42:35 24/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Moq;
using StarData.Utilities.Tests.Base;
using StarData.Utilities.Utils;
using Xunit;

namespace StarData.Utilities.Tests.Utils
{
    public class ReaderParserTests : FixtureBaseTests
    {
        [Fact]
        public void Parse_ObjectReader_ReturnsObject()
        {
            // Arrange
            var output = new FakeClass();
            output.First = I();
            output.Second = S();
            var reader = ConfigReader();
            reader.Setup(r => r["First"]).Returns(output.First);
            reader.Setup(r => r["Second"]).Returns(output.Second);
            reader.SetupSequence(r => r.Read())
                .Returns(true)
                .Returns(true)
                .Returns(false);

            // Action
            var parser = new ReaderParser<FakeClass>();
            var actual = parser.Parse(reader.Object);

            // Assert
            Assert.Equal(output, actual);
        }

        [Fact]
        public void Parse_EnumerableType_ThrowsException()
        {
            // Arrange
            var reader = ConfigReader();

            // Action
            var parser = new ReaderParser<IEnumerable<FakeClass>>();
            Action actual = () => parser.Parse(reader.Object);

            // Assert
            Assert.Throws<Exception>(actual);
        }

        [Fact]
        public void ParseForCollection_EnumerableReader_ReturnsEnumerable()
        {
            // Arrange
            var item = new FakeClass();
            item.First = I();
            item.Second = S();
            var data = new List<FakeClass>()
            {
                item,
                item
            };
            var reader = ConfigReader();
            reader.Setup(r => r["First"]).Returns(item.First);
            reader.Setup(r => r["Second"]).Returns(item.Second);
            reader.SetupSequence(r => r.Read())
                .Returns(true)
                .Returns(true)
                .Returns(true)
                .Returns(true)
                .Returns(false);

            // Action
            var parser = new ReaderParser<IEnumerable<FakeClass>>();
            var actual = parser.ParseForCollection(reader.Object);

            // Assert
            foreach(var a in actual)
            {
                Assert.Equal(item, a);
            }
        }

        [Fact]
        public void ParseForCollection_ObjectType_ThrowsException()
        {
            // Arrange
            var reader = ConfigReader();

            // Action
            var parser = new ReaderParser<FakeClass>();
            Func<IEnumerable> func = () => parser.ParseForCollection(reader.Object);
            Action actual = () =>
            {
                var result = func.Invoke();
                foreach (var item in result);
            };

            // Assert
            
            Assert.Throws<Exception>(actual);
        }

        private Mock<IDataReader> ConfigReader()
        {
            var reader = new Mock<IDataReader>();
            reader.Setup(r => r.FieldCount).Returns(2);
            reader.Setup(r => r.GetName(0)).Returns("First");
            reader.Setup(r => r.GetName(1)).Returns("Second");
            reader.Setup(r => r.GetOrdinal("First")).Returns(0);
            reader.Setup(r => r.GetOrdinal("Second")).Returns(1);
            reader.Setup(r => r.GetFieldType(0)).Returns(typeof(int));
            reader.Setup(r => r.GetFieldType(1)).Returns(typeof(string));

            return reader;
        }
    }

    public class FakeClass : IEquatable<FakeClass>
    {
        public int First { get; set; }
        public string Second { get; set; }

        public bool Equals(FakeClass other)
        {
            if (other != null)
            {
                return First == other.First
                && Second.Equals(other.Second);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(First, Second);
        }
    }
}

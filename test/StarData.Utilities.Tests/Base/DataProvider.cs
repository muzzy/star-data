﻿//  DataProvider.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       14:18:8 3/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
namespace StarData.Utilities.Tests.Base
{
    public static class DataProvider
    {
        public static readonly string JSON_EMPTY = "{}";

        /*---------- Category ----------*/

        public static readonly string INSERT_SQL_CATEGORY =
                "INSERT INTO League(Name)" +
                "SELECT 'English Premier League'" +
                "WHERE NOT EXISTS " +
                "(SELECT 1 FROM League WHERE Name='English Premier League');" +
                "INSERT INTO Season(Name) VALUES(" +
                "'1996-1997');";

        /*---------- Club ----------*/

        public static readonly string INSERT_SQL_CLUB =
                "INSERT INTO Club(Name, Nation) VALUES(" +
                "'Nottingham Forest', " +
                "(SELECT Id FROM Nation WHERE Name='English' OR Alias='English'));" +
                "INSERT INTO Club(Name, Nation) VALUES(" +
                "'Sunderland', " +
                "(SELECT Id FROM Nation WHERE Name='English' OR Alias='English'));";

        /*---------- Player ----------*/

        public static readonly string INSERT_SQL_PLAYER =
            "INSERT INTO Player(Name) VALUES('Mark Crossley');" +
            "INSERT INTO Player(Name) VALUES('Des Lyttle');" +
            "INSERT INTO Player(Name) VALUES('Tony Coton');" +
            "INSERT INTO Player(Name) VALUES('Craig Russell');";

        /*---------- Match ----------*/

       

        public static readonly string INSERT_SQL_MATCH =
            "INSERT INTO Match(" +
            "Season, Round, HomeTeam, AwayTeam, HomeGoal, AwayGoal, Time, MD5Code) " +
            "VALUES(" +
            "(SELECT Id FROM Season WHERE Name='1996-1997'), " +
            "1, " +
            "(SELECT Id FROM Club WHERE Name='Nottingham Forest'), " +
            "(SELECT Id FROM Club WHERE Name='Sunderland'), " +
            "1, " +
            "4, " +
            "CONVERT(datetime, '21 Aug 1996 19:45'), " +
            "'4282858481f6a863830839adb3c381b2');";

        /*---------- Goal ----------*/

        public static readonly string INSERT_SQL_GOAL =
            "INSERT INTO Goal(Match, Player, Time) VALUES(" +
            "(SELECT Id FROM Match WHERE MD5Code='4282858481f6a863830839adb3c381b2'), " +
            "(SELECT Id FROM Player WHERE Name='Alf-Inge Haaland'), " +
            "26);" +
            "INSERT INTO Goal(Match, Player, Time) VALUES(" +
            "(SELECT Id FROM Match WHERE MD5Code='4282858481f6a863830839adb3c381b2'), " +
            "(SELECT Id FROM Player WHERE Name='Michael Gray'), " +
            "7);";

        /*---------- Substitution ----------*/

        public static readonly string JSON_SUBSTITUTION =
            @"{" +
            "'general': {" +
            "'date': '21 Aug 1996'," +
            "'time': '19:45'" +
            "}," +
            "'score': {" +
            "'home': 'Nottingham Forest'," +
            "'away': 'Sunderland'," +
            "}," +
            "'benchHome': {" +
            "'1': 'Chris Allen'}," +
            "'benchAway': {" +
            "'1': 'Craig Russell'," +
            "'2': 'Michael Bridges'" +
            "}," +
            "'subsHome': {" +
            "'1': { 'on': 'Chris Allen', 'off': 'Ian Woan', 'time': '69' }," +
            "'2': { 'on': 'David Wetherall', 'off': 'Lucas Radebe', 'time': '64' }," +
            "'3': { 'on': 'Mark Tinkler', 'off': 'Mark Hateley', 'time': '83' }}," +
            "'subsAway': {" +
            "'1': { 'on': 'Craig Russell', 'off': 'Paul Stewart', 'time': '78' }," +
            "'2': { 'on': 'Michael Bridges', 'off': 'Niall Quinn', 'time': '88' }," +
            "'3': { 'on': 'Graham Potter', 'off': 'Simon Charlton', 'time': '79' }},}";

        public static readonly string INSERT_SQL_SUBSTITUTION =
                "INSERT INTO Substitution(Match, OnPlayer, OffPlayer, Time) VALUES(" +
                "(SELECT Id FROM Match WHERE MD5Code='4282858481f6a863830839adb3c381b2'), " +
                "(SELECT Id FROM Player WHERE Name='Chris Allen'), " +
                "(SELECT Id FROM Player WHERE Name='Ian Woan'), 69);" +
                "INSERT INTO Substitution(Match, OnPlayer, OffPlayer, Time) VALUES(" +
                "(SELECT Id FROM Match WHERE MD5Code='4282858481f6a863830839adb3c381b2'), " +
                "(SELECT Id FROM Player WHERE Name='Craig Russell'), " +
                "(SELECT Id FROM Player WHERE Name='Paul Stewart'), 78);";

        /*---------- Foul ----------*/

        public static readonly string JSON_FOUL =
            "{" +
            "'foulHome': {" +
            "'1': {" +
            "'name': 'Aljosa Asanovic'," +
            "'card': 'Yellow Card'," +
            "'type': 'No'," +
            "'time': '0'}}," +
            "'foulAway': {" +
            "'1': {" +
            "'name': 'Mark Summerbell'," +
            "'card': 'Yellow Card'," +
            "'type': 'No'," +
            "'time': '0'}," +
            "'2': {" +
            "'name': 'Michael Gray'," +
            "'card': 'Yellow/Red'," +
            "'type': 'No'," +
            "'time': '90'}}" +
            "}";
        public static readonly string INSERT_SQL_FOUL =
            "INSERT INTO Foul(Match, Player, Time) VALUES(" +
            "(SELECT Id FROM Match WHERE MD5Code='4282858481f6a863830839adb3c381b2'), " +
            "(SELECT Id FROM Player WHERE Name='Michael Gray'), " +
            "90);";
    }


}

﻿//  FactorDeciderTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       15:37:49 26/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using AutoFixture;
using StarData.RatingEngine.Core;
using Xunit;

namespace StarData.RatingEngine.Tests.Core
{
    public class FactorDeciderTests
    {
        public FactorDeciderTests()
        {
        }

        [Theory]
        [InlineData(1, 17)]
        [InlineData(1, 18)]
        [InlineData(2300, 17)]
        [InlineData(2300, 18)]
        [InlineData(2400, 17)]
        [InlineData(2400, 18)]
        public void SelectKA_NumberOfGameLessThan30_Return40(int rating, int age)
        {
            // Arrange
            const int NUMBER_OF_GAME_LESS_THAN_30 = 1;
            const int KA_EXPECTED = 40;
            var decider = new FactorDecider();

            // Act
            var result = decider.SelectKA(rating, NUMBER_OF_GAME_LESS_THAN_30, age);

            // Assert
            Assert.Equal(KA_EXPECTED, result);
        }

        [Theory]
        [InlineData(30)]
        [InlineData(1)]
        public void SelectKA_AgeLessThan18AndRaUnder2300_Return40(int numberOfGame)
        {
            // Arrange
            const int AGE_LESS_THAN_18 = 1;
            const int RATING_UNDER_2300 = 1;
            const int KA_EXPECTED = 40;
            var decider = new FactorDecider();

            // Act
            var result = decider.SelectKA(RATING_UNDER_2300, numberOfGame, AGE_LESS_THAN_18);

            // Assert
            Assert.Equal(KA_EXPECTED, result);
        }

        [Theory]
        [InlineData(30, 18)]
        public void SelectKA_RaUnder2400_Return20(int numberOfGame, int age)
        {
            // Arrange
            const int RATING_UNDER_2400 = 1;
            const int KA_EXPECTED = 20;
            var decider = new FactorDecider();

            // Act
            var result = decider.SelectKA(RATING_UNDER_2400, numberOfGame, age);

            // Assert
            Assert.Equal(KA_EXPECTED, result);
        }

        [Theory]
        [InlineData(30, 2400, 10)]
        [InlineData(31, 2401, 10)]
        public void SelectKA_NumberOfGameAtLeast30AndRaAtLeast2400_Return10(int numberOfGame, int rating, int ka)
        {
            // Arrange
            var fixture = new Fixture();
            var age = fixture.Create<int>();
            var decider = new FactorDecider();

            // Act
            var result = decider.SelectKA(rating, numberOfGame, age);

            // Assert
            Assert.Equal(ka, result);
        }

        [Theory]
        [InlineData(-1, 0)]
        [InlineData(0, 0.5)]
        [InlineData(1, 1)]
        public void SelectSA_DifferentDai_ReturnSA(int dai, decimal sa)
        {
            // Arrange
            var decider = new FactorDecider();

            // Act
            var result = decider.SelectSA(dai);

            // Assert
            Assert.Equal(sa, result);
        }
    }
}

﻿//  EloEvaluatorTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       14:16:12 26/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using AutoFixture;
using Moq;
using StarData.RatingEngine.Core;
using StarData.RatingEngine.Interface;
using Xunit;

namespace StarData.RatingEngine.Tests.Core
{
    public class EloEvaluatorTests
    {
        public EloEvaluatorTests()
        {
        }

        [Fact]
        public void CalculateNewRA__ReturnNewRA()
        {
            // Arrange
            var fixture = new Fixture();
            var numberOfGame = fixture.Create<int>();
            var age = fixture.Create<int>();
            var dai = fixture.Create<int>();
            const int RA = 1;
            const int RB = 1;
            const int NEW_RA = 6;
            var mockDecider = new Mock<IFactorDecider>();
            mockDecider.Setup(d => d.SelectSA(dai)).Returns(1);
            mockDecider.Setup(d => d.SelectKA(RA, numberOfGame, age)).Returns(10);
            
            var evaluator = new EloEvaluator(mockDecider.Object);

            // Act
            var result = evaluator.CalculateNewRA(RA, numberOfGame, age, RB, dai);

            // Assert
            Assert.Equal(NEW_RA, result);
        }

        [Fact]
        public void CalculateTeamRA()
        {
            // Arrange
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);
            var data = new List<(int rating, int durationOfPlay)>()
            {
                (1,2),
                (3,4),
                (5,6)
            }.AsReadOnly();
            const int TEAM_RA_EXPECTED = 4;

            // Act
            var result = evaluator.CalculateTeamRA(data);

            // Assert
            Assert.Equal(TEAM_RA_EXPECTED, result);
        }

        [Fact]
        public void CalculateCA_DAEqualZero_ReturnCAUsingMAAndMMax()
        {
            // Arrange
            var fixture = new Fixture();
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);
            const int dA = 0;
            mockDecider.Setup(d => d.SelectSA(dA)).Returns(0);
            const decimal w = 1;
            const int eA = 1;
            const int mA = 1;
            const int mMax = 1;
            const decimal CA_EXPECTED = -1M;
            // Act
            var result = evaluator.CalculateCA(w, dA, mA, mMax, eA);

            // Assert
            Assert.Equal(CA_EXPECTED, result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(-1)]
        public void CalculateCA_DAUnequalZero_ReturnCAUsingDA(int dA)
        {
            // Arrange
            var fixture = new Fixture();
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);
            var mA = 1;
            var mMax = 2;
            mockDecider.Setup(d => d.SelectSA(dA)).Returns(0);
            const decimal w = 1;
            const decimal eA = 0.1M;
            const decimal CA_EXPECTED = -0.1M;
            // Act
            var result = evaluator.CalculateCA(w, dA, mA, mMax, eA);

            // Assert
            Assert.Equal(CA_EXPECTED, result);
        }

        [Fact]
        public void CalculateCA_DAEqualZeroAndMADivideMMaxNotInteger_ReturnCAUsingMAAndMMax()
        {
            // Arrange
            const int mA = 1;
            const int mMax = 2;
            const int dA = 0;

            var mockDecider = new Mock<IFactorDecider>();
            mockDecider.Setup(d => d.SelectSA(dA)).Returns(0);
            var evaluator = new EloEvaluator(mockDecider.Object);

            const decimal w = 1;
            const decimal eA = 0.1M;

            const decimal CA_EXPECTED = -0.05M;
            // Act
            var result = evaluator.CalculateCA(w, dA, mA, mMax, eA);

            // Assert
            Assert.Equal(CA_EXPECTED, result);
        }

        [Fact]
        public void CalculateCA_DAEqualZeroAndMAGreaterThanMMax_ThrowsArgumentOutOfRangeException()
        {
            // Arrange
            var fixture = new Fixture();
            var sA = fixture.Create<decimal>();
            var w = fixture.Create<decimal>();
            var eA = fixture.Create<decimal>();
            const int mA = 2;
            const int mMax = 1;
            const int dA = 0;

            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);

            // Act
            Action result = () => evaluator.CalculateCA(w, dA, mA, mMax, eA);
            var exception = Assert.Throws<ArgumentOutOfRangeException>(result);
            
            // Assert
            Assert.IsType<ArgumentOutOfRangeException>(exception);
        }

        /*---------- ReducePlayerRA ---------- */

        [Theory]
        [InlineData(23)]
        [InlineData(41)]
        public void ReducePlayerRA_KANotBetween24And40_ThrowArgumentOutOfRangeExecption(int kA)
        {
            // Arrange
            var fixture = new Fixture();
            var rA = fixture.Create<int>();
            var qA = fixture.Create<decimal>();
            var cA = fixture.Create<decimal>();
            var mA = fixture.Create<int>();
            var mMax = fixture.Create<int>();
            var teamCA = fixture.Create<decimal>();
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);

            // Act
            Action actual = () => evaluator.ReducePlayerRA(rA, kA, qA, cA, teamCA, mA, mMax);

            // Assert
            var exception = Assert.Throws<ArgumentOutOfRangeException>(actual);
            Assert.Equal("kA", exception.ParamName);
            Assert.Equal("Value shoulde be in range 24 - 40 (Parameter 'kA')", exception.Message);

        }

        [Theory]
        [InlineData(0.4)]
        [InlineData(1.1)]
        public void ReducePlayerRA_QANotBetweenPoint5And1_ThrowArgumentOutOfRangeExecption(decimal qA)
        {
            // Arrange
            var fixture = new Fixture();
            var rA = fixture.Create<int>();
            const int kA = 24;
            var cA = fixture.Create<decimal>();
            var mA = fixture.Create<int>();
            var mMax = fixture.Create<int>();
            var teamCA = fixture.Create<decimal>();
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);

            // Act
            Action actual = () => evaluator.ReducePlayerRA(rA, kA, qA, cA, teamCA, mA, mMax);

            // Assert
            var exception = Assert.Throws<ArgumentOutOfRangeException>(actual);
            Assert.Equal("qA", exception.ParamName);
            Assert.Equal("Value shoulde be in range 0.5 - 1 (Parameter 'qA')", exception.Message);

        }

        [Theory]
        [InlineData(2, 1)]
        [InlineData(-1, 1)]
        public void ReducePlayerRA_MAGreaterThanMMaxOrNegative_ThrowArgumentOutOfRangeExecption(int mA, int mMax)
        {
            // Arrange
            var fixture = new Fixture();
            var rA = fixture.Create<int>();
            var cA = fixture.Create<decimal>();
            var teamCA = fixture.Create<decimal>();
            const int kA = 24;
            const decimal qA = 0.5m;
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);

            // Act
            Action actual = () => evaluator.ReducePlayerRA(rA, kA, qA, cA, teamCA, mA, mMax);

            // Assert
            var exception = Assert.Throws<ArgumentOutOfRangeException>(actual);
            Assert.Equal("mA or mMax", exception.ParamName);
            Assert.Equal("Value cannot be negative or mA > mMax (Parameter 'mA or mMax')", exception.Message);
        }

        [Fact]
        public void ReducePlayerRA_ReturnNewRA()
        {
            // Arrange
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);
            const int VALID_RA = 1;
            const int VALID_KA = 24;
            const decimal VALID_QA = 0.5m;
            const decimal VALID_CA = 0.1m;
            const decimal VALID_TEAM_CA = 0.1m;
            const int VALID_MA = 1;
            const int VALID_MMAX = 1;
            const int RESULT_EXPECTED = 3;

            //Act
            var result = evaluator.ReducePlayerRA(VALID_RA, VALID_KA,
                VALID_QA, VALID_CA, VALID_TEAM_CA, VALID_MA, VALID_MMAX);
            
            //Assert
            Assert.Equal(RESULT_EXPECTED, result);
        }
        
        [Fact]
        public void ReduceTeamRA_ReturnNewRA()
        {
            // Arrange
            var mockDecider = new Mock<IFactorDecider>();
            var evaluator = new EloEvaluator(mockDecider.Object);
            var VALID_RA = 1;
            var VALID_CA = 0.1;
            var EXPECTED_RESULT = 3;

            // Act
            var result = evaluator.ReduceTeamRA(VALID_RA, VALID_CA);

            //Assert
            Assert.Equal(EXPECTED_RESULT, result);
        }

    }
}

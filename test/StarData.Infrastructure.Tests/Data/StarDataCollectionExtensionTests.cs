﻿//  StarDataCollectionExtensionTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       18:52:20 31/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Moq;
using StarData.Infrastructure.Data;
using StarData.Infrastructure.Tests.Base;
using Xunit;

namespace StarData.Infrastructure.Tests.Data
{
    public class StarDataCollectionExtensionTests : SqliteContextBaseTests
    {
        [Fact]
        public void SeedStarData_InputJsonFile_CreateSqlFile()
        {
            using(StarDataContext context = new StarDataContext(Options))
            {
                // Arrange

                // Action

                // Assert
            }

        }
    }
}

﻿//  RepositoryTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       Ryan Xu(XuChunlei) <hitxcl@gmail.com>
//  Create at:
//       16:12:57 25/7/2021
//
//  Copyright (c) 2021 XuChunlei
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using StarData.Core.Model.AggregateRoots;
using StarData.Core.Repositories;
using StarData.Core.Specifications;
using StarData.Infrastructure.Data;
using StarData.Infrastructure.Repositories;
using StarData.Infrastructure.Tests.Base;
using Xunit;

namespace StarData.Infrastructure.Tests
{
    public class RepositoryTests : SqliteContextBaseTests
    {

        //[Fact]
        //public async void Create_Nation_ReturnSuccessDataResult()
        //{
        //    using (var context = new StarDataContext(Options))
        //    {
        //        // Arrange
        //        Seed(context);
        //        IEnumerable<IDataValidator<Nation>> NULL_VALIDATORS = null;
        //        var mockLogger = new Mock<ILogger<Repository<Nation>>>();
        //        var mockDao = new Mock<DaoAgent<Nation>>();
        //        var repository = new Repository<Nation>(mockDao.Object, NULL_VALIDATORS, mockLogger.Object);
        //        const string NATION_NAME = "C";
        //        var nation = new Nation { Name = NATION_NAME };

        //        // Act
        //        var result = repository.CreateAsync(nation).Result;

        //        // Assert
        //        Assert.True(result.Succeeded);
        //        Assert.Equal(NATION_NAME, result.Data.Name);
        //    }
        //}

        //[Fact]
        //public void GetAll_Nations_ReturnSuccessDataResultWithAllNations()
        //{
        //    using (var context = new StarDataContext(Options))
        //    {
        //        // Arrange
        //        Seed(context);
        //        const IEnumerable<IDataValidator<Nation>> NULL_VALIDATORS = null;
        //        var spec = new Mock<ISpecification<Nation>>();
        //        var mockLogger = new Mock<ILogger<Repository<Nation>>>();
        //        var mockDao = new Mock<IDao<Nation>>();
        //        var nation = new Mock<Nation>();
        //        mockDao.Setup(d => d.GetAllAsync(spec.Object))
        //            .Returns(new List<Nation>
        //            {
        //                nation.Object
        //            }.AsEnumerable());
        //        var repository = new Repository<Nation>(mockDao.Object, NULL_VALIDATORS, mockLogger.Object);

        //        // Act
        //        var result = repository.GetAllAsync(spec.Object);

        //        // Assert
        //        Assert.Single(result);

        //    }
        //}
    }
}

﻿//  DbContextBase.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:2:45 31/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using Microsoft.EntityFrameworkCore;
using StarData.Core.Model.AggregateRoots;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Tests.Base
{
    public class DbContextBaseTests : FixtureBaseTests
    {
        protected DbContextOptions<StarDataContext> Options { get; }
        public DbContextBaseTests(DbContextOptions<StarDataContext> options)
        {
            Options = options;
        }

        protected virtual void Seed(DbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            var league = new League();
            league.Name = "English Premier League";
            context.Add<League>(league);

            var nation = new Nation();
            nation.Name = "England";
            nation.Code = "Eg";
            nation.Alias = "English";
            context.Add<Nation>(nation);

            var season = new Season();
            season.Name = "1996-1997";
            context.Add<Season>(season);

            context.SaveChanges();

        }
    }
}

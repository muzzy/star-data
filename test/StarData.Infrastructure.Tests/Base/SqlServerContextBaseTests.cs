﻿//  SqlServerContextBaseTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       1:38:54 6/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Tests.Base
{
    public class SqlServerContextBaseTests : DbContextBaseTests, IDisposable
    {
        private const string _connectionString =
            "Server=localhost,1433;User Id=sa;Password=34U8&4ie(b;Initial Catalog=StarDataBase;";
        private readonly DbConnection _connection;

        public SqlServerContextBaseTests() : base(
            new DbContextOptionsBuilder<StarDataContext>()
            .UseSqlServer(_connectionString).Options)
        {
            _connection = RelationalOptionsExtension.Extract(Options).Connection;
        }

        public void Dispose() => _connection?.Dispose();
    }
}

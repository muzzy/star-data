﻿//  FixtureBaseTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       9:18:54 6/8/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using AutoFixture;

namespace StarData.Infrastructure.Tests.Base
{
    public class FixtureBaseTests
    {
        private Fixture _fixture = new Fixture();

        protected string S()
        {
            return V<string>();
        }
        protected int I()
        {
            return V<int>();
        }
        protected T V<T>()
        {
            return _fixture.Create<T>();
        }
    }
}

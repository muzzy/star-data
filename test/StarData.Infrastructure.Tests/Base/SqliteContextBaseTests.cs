﻿//  SqliteContextBaseTests.cs
//
// Description: 
//       <Describe here>
//  Author:
//       xuchunlei <hitxcl@gmail.com>
//  Create at:
//       19:10:37 31/7/2021
//
//  Copyright (c) 2021 ${CopyrightHolder}
using System;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using StarData.Infrastructure.Data;

namespace StarData.Infrastructure.Tests.Base
{
    public class SqliteContextBaseTests : DbContextBaseTests, IDisposable
    {
        private readonly DbConnection _connection;

        public SqliteContextBaseTests() : base(
            new DbContextOptionsBuilder<StarDataContext>()
            .UseSqlite(CreateInMemoryDatabase()).Options)
        {
            _connection = RelationalOptionsExtension.Extract(Options).Connection;
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();
            return connection;
        }

        public void Dispose() => _connection.Dispose();
    }
}

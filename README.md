# Star Data
Using ELO Algorithm Ranks The Winning 11 Football Players of Nation Team according to their performance data.

## Links
[Demo](https://zen-borg-eec214.netlify.app/)

[Repo: star-web](https://gitlab.com/muzzy/star-web)

## Milestones
| Date  | Milestone  | Tag |
|:----------- |:---------------| :-------------|
| 20 Jun, 2021 | Initialize star-data project |  [init project](https://gitlab.com/muzzy/star-data/-/commit/23e567a0ab7fb0ddb62c18e9530cb544f8aebc96) |
|19 Jul, 2021|Finish CI/CD configuration|[ci support dind](https://gitlab.com/muzzy/star-data/-/commit/a7842daf1ab4f4667938d2e59db940711fdbbf2f)|
|28 Jul, 2021|Publish introduction page|[update index style](https://gitlab.com/muzzy/star-data/-/commit/72dfeade798865dd36aed5ffe73d1b7e2fd97fa9)|
|18 Aug, 2021|Rating by Match or Season|[Add Porject StarData.Utilities](https://gitlab.com/muzzy/star-data/-/commit/ef65a01cb6e9cd59ce202ca3cd0221a42b51a198)|

## Run me


## References

Anybody interested in the materials referred to this project, Please [click](doc/References.md).

# References

## Architecture
[DDD](https://www.kancloud.cn/wizardforcel/learning-hard-csharp/111611)

## Gitlab CI/CD
[Publishing ASP.NET Core unit test results and code coverage to Azure Devops using Docker Images](https://medium.com/@harioverhere/running-asp-net-52a6ed92375b)

[Building a docker image with Gitlab CI and .NET Core](https://faun.pub/building-a-docker-image-with-gitlab-ci-and-net-core-8f59681a86c4)

[Docker Compose and GitLab](https://medium.com/@vitalypanukhin/docker-compose-and-gitlab-b209d09210f6)

[.NET Core Multi-Stage Dockerfile with Test and Code Coverage in Azure Pipelines](https://colinsalmcorner.com/net-core-multi-stage-dockerfile-with-test-and-code-coverage-in-azure-pipelines/)

[.NET Core - Open Source Unit Test Coverage and Reports](https://www.linkedin.com/pulse/net-core-open-source-unit-test-coverage-reports-federico-antu%C3%B1a/?trk=public_profile_article_view)

## Algorithm & Data
[A football player rating system](https://content.iospress.com/articles/journal-of-sports-analytics/jsa200411)

[Football Data](https://github.com/jokecamp/FootballData)

[whoscored.com](https://www.whoscored.com/)

## Tools
[SVG Editor](https://svgeditoronline.com/)